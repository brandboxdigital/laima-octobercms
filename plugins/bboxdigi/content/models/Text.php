<?php namespace Bboxdigi\Content\Models;

use Model;
use Cache;

/**
 * Model
 */
class Text extends Model
{
    use \October\Rain\Database\Traits\Validation;

    use \October\Rain\Database\Traits\SoftDelete;

    public $implement = ['@RainLab.Translate.Behaviors.TranslatableModel'];

    public $translatable = [
        'text',
    ];

    protected $dates = ['deleted_at'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'bboxdigi_content_texts';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public function afterSave() {
        $locales = \RainLab\Translate\Models\Locale::listEnabled();
        foreach ($locales as $key => $locale) {
            Cache::forget('texts_'.$key);
        }
    }
}
