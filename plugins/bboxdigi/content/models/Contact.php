<?php namespace Bboxdigi\Content\Models;

use Model;

/**
 * Model
 */
class Contact extends Model
{
    use \October\Rain\Database\Traits\Validation;

    use \October\Rain\Database\Traits\SoftDelete;

    public $implement = ['@RainLab.Translate.Behaviors.TranslatableModel'];

    public $translatable = [
        'red_title',
        'red_text',
        'side_title',
        'side_text',
        'contacts',
    ];

    protected $dates = ['deleted_at'];

    public $jsonable = [
        'contacts'
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'bboxdigi_content_contacts';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
}
