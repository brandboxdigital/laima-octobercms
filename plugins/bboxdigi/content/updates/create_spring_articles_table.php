<?php namespace Bboxdigi\Content\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateSpringArticlesTable extends Migration
{
    public function up()
    {
        Schema::create('bboxdigi_content_spring_articles', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->timestamps();
            $table->boolean('is_published')->default(false);
            $table->string('title');
            $table->text('subitems');
            $table->integer('sort_order')->nullable()->unsigned()->default(0);
        });
    }

    public function down()
    {
        Schema::dropIfExists('bboxdigi_content_spring_articles');
    }
}