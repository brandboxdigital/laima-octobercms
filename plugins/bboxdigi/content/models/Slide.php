<?php namespace Bboxdigi\Content\Models;

use Model;

/**
 * Model
 */
class Slide extends Model
{
    use \October\Rain\Database\Traits\Validation;

    use \October\Rain\Database\Traits\SoftDelete;
    use \October\Rain\Database\Traits\Sortable;

    public $implement = ['@RainLab.Translate.Behaviors.TranslatableModel'];

    public $translatable = [
        'title',
    ];

    protected $dates = ['deleted_at'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'bboxdigi_content_slides';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $attachOne = [
        'title_image' => 'System\Models\File',
        'mobile_title_image' => 'System\Models\File',
    ];
}
