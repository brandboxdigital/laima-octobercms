<?php namespace Bboxdigi\Content\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBboxdigiContentExtraContents extends Migration
{
    public function up()
    {
        Schema::table('bboxdigi_content_extra_contents', function($table)
        {
            $table->text('chocolate_qualities')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('bboxdigi_content_extra_contents', function($table)
        {
            $table->dropColumn('chocolate_qualities');
        });
    }
}
