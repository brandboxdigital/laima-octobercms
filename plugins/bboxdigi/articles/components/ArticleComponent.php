<?php namespace Bboxdigi\Articles\Components;

use Cms\Classes\ComponentBase;
use Bboxdigi\Articles\Models\Article;
use Bboxdigi\Articles\Models\Recipe;
use Cms\Classes\Page;

class ArticleComponent extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Article Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [
            'count' => [
                'title'       => 'Articles count per page',
                'description' => 'How many articles to load in each request',
                'default'     => '14',
                'validationPattern' => '^[0-9]+$',
                'type'        => 'string'
            ],
        ];
    }

    public function onRun()
    {
        $slug = $this->param('slug', null);
        if ($this->page->url == "/news/:slug") {
            $article = Article::where('slug', $slug)->first();
            if ($article) {
                $this->page->seo_keywords = $article->seo_keywords;
                $this->page->seo_title = $article->seo_title;
                $this->page->title = $article->title;
                $this->page->seo_description = $article->seo_description;
            }
        } elseif ($this->page->url == "/recipes/:slug") {
            $recipe = Recipe::where('slug', $slug)->first();
            if ($recipe) {
                $this->page->seo_keywords = $recipe->seo_keywords;
                $this->page->seo_title = $recipe->seo_title;
                $this->page->title = $recipe->title;
                $this->page->seo_description = $recipe->seo_description;
            }
        }
        
    }

    public function LandingArticles($count = null) {
        return $this->getArticles($count)->get();
    }

    public function LandingRecipes($count = null) {
        $recepies = $this->getRecipes($count)->get();

        return $recepies;
    }

    public function getArticles($count) {
        $query = Article::isPublished()->orderByPublished();
        if ($count) {
            $query->take($count);
        }
        return $query;
    }

    public function getRecipes($count) {
        $query = Recipe::isPublished()->orderByPublished();
        if ($count) {
            $query->take($count);
        }
        return $query;
    }

    public function getArticlesPerPage($page = 0) {
        $take = $this->property('count',14);
        $skip = $page * $take;
        $query = $this->getArticles($take)->skip($skip);
        return $query->get();
    }

    public function getRecipesPerPage($page = 0) {
        $take = $this->property('count',14);
        $skip = $page * $take;
        $query = $this->getRecipes($take)->skip($skip);
        return $query->get();
    }

    public function getTotalPages()
    {
        $query = Recipe::isPublished()->orderByPublished()->count();
        $totalPages = $query / $this->property('count',14);
        return $totalPages;
    }

}
