<?php namespace Bboxdigi\Articles\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBboxdigiArticlesRecipes extends Migration
{
    public function up()
    {
        Schema::table('bboxdigi_articles_recipes', function($table)
        {
            $table->text('seo_keywords')->nullable();
            $table->text('seo_description')->nullable();
            $table->string('seo_title')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('bboxdigi_articles_recipes', function($table)
        {
            $table->dropColumn('seo_keywords');
            $table->dropColumn('seo_description');
            $table->dropColumn('seo_title');
        });
    }
}