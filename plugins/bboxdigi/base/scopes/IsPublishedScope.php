<?php namespace Bboxdigi\Base\Scopes;

use Illuminate\Database\Eloquent\Model as ModelBase;
use Illuminate\Database\Eloquent\Scope as ScopeInterface;
use Illuminate\Database\Eloquent\Builder as BuilderBase;
use Carbon\Carbon;

class IsPublishedScope implements ScopeInterface
{
    /**
     * Apply the scope to a given Eloquent query builder.
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @return void
     */
    public function apply(BuilderBase $builder, ModelBase $model)
    {
        return $builder->where(function($query) use ($model) {
            $exists_ispublished = in_array('is_published', $model->publish_fields);
            $exists_at          = in_array('published_at', $model->publish_fields);
            $exists_till        = in_array('published_till', $model->publish_fields);
            $now = Carbon::now();

            if ($exists_ispublished) {
                $query->where('is_published',true);
            }

            if ($exists_at) {
                $query->where('published_at','<=',$now);
            }
            
            if ($exists_till) {
                $query->where(function($endQuery) use ($now) {
                    $endQuery
                        ->whereNull('published_till')
                        ->orWhere('published_till','>=',$now);
                });
            }

            // Will leave this here for debugin purposes
            // ----------------------------------------------------------------
            // if (class_basename(get_class($model)) == 'Sale') {
            // dd($model->publish_fields, $exists_ispublished, $query->toSql());
            // }
        });
    }
}
