<?php namespace Bboxdigi\Content;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
      return [
        'Bboxdigi\Content\Components\ContentComponent' => 'Content',
        'Bboxdigi\Content\Components\FormComponent' => 'Form',
      ];
    }

    public function registerSettings()
    {
    }

    public function registerMailTemplates()
    {
        return [
            'bboxdigi.content::mail.message' => 'Form report',
        ];
    }
}
