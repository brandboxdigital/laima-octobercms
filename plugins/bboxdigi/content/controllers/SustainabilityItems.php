<?php namespace Bboxdigi\Content\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Sustainability Items Back-end Controller
 */
class SustainabilityItems extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
        'Backend\Behaviors\ReorderController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';
    public $reorderConfig = 'config_reorder.yaml';

    public $bodyClass = 'compact-container';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Bboxdigi.Content', 'bboxdigi-laima-content', 'bboxdigi-laima-content-sustainability-items');
    }
}
