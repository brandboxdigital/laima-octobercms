<?php namespace Bboxdigi\Content\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Extra Contents Back-end Controller
 */
class ExtraContents extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Bboxdigi.Content', 'bboxdigi-laima-content', 'bboxdigi-laima-content-extra-contents');
    }
}
