<?php namespace Bboxdigi\Content\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateBboxdigiContentContacts extends Migration
{
    public function up()
    {
        Schema::create('bboxdigi_content_contacts', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->timestamp('deleted_at')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->string('red_title')->nullable();
            $table->text('red_text')->nullable();
            $table->text('contacts')->nullable();
            $table->string('side_title')->nullable();
            $table->text('side_text')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('bboxdigi_content_contacts');
    }
}
