<?php namespace Bboxdigi\Products\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBboxdigiProductsCategories extends Migration
{
    public function up()
    {
        Schema::table('bboxdigi_products_categories', function($table)
        {
            $table->text('seo_keywords')->nullable();
            $table->text('seo_description')->nullable();
            $table->string('seo_title')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('bboxdigi_products_categories', function($table)
        {
            $table->dropColumn('seo_keywords');
            $table->dropColumn('seo_description');
            $table->dropColumn('seo_title');
        });
    }
}