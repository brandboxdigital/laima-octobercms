/* ==================================================== */
var log         = require('fancy-log');
/* ==================================================== */
var gulp        = require('gulp');
var sass        = require('gulp-sass');
var concat      = require('gulp-concat');
var babel       = require("gulp-babel");
var browserSync = require('browser-sync').create();
var bourbon     = require("bourbon").includePaths;
var autoprefix  = require("gulp-autoprefixer");
var sourcemaps  = require('gulp-sourcemaps');
var minify      = require("gulp-babel-minify");
var svgSprite   = require("gulp-svg-sprites");


var sassPaths = [
  'assets/scss/main.scss',
  bourbon
];

var jsPaths = [
  'assets/js/main-new/*.js'
  // 'assets/js/extra/*.js'
];

// var svgconfig = {
//   cssFile: "scss/basic/_svg-sprites.scss",
//   svg: {
//       sprite: "img/icon-sprite.svg",
//       symbols: "img/icon-sprite.svg",
//       defs: "img/icon-sprite.svg",
//   },
//   preview: {
//       sprite: "img/icon-sprite-preview.html",
//       symbols: "img/icon-sprite-preview.html",
//       defs: "img/icon-sprite-preview.html",
//   },

//   mode: "symbols",
// };

/**
 * ===============================================================
 * Compiling svg sprites
 */
// gulp.task('sprites', function () {
//     return gulp.src('assets/img/icon-sprite-saurce/*.svg')
//       .pipe(svgSprite(svgconfig))
//       .pipe(gulp.dest("assets"));
// });

/**
 * ===============================================================
 * Compiling sass files
 */
gulp.task("sass-build--env-dev", function () {
  return gulp.src('assets/scss/main.scss')
    .pipe(sass({
          sourcemaps: true,
          includePaths: sassPaths,
      })
      .on('error', sass.logError)
    )
    .pipe(autoprefix("last 2 versions"))
    .pipe(gulp.dest('assets/css/extra'))
    .pipe(browserSync.stream());
});

gulp.task("sass-build", function () {
  return gulp.src('assets/scss/main.scss')
    .pipe(sass({
          sourcemaps: true,
          includePaths: sassPaths,
          outputStyle: 'compressed',
      })
      .on('error', sass.logError)
    )
    .pipe(autoprefix("last 4 versions"))
    .pipe(gulp.dest('assets/css/extra'))
});

/**
 * ===============================================================
 * Compiling js files and running then trough babel
 */
// gulp.task('js-vendor', function(done) {
//   return gulp.src(['assets/js/vendor/*.js'])
//     .pipe(concat('vendor.js'))
//     .pipe(babel({presets: ["@babel/env"]}))
//     .pipe(gulp.dest('assets/js/min'))
//     .pipe(browserSync.stream());
// });

gulp.task('js-build--env-dev', function(done) {
  return gulp.src(jsPaths)
    .pipe(concat('main-new.js'))
    .pipe(babel({presets: ["@babel/env"]}))
    .pipe(gulp.dest('assets/js/main-new/min'))
    .pipe(browserSync.stream());
});

gulp.task('js-build', function() {
  console.log(jsPaths);
  return gulp.src(jsPaths)
    .pipe(concat('main-new.js'))
    .pipe(babel({
      presets: ["@babel/env"]
    }))
    // .pipe(sourcemaps.init())
    // .pipe(minify({
    //   mangle: {
    //     keepClassName: true,
    //     keepFnName: true
    //   }
    // }))
    // .pipe(sourcemaps.write('assets/js/main-new'))
    .pipe(gulp.dest('assets/js/main-new/min'))
});


/**
 * ======================
 * HTML + TWIG for static sites...
 */
// gulp.task('html-compile', function () {
    
//     return gulp.src('twig/pages/*.twig')
//       .pipe(twig({
//           base: './twig'
//       }))
//       .pipe(gulp.dest('./'))
//       .pipe(browserSync.stream());
// });

/**
 * ===============================================================
 * Calling Browser Sync And watching for changes
 */
gulp.task('watcher', function() {
  browserSync.init({
      // server: {
      //     baseDir: "./"
      // }
      proxy: "http://laima-web.laragon/",
  });

  // gulp.watch(['assets/img/icon-sprite-saurce/*.svg'], gulp.series(['sprites']));

  gulp.watch(['assets/scss/**/*.scss'], gulp.series(['sass-build--env-dev']));

  // gulp.watch(['assets/js/vendor/**/*.js'], gulp.series(['js-vendor']));
  gulp.watch(['assets/js/extra/*.js', 'assets/js/main-new/main-new.js'], gulp.series(['js-build--env-dev']));
  
  // gulp.watch(['twig/**/*.twig'], gulp.series(['html-compile']));
  gulp.watch(['**/*.html', '**/*.htm']).on('change', browserSync.reload);
});

gulp.task('just-watch', function() {
  browserSync.init({
      // server: {
      //     baseDir: "./"
      // }
      proxy: "http://laima-web.laragon/",
  });
  
  // gulp.watch(['twig/**/*.twig'], gulp.series(['html-compile']));
  gulp.watch(['**/*.html', '**/*.htm', '**/*.php']).on('change', browserSync.reload);
});


// gulp.task( "default", gulp.series(['sprites', 'sass-build--env-dev', 'js-vendor', 'js-build--env-dev', 'watcher']) );
gulp.task( "default", gulp.series(['sass-build--env-dev', 'js-build--env-dev', 'watcher']) );
// gulp.task( "build", gulp.series(['sprites', 'sass-build', 'js-vendor', 'js-build']) );
gulp.task( "build", gulp.series(['sass-build', 'js-build']) );

