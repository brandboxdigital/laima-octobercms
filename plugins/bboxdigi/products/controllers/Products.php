<?php namespace Bboxdigi\Products\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class Products extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController',        'Backend\Behaviors\ReorderController',    'Backend\Behaviors\RelationController'];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $reorderConfig = 'config_reorder.yaml';
    public $relationConfig = 'config_relation.yaml';

    public $bodyClass = 'compact-container';

    public $requiredPermissions = [
        'bboxdigi-laima-products'
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Bboxdigi.Products', 'bboxdigi-laima-products', 'bboxdigi-laima-products-products');
    }
}
