<?php namespace Bboxdigi\Products\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateBboxdigiProductsCatProd extends Migration
{
    public function up()
    {
        Schema::create('bboxdigi_products_cat_prod', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('category_id')->unsigned();
            $table->integer('product_id')->unsigned();
            $table->primary(['category_id','product_id']);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('bboxdigi_products_cat_prod');
    }
}