<?php namespace Bboxdigi\Products\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBboxdigiProductsCollections extends Migration
{
    public function up()
    {
        Schema::table('bboxdigi_products_collections', function($table)
        {
            $table->text('seo_keywords')->nullable();
            $table->text('seo_description')->nullable();
            $table->string('seo_title')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('bboxdigi_products_collections', function($table)
        {
            $table->dropColumn('seo_keywords');
            $table->dropColumn('seo_description');
            $table->dropColumn('seo_title');
        });
    }
}
