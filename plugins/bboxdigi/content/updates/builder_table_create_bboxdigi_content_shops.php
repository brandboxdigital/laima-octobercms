<?php namespace Bboxdigi\Content\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateBboxdigiContentShops extends Migration
{
    public function up()
    {
        Schema::create('bboxdigi_content_shops', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->string('title');
            $table->string('slug');
            $table->integer('sort_order')->unsigned()->default(0);
            $table->text('text')->nullable();
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->boolean('is_published')->default(0);
            $table->string('long')->nullable();
            $table->string('lat')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('bboxdigi_content_shops');
    }
}
