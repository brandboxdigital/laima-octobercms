<?php namespace Bboxdigi\Articles\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class Recipes extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController'    ];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public $bodyClass = 'compact-container';

    public $requiredPermissions = [
        'bboxdigi-laima-articles'
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Bboxdigi.Articles', 'bboxdigi-laima-articles', 'bboxdigi-laima-articles-recipes');
    }
}
