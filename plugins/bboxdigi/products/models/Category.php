<?php namespace Bboxdigi\Products\Models;

use Model;

use Cms\Classes\Page as CmsPage;
use Cms\Classes\Theme;

/**
 * Model
 */
class Category extends Model
{
    use \October\Rain\Database\Traits\Validation;

    use \October\Rain\Database\Traits\SoftDelete;
    use \October\Rain\Database\Traits\Sluggable;
    use \October\Rain\Database\Traits\Sortable;

    /**
     * @var array Generate slugs for these attributes.
     */
    protected $slugs = ['slug' => 'title'];

    public $implement = ['@RainLab.Translate.Behaviors.TranslatableModel'];

    public $translatable = [
        'title',
    ];

    protected $dates = ['deleted_at'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'bboxdigi_products_categories';

    /**
     * @var array Validation rules
     */

    public $belongsToMany = [
        'products' => [
            'Bboxdigi\Products\Models\Product',
            'table' => 'bboxdigi_products_cat_prod'
        ],
    ];

    public $rules = [
    ];

    public function scopeIsPublished($query)
    {
        return $query->where('is_published', 1);
    }

    public static function getMenuTypeInfo($type)
    {
        $result = [
            'dynamicItems' => true
        ];
        $theme = Theme::getActiveTheme();
        $pages = CmsPage::listInTheme($theme, true);
        $result['cmsPages'] = $pages;

        return $result;
    }

    public static function resolveMenuItem($item, $url, $theme)
    {
        $result = [
            'items' => []
        ];

        $page = CmsPage::loadCached($theme, $item->cmsPage);

        $defaultLocale = \RainLab\Translate\Models\Locale::getDefault()->code;

        $alternateLocales = array_keys(\RainLab\Translate\Models\Locale::listEnabled());

        $categories = self::isPublished()->get();
        foreach ($categories as $category) {
            $category->_type = 'cat';
            
            $pageUrl = \Utopigs\Seo\Models\Sitemap::getPageLocaleUrl($page, $category, $defaultLocale, [
                'type' => '_type',
                'slug' => 'slug'
            ]);

            $categoryItem = [
                'title' => $category->title,
                'url'   => $pageUrl,
                'mtime' => $category->updated_at,
            ];
            if (count($alternateLocales) > 1) {
                foreach ($alternateLocales as $locale) {
                    $categoryItem['alternate_locale_urls'][$locale] = \Utopigs\Seo\Models\Sitemap::getPageLocaleUrl($page, $category, $locale, [
                        'type' => '_type',
                        'slug' => 'slug'
                    ]);
                }
            }

            $result['items'][] = $categoryItem;
        }
        return $result;
    }
}
