<?php namespace Bboxdigi\Content\Models;

use Model;

/**
 * ExtraContent Model
 */
class ExtraContent extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'bboxdigi_content_extra_contents';

    public $implement = ['@RainLab.Translate.Behaviors.TranslatableModel'];

    public $translatable = [
        'balta_sarma_title',
        'balta_sarma_items',
        'balta_sarma_bottom_text'
    ];

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Validation rules for attributes
     */
    public $rules = [];

    /**
     * @var array Attributes to be cast to native types
     */
    protected $casts = [];

    /**
     * @var array Attributes to be cast to JSON
     */
    protected $jsonable = ['chocolate_qualities', 'balta_sarma_items'];

    /**
     * @var array Attributes to be appended to the API representation of the model (ex. toArray())
     */
    protected $appends = [];

    /**
     * @var array Attributes to be removed from the API representation of the model (ex. toArray())
     */
    protected $hidden = [];

    /**
     * @var array Attributes to be cast to Argon (Carbon) instances
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [
        'qualities_image' => 'System\Models\File',
        'balta_sarma_bg_desktop' => 'System\Models\File',
        'balta_sarma_bg_mobile' => 'System\Models\File',
        'balta_sarma_icon' => 'System\Models\File',
        'spring_intro_image' => 'System\Models\File',
        'spring_intro_image_mobile' => 'System\Models\File',
        'painting_icon' => 'System\Models\File',
        'photo_gallery_icon' => 'System\Models\File',
        'spring_footer_image_mobile' => 'System\Models\File'
    ];
    public $attachMany = [
        'paintings' => 'System\Models\File',
        'paintings_mobile' => 'System\Models\File',
        'gallery_images' => 'System\Models\File',
        'wallpapers_desktop' => 'System\Models\File',
        'wallpapers_mobile' => 'System\Models\File'
    ];
}
