<?php namespace Bboxdigi\Products\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateBboxdigiProductsProducts extends Migration
{
    public function up()
    {
        Schema::create('bboxdigi_products_products', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->string('title');
            $table->string('slug');
            $table->text('excerpt')->nullable();
            $table->string('weight')->nullable();
            $table->string('code')->nullable();
            $table->text('desc')->nullable();
            $table->text('nutrition')->nullable();
            $table->string('shop_link')->nullable();
            $table->integer('collection_id')->nullable()->unsigned();
            $table->integer('sort_order')->unsigned()->default(0);
            $table->boolean('is_published')->default(0);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('bboxdigi_products_products');
    }
}