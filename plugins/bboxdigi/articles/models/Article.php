<?php namespace Bboxdigi\Articles\Models;

use Model;

use Cms\Classes\Page as CmsPage;
use Cms\Classes\Theme;

/**
 * Model
 */
class Article extends Model
{
    use \October\Rain\Database\Traits\Validation;

    use \October\Rain\Database\Traits\SoftDelete;
    use \October\Rain\Database\Traits\Sluggable;
    use \Bboxdigi\Base\Traits\IsPublished;

    /**
     * @var array Generate slugs for these attributes.
     */
    protected $slugs = ['slug' => 'title'];

    public $implement = ['@RainLab.Translate.Behaviors.TranslatableModel'];

    public $translatable = [
        'title',
        'excerpt',
        'body',
    ];

    /**
     * @var array Fields used for publish states
     */
    public $publish_fields = [
        'is_published',
        'published_at',
        'published_till',
    ];

    protected $dates = ['deleted_at'];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'bboxdigi_articles_articles';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $attachOne = [
        'title_image' => 'System\Models\File',
    ];

    public $attachMany = [
        'images' => 'System\Models\File',
    ];

    public function scopeIsPublished($query)
    {
        return $query->where('is_published', 1);
    }

    public static function getMenuTypeInfo($type)
    {
        $result = [
            'dynamicItems' => true
        ];
        $theme = Theme::getActiveTheme();
        $pages = CmsPage::listInTheme($theme, true);
        $result['cmsPages'] = $pages;

        return $result;
    }

    public static function resolveMenuItem($item, $url, $theme)
    {
        $result = [
            'items' => []
        ];

        $page = CmsPage::loadCached($theme, $item->cmsPage);

        $defaultLocale = \RainLab\Translate\Models\Locale::getDefault()->code;

        $alternateLocales = array_keys(\RainLab\Translate\Models\Locale::listEnabled());

        $articles = self::isPublished()->get();
        foreach ($articles as $article) {
            $pageUrl = \Utopigs\Seo\Models\Sitemap::getPageLocaleUrl($page, $article, $defaultLocale, [
                'slug' => 'slug'
                ]);

            $articleItem = [
                'title' => $article->title,
                'url'   => $pageUrl,
                'mtime' => $article->updated_at,
            ];
            if (count($alternateLocales) > 1) {
                foreach ($alternateLocales as $locale) {
                    $articleItem['alternate_locale_urls'][$locale] = \Utopigs\Seo\Models\Sitemap::getPageLocaleUrl($page, $article, $locale, ['slug' => 'slug']);
                }
            }

            $result['items'][] = $articleItem;
        }
        return $result;
    }
}
