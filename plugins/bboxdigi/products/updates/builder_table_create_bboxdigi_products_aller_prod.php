<?php namespace Bboxdigi\Products\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateBboxdigiProductsAllerProd extends Migration
{
    public function up()
    {
        Schema::create('bboxdigi_products_aller_prod', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('allergen_id')->unsigned();
            $table->integer('product_id')->unsigned();
            $table->primary(['allergen_id','product_id']);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('bboxdigi_products_aller_prod');
    }
}
