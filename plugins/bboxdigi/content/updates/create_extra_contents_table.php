<?php namespace Bboxdigi\Content\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateExtraContentsTable extends Migration
{
    public function up()
    {
        Schema::create('bboxdigi_content_extra_contents', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->timestamps();
            $table->text('video_src');
        });
    }

    public function down()
    {
        Schema::dropIfExists('bboxdigi_content_extra_contents');
    }
}