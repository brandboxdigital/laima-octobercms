<?php namespace Bboxdigi\Products\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBboxdigiProductsProducts2 extends Migration
{
    public function up()
    {
        Schema::table('bboxdigi_products_products', function($table)
        {
            $table->dateTime('new_at')->nullable();
        });
    }

    public function down()
    {
        Schema::table('bboxdigi_products_products', function($table)
        {
            $table->dropColumn('new_at');
        });
    }
}
