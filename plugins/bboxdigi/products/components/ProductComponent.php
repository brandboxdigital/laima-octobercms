<?php namespace Bboxdigi\Products\Components;

use Cms\Classes\ComponentBase;
use Bboxdigi\Products\Models\Product;
use Bboxdigi\Products\Models\Category;
use Bboxdigi\Products\Models\Collection;
use Bboxdigi\Products\Models\Allergen;
use Cms\Classes\Page;

class ProductComponent extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Product Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [
            'count' => [
                'title'       => 'Product count per page',
                'description' => 'How many products to load in each request',
                'default'     => '20',
                'validationPattern' => '^[0-9]+$',
                'type'        => 'string'
            ],
        ];
    }

    public function onRun()
    {
        $type = $this->param('type', null);
        $slug = $this->param('slug', null);
        if ($type == 'collection') {
            $col = Collection::where('slug', $slug)->first();
            if ($col) {
                $this->page->seo_keywords = $col->seo_keywords;
                $this->page->seo_title = $col->seo_title;
                $this->page->title = $col->title;
                $this->page->seo_description = $col->seo_description;
            }
        }
        elseif ($type == 'cat') {
            $cat = Category::where('slug', $slug)->first();
            if ($cat) {
                $this->page->seo_keywords = $cat->seo_keywords;
                $this->page->seo_title = $cat->seo_title;
                $this->page->title = $cat->title;
                $this->page->seo_description = $cat->seo_description;
            }
        }
        else {
            $product = Product::where('slug', $slug)->first();
            if ($product) {
                $this->page->seo_keywords = $product->seo_keywords;
                $this->page->seo_title = $product->seo_title;
                $this->page->title = $product->title;
                $this->page->seo_description = $product->seo_description;
            }
        }
    }

    public function filteredProducts($page = 0, $categories = [], $collections = [], $allergens = [], $search = '') {

        $query = Product::with('title_image')->where('is_published', 1);

        if ($categories) {
            $query->whereHas('categories', function($query) use ($categories) {
                $query->whereIn('id', $categories);
            });
        }

        if ($collections) {
            $query->whereHas('collections', function($query) use ($collections) {
                $query->whereIn('id', $collections);
            });
        }

        if ($allergens) {
            $query->whereDoesntHave('allergens', function($query) use ($allergens) {
                $query->whereIn('id', $allergens);
            });
        }

        if ($search) {
            $query->where('title', 'like', '%'.$search.'%');
        }

        $take = $this->property('count',20);
        $skip = $page * $take;

        $query->take($take)->skip($skip)->orderBy('sort_order', 'desc');

        $products = $query->get();

        return $products;
    }

    public function getProductCount(){
        return Product::where('is_published', 1)->get()->count();
    }

    public function getfilteredCategories($collections = [], $allergens = [], $search = '') {

        $query = Category::where('is_published', 1)->whereHas('products', function($query) {
            $query->where('is_published', 1);
        });

        if ($collections) {
            $query->whereHas('products', function($query) use ($collections) {
                $query->whereHas('collections', function($query) use ($collections) {
                    $query->whereIn('id', $collections);
                });
            });
        }

        if ($allergens) {
            $query->whereHas('products', function($query) use ($allergens) {
                $query->whereDoesntHave('allergens', function($query) use ($allergens) {
                    $query->whereIn('id', $allergens);
                });
            });
        }

        if ($search) {
            $query->whereHas('products', function($query) use ($search) {
                $query->where('title', 'like', '%'.$search.'%');
            });
        }

        $categories = $query->orderBy('sort_order', 'asc')->get();

        return $categories;
    }

    public function getfilteredCollections($categories = [], $allergens = [], $search = '') {

        $query = Collection::whereHas('products', function($query) {
            $query->where('is_published', 1);
        });
        // dd(Collection::has('products')->get());

        if ($categories) {
            $query->whereHas('products', function($query) use ($categories) {
                $query->whereHas('categories', function($query) use ($categories) {
                    $query->whereIn('id', $categories);
                });
            });
        }

        if ($allergens) {
            $query->whereHas('products', function($query) use ($allergens) {
                $query->whereDoesntHave('allergens', function($query) use ($allergens) {
                    $query->whereIn('id', $allergens);
                });
            });
        }

        if ($search) {
            $query->whereHas('products', function($query) use ($search) {
                $query->where('title', 'like', '%'.$search.'%');
            });
        }

        $collections = $query->orderBy('sort_order', 'asc')->get();
        return $collections;
    }

    public function getfilteredAllergens($categories = [], $collections = [], $search = '') {

        $query = Allergen::whereHas('products', function($query) {
            $query->where('is_published', 1);
        });

        if ($categories) {
            $query->whereHas('products', function($query) use ($categories) {
                $query->whereHas('categories', function($query) use ($categories) {
                    $query->whereIn('id', $categories);
                });
            });
        }

        if ($collections) {
            $query->whereHas('products', function($query) use ($collections) {
                $query->whereHas('collections', function($query) use ($collections) {
                    $query->whereIn('id', $collections);
                });
            });
        }

        if ($search) {
            $query->whereHas('products', function($query) use ($search) {
                $query->where('title', 'like', '%'.$search.'%');
            });
        }

        $allergens = $query->orderBy('sort_order', 'asc')->get();

        return $allergens;
    }

    public function onFilterProducts(){
        $data = post();
        // dd(input());
        if(!isset($data['page'])) $data['page'] = 0;
        if(!isset($data['cats'])) $data['cats'] = [];
        if(!isset($data['collections'])) $data['collections'] = [];
        if(!isset($data['allergens'])) $data['allergens'] = [];
        if(!isset($data['search'])) $data['search'] = false;
        return $this->filteredProducts(intval($data['page']), $data['cats'], $data['collections'], $data['allergens'], $data['search']);
    }

    public function getCatIdBySlug($slug){
        return Category::where('is_published', 1)->where('slug', $slug)->first()->id;
    }

    public function getCollectionIdBySlug($slug){
        return Collection::where('slug', $slug)->first()->id;
    }

    public function getProduct($slug){
        return Product::with('title_image')->with('categories')->where('slug', $slug)->where('is_published', 1)->first();
    }
}
