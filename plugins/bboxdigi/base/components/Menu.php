<?php namespace Bboxdigi\Base\Components;

use Cms\Classes\ComponentBase;
use Cms\Classes\Page;

class Menu extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Menu Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function on_Run()
    {
        dd(
            $this->page->id
        );
    }

    public function isAct($group,$class='is-active')
    {
        $found = strpos($this->page->id, strtolower($group) );
        return ($found !== false)
            ? $class
            : '';
    }
}
