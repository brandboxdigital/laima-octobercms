<?php namespace Bboxdigi\Content\Components;

use Cms\Classes\ComponentBase;
use Cms\Classes\Page;
use Bboxdigi\Content\Models\Text;
use Bboxdigi\Content\Models\Slide;
use Bboxdigi\Content\Models\Shop;
use RainLab\Translate\Classes\Translator;
use Mail;
use Flash;
use Cache;

class FormComponent extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Form Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun() {
    }

    public function onSendMail()
    {
        $data = post();

        $messages = [
            'email.required' => 'Epasta lauks ir obligāts!',
            'email.email' => 'Epastam jābūt korektam!',
            'subject.required' => 'Temata lauks ir obligāts!',
            'text.required' => 'Ziņas teksta lauks ir obligāts!',
        ];

        $validator = \Validator::make($data,
            [
                'email' => 'required|email',
                'subject' => 'required',
                'text' => 'required',
            ],
            $messages
        );

        if ($validator->fails()) {
            throw new \ValidationException($validator);
        }

        $recipients = [
            "eksports" => "export@orkla.lv",
            "iepirkumi" => "procurement@orkla.lv",
            "vakances" => "konkurss@orkla.lv",
            "produktu-kvalitate" => "kvalitate@orkla.lv",
            "marketings-sadarbiba" => "sadarbiba@orkla.lv",
        ];

        $defRecipient = 'laima@orkla.lv';

        $to = (isset($recipients[$data['subject']]))
            ? $recipients[$data['subject']]
            : $defRecipient;

        $email = 'adx@adx.lv';

        Mail::send('bboxdigi.content::mail.message', $data, function($message) use ($email, $to) {
            $message->to($to);
        });
    }

}
