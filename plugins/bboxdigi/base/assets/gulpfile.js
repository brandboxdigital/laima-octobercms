/* ==================================================== */
var log         = require('fancy-log');
/* ==================================================== */
var gulp        = require('gulp');
var sass        = require('gulp-sass');
var concat      = require('gulp-concat');
var babel       = require("gulp-babel");
var browserSync = require('browser-sync').create();
var bourbon     = require("bourbon").includePaths;
var autoprefix  = require("gulp-autoprefixer");
var sourcemaps  = require('gulp-sourcemaps');
var minify      = require("gulp-babel-minify");
var svgSprite   = require("gulp-svg-sprites");


/* ==================================================== */
/*                    FILE CONFING                      */
/* ==================================================== */
var css_assets  = 'css/';
var scss_assets = 'scss/';
var js_assets   = 'js/';
var img_assets  = 'img/';
/* ==================================================== */



var sassPaths = [
  scss_assets,
  bourbon
];

var jsPaths = [
  'node_modules/@babel/polyfill/dist/polyfill.js',
  js_assets + 'modules/**/*.js', 
  js_assets + 'main.js',
];

var svgconfig = {
  cssFile: scss_assets + "basic/_svg-sprites.scss",
  svg: {
      sprite: img_assets + "icon-sprite.svg",
      symbols: img_assets + "icon-sprite.svg",
      defs: img_assets + "icon-sprite.svg",
  },
  preview: {
      sprite: img_assets + "icon-sprite-preview.html",
      symbols: img_assets + "icon-sprite-preview.html",
      defs: img_assets + "icon-sprite-preview.html",
  },

  mode: "symbols",
};

/**
 * ===============================================================
 * Compiling svg sprites
 */
gulp.task('sprites', function () {
    return gulp.src(img_assets + 'icon-sprite-saurce/*.svg')
      .pipe(svgSprite(svgconfig))
      .pipe(gulp.dest("./"));
});

/**
 * ===============================================================
 * Compiling sass files
 */
gulp.task("sass-build--env-dev", function () {
  return gulp.src(scss_assets + 'main.scss')
    .pipe(sass({
          sourcemaps: true,
          includePaths: sassPaths,
      })
      .on('error', sass.logError)
    )
    .pipe(autoprefix("last 2 versions"))
    .pipe(gulp.dest(css_assets))
    .pipe(browserSync.stream());
});

gulp.task("sass-build", function () {
  return gulp.src(scss_assets + 'main.scss')
    .pipe(sass({
          sourcemaps: true,
          includePaths: sassPaths,
          outputStyle: 'compressed',
      })
      .on('error', sass.logError)
    )
    .pipe(autoprefix("last 4 versions"))
    .pipe(gulp.dest(css_assets))
});

/**
 * ===============================================================
 * Compiling js files and running then trough babel
 */
gulp.task('js-vendor', function(done) {
  return gulp.src([js_assets+'vendor/*.js'])
    .pipe(concat('vendor.js'))
    // .pipe(babel({presets: ["@babel/env"]}))
    .pipe(gulp.dest(js_assets+'min'))
    .pipe(browserSync.stream());
});

gulp.task('js-build--env-dev', function(done) {
  return gulp.src(jsPaths)
    .pipe(concat('main.js'))
    .pipe(babel({presets: ["@babel/env"]}))
    .pipe(gulp.dest(js_assets+'min'))
    .pipe(browserSync.stream());
});

gulp.task('js-build', function() {

  return gulp.src(jsPaths)
    .pipe(concat('main.js'))
    .pipe(babel({presets: ["@babel/env"]}))
    .pipe(sourcemaps.init())
    .pipe(minify({
      mangle: {
        keepClassName: true,
        keepFnName: true
      }
    }))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(js_assets + 'min'))
});


/**
 * ======================
 * HTML + TWIG for static sites...
 */
gulp.task('html-compile', function () {
    
    return gulp.src('twig/pages/*.twig')
      .pipe(twig({
          base: './twig'
      }))
      .pipe(gulp.dest('./'))
      .pipe(browserSync.stream());
});

/**
 * ===============================================================
 * Calling Browser Sync And watching for changes
 */
gulp.task('watcher', function() {
  
  /**
   * Sprite Watcher
   */
  // gulp.watch([
  //   img_assets + 'icon-sprite-saurce/*.svg'
  // ], gulp.series(['sprites']));

  /**
   * SCSS Watcher
   */
  gulp.watch([
    scss_assets + '**/*.scss'
  ], gulp.series(['sass-build--env-dev']));

  /**
   * JS Vendor Watcher
   */
  // gulp.watch([
  //   js_assets+'vendor/**/*.js'
  // ], gulp.series(['js-vendor']));

  /**
   * JS Watcher
   */
  gulp.watch([
    js_assets+'main.js',
    js_assets+'modules/**/*.js'
  ], gulp.series(['js-build--env-dev']));

  /**
   * HTML Watcher
   */
  // gulp.watch([
  //   '**/*.html',
  //   '**/*.htm'
  // ]).on('change', browserSync.reload);

  // gulp.watch(['twig/**/*.twig'], gulp.series(['html-compile']));

});


gulp.task( "default", gulp.series([
  // 'sprites', 
  'sass-build--env-dev', 
  // 'js-vendor', 
  'js-build--env-dev', 
  'watcher'
]) );
gulp.task( "build", gulp.series([
  // 'sprites',
  'sass-build',
  // 'js-vendor',
  'js-build'
]) );

