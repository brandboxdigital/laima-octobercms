<?php namespace Bboxdigi\Base\Console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use Bboxdigi\Base\Plugin;

class FreshInstall extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'bboxdigi:fresh-install';

    /**
     * @var string The console command description.
     */
    protected $description = 'Call this on configured October to prepare october for development';

    /**
     * Execute the console command.
     * @return void
     */
    public function handle()
    {
        if (!$this->confirm('Do you wish to continue? This will resset all migrations! [yes|no]')) {
            $this->info('   Canceled...');
            return false;
        }
        
        try {
            $this->call('october:down', ['--force' => true]);
        } catch (\Exception $e) {
            
        }
        $this->call('key:generate', ['--force' => true]);

        $this->call('october:up');
        $this->call('october:update', ['--force' => true]);

        $this->removeDemoStuff();

        $this->installRequiredPlugins();

        $this->updateAdminUser();

        $this->printFooter();
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];

        return [
            ['example', InputArgument::REQUIRED, 'An example argument.'],
        ];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [];

        return [
            ['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
        ];
    }

    // ----------------------------------------------------------------------

    protected function removeDemoStuff()
    {
        // Remove demo theme
        $this->call('october:fresh');

        // Remove demo plugin
        $this->call('plugin:remove', ['name' => 'October.Demo', '--force' => true]);
    }

    protected function updateAdminUser() 
    {
        // use Backend\Models\User;
        $admin = \Backend\Models\User::find(1);

        if ($admin->is_superuser 
                && $admin->email == \Backend\Database\Seeds\SeedSetupAdmin::$email 
                && $admin->login == \Backend\Database\Seeds\SeedSetupAdmin::$login 
                && $admin->first_name == \Backend\Database\Seeds\SeedSetupAdmin::$firstName
                && $admin->last_name == \Backend\Database\Seeds\SeedSetupAdmin::$lastName) {
            $admin->email = 'dev@brandbox.digital';
            $admin->login = 'bboxdigi';
            $admin->password = 'Viss@11bumbas';
            $admin->password_confirmation = 'Viss@11bumbas';
            $admin->first_name = 'Brandbox';
            $admin->last_name = 'Digital';

            $admin->save();
        }
    }

    protected function installRequiredPlugins() 
    {
        $plugin = new Plugin(app());
        
        foreach ($plugin->require as $require) {
            $this->call('plugin:install', ['name' => $require]);
        }
    }

    protected function printFooter()
    {
        $this->info(' ');
        $this->info('Finished!');
        $this->info(' ');
        $this->info('NB! Please remember to rename theme folder and set correct theme metadata (title, decription, img, etc.)');
        $this->info(' ');
    }
}
