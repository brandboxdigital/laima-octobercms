<?php namespace Bboxdigi\Content\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateBboxdigiContentPackagehistories extends Migration
{
    public function up()
    {
        Schema::create('bboxdigi_content_packagehistories', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('title')->nullable();
            $table->text('body')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('year')->nullable()->unsigned();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('bboxdigi_content_packagehistories');
    }
}
