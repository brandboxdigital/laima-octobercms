<?php namespace Bboxdigi\Content\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBboxdigiContentShops extends Migration
{
    public function up()
    {
        Schema::table('bboxdigi_content_shops', function($table)
        {
            $table->string('city')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('bboxdigi_content_shops', function($table)
        {
            $table->dropColumn('city');
        });
    }
}
