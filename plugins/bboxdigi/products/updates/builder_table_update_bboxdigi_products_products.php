<?php namespace Bboxdigi\Products\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBboxdigiProductsProducts extends Migration
{
    public function up()
    {
        Schema::table('bboxdigi_products_products', function($table)
        {
            $table->string('nutr1')->nullable();
            $table->string('nutr2')->nullable();
            $table->string('nutr3')->nullable();
            $table->string('nutr4')->nullable();
            $table->string('nutr5')->nullable();
            $table->string('nutr6')->nullable();
            $table->string('nutr7')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('bboxdigi_products_products', function($table)
        {
            $table->dropColumn('nutr1');
            $table->dropColumn('nutr2');
            $table->dropColumn('nutr3');
            $table->dropColumn('nutr4');
            $table->dropColumn('nutr5');
            $table->dropColumn('nutr6');
            $table->dropColumn('nutr7');
        });
    }
}
