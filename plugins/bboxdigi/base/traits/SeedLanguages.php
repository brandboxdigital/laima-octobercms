<?php namespace Bboxdigi\Base\Traits;

use DbDongle;
use Exception;
use GuzzleHttp\Client;
use October\Rain\Support\Collection;

trait SeedLanguages
{
    // public $wp_api_url = '';
    /*
     * Constructor Example
     */
    public static function bootSeedLanguages()
    {
        
    }

    public function getItem($id) 
    {
        $wpjson = $this->wp_api_url;

        // $ret = new Collection;

        $c_id = str_replace(['http', ':'. 'www', '/'], '', $wpjson) . $id;

        $cached_data = \Cache::remember( $c_id, 10, function() use ($wpjson, $id) {
            $client = new \GuzzleHttp\Client();

            $res = $client->request('GET', $wpjson.'/'.$id, [
                'query' => [],
            ]);
            $data = $res->getBody()->getContents();

            return $data;
        });

        return json_decode($cached_data);
    }

    public function translateContent($models)
    {
        $models->each(function($item) {
            try {
                foreach (json_decode($item->old_languages) as $code => $old_post_id) {
                    $this->translateTheItem($code, $old_post_id, $item);
                }
            } catch (\Exception $e) {
                $class = get_class($this);
                \Log::error(["translateContent::$class", $e->getMessage()]);
            }
        });
    }
}
