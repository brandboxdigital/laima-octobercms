<?php namespace Bboxdigi\Products\Models;

use Model;

use Cms\Classes\Page as CmsPage;
use Cms\Classes\Theme;

/**
 * Model
 */
class Collection extends Model
{
    use \October\Rain\Database\Traits\Validation;

    use \October\Rain\Database\Traits\SoftDelete;
    use \October\Rain\Database\Traits\Sluggable;
    use \October\Rain\Database\Traits\Sortable;

    /**
     * @var array Generate slugs for these attributes.
     */
    protected $slugs = ['slug' => 'title'];

    public $implement = ['@RainLab.Translate.Behaviors.TranslatableModel'];

    public $translatable = [
        'title',
    ];

    protected $dates = ['deleted_at'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'bboxdigi_products_collections';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $belongsToMany = [
        'products' => [
            'Bboxdigi\Products\Models\Product',
            'table' => 'bboxdigi_products_coll_prod'
        ],
    ];

    public $attachOne = [
        'title_image' => 'System\Models\File',
    ];

    public static function getMenuTypeInfo($type)
    {
        $result = [
            'dynamicItems' => true
        ];
        $theme = Theme::getActiveTheme();
        $pages = CmsPage::listInTheme($theme, true);
        $result['cmsPages'] = $pages;

        return $result;
    }

    public static function resolveMenuItem($item, $url, $theme)
    {
        $result = [
            'items' => []
        ];

        $page = CmsPage::loadCached($theme, $item->cmsPage);

        $defaultLocale = \RainLab\Translate\Models\Locale::getDefault()->code;

        $alternateLocales = array_keys(\RainLab\Translate\Models\Locale::listEnabled());

        $collections = self::get();
        foreach ($collections as $col) {
            $col->_type = 'collection';
            
            $pageUrl = \Utopigs\Seo\Models\Sitemap::getPageLocaleUrl($page, $col, $defaultLocale, [
                'type' => '_type',
                'slug' => 'slug'
            ]);

            $colItem = [
                'title' => $col->title,
                'url'   => $pageUrl,
                'mtime' => $col->updated_at,
            ];
            if (count($alternateLocales) > 1) {
                foreach ($alternateLocales as $locale) {
                    $colItem['alternate_locale_urls'][$locale] = \Utopigs\Seo\Models\Sitemap::getPageLocaleUrl($page, $col, $locale, [
                        'type' => '_type',
                        'slug' => 'slug'
                    ]);
                }
            }

            $result['items'][] = $colItem;
        }
        return $result;
    }
}
