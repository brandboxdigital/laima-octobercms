<?php namespace Bboxdigi\Content\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;
use Bboxdigi\Content\Models\SustainabilityItem;
use Bboxdigi\Content\Models\SustainabilityGoal;

class SustainabilityItemsAndGoalsSortableUpdate extends Migration
{
    public function up()
    {
        Schema::table('bboxdigi_content_sustainability_items', function($table)
        {
            $table->integer('sort_order')->nullable()->unsigned()->default(0);
        });

        $items = SustainabilityItem::all();
        foreach ($items as $item) {
            $item->sort_order = $item->id;
            $item->save();
        }

        Schema::table('bboxdigi_content_sustainability_goals', function($table)
        {
            $table->integer('sort_order')->nullable()->unsigned()->default(0);
        });

        $goals = SustainabilityGoal::all();
        foreach ($goals as $goal) {
            $goal->sort_order = $goal->id;
            $goal->save();
        }
    }
    
    public function down()
    {
        Schema::table('bboxdigi_content_sustainability_items', function($table)
        {
            $table->dropColumn('sort_order');
        });
        Schema::table('bboxdigi_content_sustainability_goals', function($table)
        {
            $table->dropColumn('sort_order');
        });
    }
}