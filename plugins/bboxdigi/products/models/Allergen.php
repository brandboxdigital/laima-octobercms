<?php namespace Bboxdigi\Products\Models;

use Model;

/**
 * Model
 */
class Allergen extends Model
{
    use \October\Rain\Database\Traits\Validation;

    use \October\Rain\Database\Traits\SoftDelete;
    use \October\Rain\Database\Traits\Sluggable;
    use \October\Rain\Database\Traits\Sortable;

    /**
     * @var array Generate slugs for these attributes.
     */
    protected $slugs = ['slug' => 'title'];

    public $implement = ['@RainLab.Translate.Behaviors.TranslatableModel'];

    public $translatable = [
        'title',
    ];

    protected $dates = ['deleted_at'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'bboxdigi_products_allergens';

    /**
     * @var array Validation rules
     */

    public $belongsToMany = [
        'products' => [
            'Bboxdigi\Products\Models\Product',
            'table' => 'bboxdigi_products_aller_prod'
        ],
    ];

    public $rules = [
    ];
}
