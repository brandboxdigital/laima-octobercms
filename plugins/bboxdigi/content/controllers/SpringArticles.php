<?php namespace Bboxdigi\Content\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Spring Articles Back-end Controller
 */
class SpringArticles extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
        'Backend\Behaviors\ReorderController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';
    public $reorderConfig = 'config_reorder.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Bboxdigi.Content', 'bboxdigi-laima-content', 'bboxdigi-laima-content-spring-articles');
    }
}
