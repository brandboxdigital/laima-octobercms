<?php namespace Bboxdigi\Content\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBboxdigiContentExtraContents2 extends Migration
{
    public function up()
    {
        Schema::table('bboxdigi_content_extra_contents', function($table)
        {
            $table->string('balta_sarma_title')->nullable();
            $table->text('balta_sarma_items')->nullable();
            $table->text('balta_sarma_bottom_text')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('bboxdigi_content_extra_contents', function($table)
        {
            $table->dropColumn('balta_sarma_title');
            $table->dropColumn('balta_sarma_items');
            $table->dropColumn('balta_sarma_bottom_text');
        });
    }
}
