<?php namespace Bboxdigi\Articles;

use System\Classes\PluginBase;

use Event;
use bboxdigi\articles\models\Article;
use bboxdigi\articles\models\Recipe;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
      return [
          'Bboxdigi\Articles\Components\ArticleComponent' => 'Articles',
      ];
    }


    public function registerSettings()
    {
    }

    public function boot()
    {
        /*
         * Register menu items for the RainLab.Pages plugin
         */
        Event::listen('pages.menuitem.listTypes', function() {
            
            return [
                'all-articles'       => 'All News Articles',
                'all-recipes'       => 'All Recipes'
            ];
        });

        Event::listen('pages.menuitem.getTypeInfo', function($type) {
            
            if ($type == 'all-articles') {
                return Article::getMenuTypeInfo($type);
            } 
            elseif ($type == 'all-recipes'){
                return Recipe::getMenuTypeInfo($type);
            }
        });

        Event::listen('pages.menuitem.resolveItem', function($type, $item, $url, $theme) {
            
            if ($type == 'all-articles') {
                return Article::resolveMenuItem($item, $url, $theme);
            }
            elseif ($type == 'all-recipes') {
                return Recipe::resolveMenuItem($item, $url, $theme);
          }
        });
    }
}
