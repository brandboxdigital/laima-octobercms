<?php namespace Bboxdigi\Content\Models;

use Model;

/**
 * Model
 */
class PackageHistory extends Model
{
    use \October\Rain\Database\Traits\Validation;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'bboxdigi_content_packagehistories';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $attachOne = [
        'image' => 'System\Models\File',
    ];
}
