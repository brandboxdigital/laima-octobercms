(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


(lib.AnMovieClip = function(){
	this.actionFrames = [];
	this.gotoAndPlay = function(positionOrLabel){
		cjs.MovieClip.prototype.gotoAndPlay.call(this,positionOrLabel);
	}
	this.play = function(){
		cjs.MovieClip.prototype.play.call(this);
	}
	this.gotoAndStop = function(positionOrLabel){
		cjs.MovieClip.prototype.gotoAndStop.call(this,positionOrLabel);
	}
	this.stop = function(){
		cjs.MovieClip.prototype.stop.call(this);
	}
}).prototype = p = new cjs.MovieClip();
// symbols:



(lib.element_2 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#D3BD93","#966A40"],[0,0.529],-2685.8,-289.8,3454.4,823.3).s().p("EBBTLONQk8jGoui6QocikhkgoQlaiKiciWQkckQAAngQAAlsCqnwQBkkmEQpqQEMpkBmkwQCqn2AAl2QAAiIhigsQgugUhcAAQlEAArOGCQkmCcnEEcQ2lOGlcC6QsWGmnqAAQrWAAmQmIQlelWAAoEQAWkgC+mIQF4sQNEoaQHIkmOOl2QL2k2FFhUQB6ggJ8iAQKuiKHEhsQYSl2JKmWQMUokAAqKQAAjYhMjqQhkk0j2maQg6hgiejGIqktAQhGhWlmoMQlSnwhOh8QpUuuj4wAQjAsUAAugQAAqaAsk+QAgjWCKoAQDKreHspuQFinAEqj4QHGl8F4mEQNMtmE0w4QDisYAw3WQAiv4mSoiQk2mimOAAQjoAAi6B8QiKBcoeGIQkyDMn0CkQpaDEoeAGQxiAMrHzCQm4rwk81CQhimojqnSQkio+lkk+Qk4kWrwkaQsykQkChkQmgiSl2iMQrMkMp4kiQ+quCzUxiQpGoOo6s2QrcwgAAqWQAAsuMimeQL+mKWWAAUAdmAAAAjGASGQMqGiPAJwQA8AmWGO4QKEGyPlKkQLyH0HAD6QIWEqGqCCQG6CGHYAAQN0AAFekoQEWjsAAn+QACq23ctMQm0j2rElOQqMkyhqhCQoGlAjckUQkIlMACm2QAEqCPwqwQGAkIKEleISYpwQXmsoKkpcQPatygcvOQgcwTpirmQo0qqzIpkQmcjOighmQjeiOhoiaQhsiggokCQgijkAAnAQAAqcDKqSQByl2FIruQE8rWB+m2QDKq8AAriQgCs0pqrQQjikElkk0QmYlQi6icQpmoEnCqsQmapsAAksQAAikBEhsQBkigDmAAQFWAAQyJIQIuEuLGGiQMWGYOYIIQJkE8G2AAQFmAAD+joQD0jgAAkoQAAskwQxwQkYkyjmjIQlUkYk0kCQosm4n+mYQvssglSkmQnGmMj4mAQlkoqAApiQAAmWECqkQCOlyAkh0QBQkCAAiyQAAmGkooGQg6hkmqpwQjgkqjUkeQmko0hkiyQkwoWiMl6QjspwAApeQAAjQAykOQAUhwBQleQAeiCBCikQAWg2BkjoQBojwDOlGQEMmsE2lWQG2nmFUk6QBohgIinWQJgoMDMiQQG2k4E2jyQjElGhImgQgykaAAmgQAAsQGiwIQHSx+NQw4QBChUDekKQDgkUCYjaQHUqiAAmuQAAnAkKjmQkCjgnqAAQlWAAmOBiQjuA8nUCeQnUCejwA8QmQBilYAAQt4AAkys4QhokYhEnSIhqrMIhooQQh4m0jslAQrGvC6vBYQh4AGlKAoQlmAql0AYQswAypInKQjqi0iIjeQh+jMAAiiQAAncKkoUQDmi0JMlYQEmisKsjCQLijSGoAAQK1AAJ8CuQJWCmL0F8QCABAFMC6QEkCkCwBSQIyEGIgAAQF+AAHMjeQIYkCGknUUAROgTOgCIgg8QAMBSAkF2QAqGkAABkQAAX6sSPmQlkHEnOD2QnOD0nwAAQs+AAnCiiQicg6i8hoQkSiag2gaQqumGnsjIQt2lmvnAAQmAAApgCmQsGDSpUFwQl0DmkOFiQkIFaAQDaQA4LCQeB+QIyBCNuhIQW/AAOAK0QMMJcBqOCQAQCGAkHUQAyHYBkFKQEwPoM0gIQFcgEHSiKQD0hIJmjmQI4jUFIhYQH8iIGmAAQJQAAEeEiQDCDEAADeQAAEck8IIQkaHQpsMUQ2wc2nsUCQpYYYJiSKQFQjuGsk4QG8kuQ+pyQGOjmIkioQJOi4GqAAQLiAAHiFGQJ0GqAAOCQAAIQkOIMQjQGWmgHeQjyEWq0EIQrYEWpGAAQvMgCssqUQoam2pwuKQxcNqpAIyQluFkq+MAQmqHEk8IcQo+PUAAOsQAASAJWOMQC0ESEeE+QEiFCAoA8QA+BcCiC0QCyDGBiCIQDkE6BcEAQBqEmAAGOQAADihcEIQg0CWiWFGQkmJ8AAGGQAAGSEUHIQDaFsHMHqQFEFaLQIoQGWE4MYJIQP8MCLeN+QNeQaAALGQAAFwjsD8QjyEClwAAQpGAAwIoOQkciSmijwQlYjEg8gYQpYkumYjCQsElyl2AAQkAAAiKCsQhuCKAGCmQAKEEGyG+QEEEKKsJ8QJwJkE4H6QGmKsgEKeQgCJCkoMoQioHKnCPGQleLyhCGyQhiKCAAFAQAAGuBkEIQBYDuDECiQCQB2E2CUQHmDsByA+QVALgH0LqQGgJugIOtQgGI+hMESQh6G2mKGoUgN8APAgxoAaOQoEEQjyEQQk6FgAAHiQAAJQJ8H6QF2EqQAIIQP0ICGCE8QJ8IEAAJmQAALYqQF0QnqEWpsAAQsWAA1QrWQookmteoMI6bwUQkEigx4sAQvWqUqel+Ugg0gSygYiAAAQ2IAAq+GuQqCGKAALSQAAOqMoQgQN2SGaGQEQMOHkTaGwQFQB2KSDaQI6DEFyCeQP4G0JCJwQLIMAEcToQEkUMJtL8QKCMWM2AAQHcAAHOjeQE4iUHAlSQIEmECohiQF6jeFIAAQNcAAFiQwQDyLYAAS+QAASqmOQQQoUVuyuO6QyQOimWPyQkYK6AARiQAAQaFyPsQE4NQJ8PAQFMH2GsIGQDmEYF2HAQEYFmB6EgQCQFYAAGmQAAPOyEKSQmWDoo4DIQi4BCp4DCQl+B2oIBkQpMBmk0A+QxpDkvmH0QtMGonCGiQpiI4AAJ8QAAGwE0EaQFOEwJCAAQI6AAQ6p6QE0i2HzlEQHQkuCChKQI+loFijMQKQl0E+AAQEuAACEEAQBgC8AAEyQAAGoiGIqQheGAi+IoQjaJ8hGD+QhSEkAAC2QAAG6EsE8QDkDwHCDMIKgEwQKOFOIiG2UAaeAVMAFIAgGUgJagk4gpYgZ+gECJXoLuQrWBau2ImQo6FKyGNEQOOQIJmE4QEcCQFCA2QDuAqGiAIQEqAGFqhYQHSh0F0jwQQCqWAC1iQAAouoMjuQlXidoNAAQjvAAkVAhg");
	this.shape.setTransform(1134.5,4998.7);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,2269,9997.4);


(lib.element_1 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#966A40").s().p("AkbEcQh0h0AAioQAAinB0h0QB0h0CnAAQCoAAB0B0QB0B0AACnQAACoh0B0Qh0B0ioAAQinAAh0h0g");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-40,-40,80,80);


(lib.element_4 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_3
	this.instance = new lib.element_1("synched",0);
	this.instance.setTransform(1878.45,4,0.1,0.1);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(2).to({_off:false},0).to({regX:0.1,regY:0.2,scaleX:0.46,scaleY:0.46,guide:{path:[1878.7,4.1,1890,109.2,1867.6,197,1846.6,278.2,1799.2,336.8,1779.9,360.6,1757.7,379]}},40,cjs.Ease.get(-1)).to({regX:0.8,regY:0.8,scaleX:0.26,scaleY:0.26,guide:{path:[1757.7,378.9,1729,402.6,1695.6,417.2,1638,442.4,1579.4,435.4,1517.6,428,1458.6,396.4,1458.6,396.4,1458.6,396.4]}},18).to({guide:{path:[1458.6,396.4,1423.2,376.4,1402.2,365.2,1364.4,345,1326.4,329.4,1255.2,300.6,1188,292.8,1121,285,1060.6,298.6,1018.5,307.7,981.8,320.6]}},12,cjs.Ease.get(-0.5)).to({regX:0.2,regY:0.3,scaleX:0.5,scaleY:0.5,guide:{path:[981.8,320.7,896.6,350.6,840,400.6,785.4,447.9,786,490.1]}},10).to({regX:0.8,regY:0.6,scaleX:0.25,scaleY:0.25,guide:{path:[786,490.1,786.5,523.3,821,553.4,881.6,600.8,980.6,591,1151.4,571.8,1243,616,1255.7,622,1267.1,629]}},20,cjs.Ease.get(0.4)).to({regX:1.2,regY:1,scaleX:0.37,scaleY:0.37,guide:{path:[1267,629,1312.1,656.7,1338.8,700.2,1358.9,733.1,1368.9,776.1]}},10).to({regX:1.5,regY:1.2,scaleX:0.3,scaleY:0.3,guide:{path:[1369,776,1375.5,804.1,1377.8,836.4,1385.4,938.2,1419,988,1430.9,1005.6,1446.1,1016.8]}},16,cjs.Ease.get(-0.5)).to({regX:0,regY:0.3,scaleX:0.2,scaleY:0.2,guide:{path:[1446,1016.8,1474.5,1037.9,1514.8,1036.2,1570.6,1034,1631.2,1012,1634,1011,1636.7,1010]}},8).to({regY:0,scaleX:1.0999,scaleY:1.0999,guide:{path:[1636.8,1010,1716,981.6,1742.6,975.6,1823.5,956.9,1870.8,966.5,1918.2,976.1,1932.1,1014]}},20).to({regX:0.3,regY:0.1,scaleX:0.4,scaleY:0.4,guide:{path:[1932.1,1014,1934.5,1020.4,1935.8,1027.6,1940.4,1049.8,1924.2,1087.4,1909.1,1122.7,1877,1168.3]}},10).to({regX:0.6,regY:0.4,scaleX:0.26,scaleY:0.26,guide:{path:[1876.9,1168.3,1874.2,1172.1,1871.4,1176,1807.8,1256.4,1773,1303.6,1710.8,1388.2,1667.6,1483.2,1623.2,1582.4,1620.8,1668.6,1618.2,1756.6,1660.2,1821.6,1667.3,1831.9,1674.5,1841.7]}},26,cjs.Ease.get(-0.5)).to({regX:0.8,scaleX:0.73,scaleY:0.73,guide:{path:[1674.6,1841.7,1751.2,1945.5,1838.4,1986.8,1892.4,2012.4,1948.6,2013.3]}},12).to({regX:0.1,regY:0.1,scaleX:0.88,scaleY:0.88,guide:{path:[1948.6,2013.2,1990.3,2013.9,2033.2,2000.8,2113.2,1979.8,2161,1934.2,2207.8,1889.6,2227.4,1818.2,2230.8,1804.7,2232.9,1792.1]}},14).to({scaleX:0.4,scaleY:0.4,guide:{path:[2232.9,1792.1,2256,1655.2,2132.4,1626.4,2025.4,1605.6,1902,1665.6,1795.4,1719.2,1668.6,1813.8]}},20).wait(1).to({regX:0,regY:0,scaleX:0.414,scaleY:0.414,x:1640.6182,y:1834.5131},0).wait(1).to({scaleX:0.4279,scaleY:0.4279,x:1615.6085,y:1853.608},0).wait(1).to({scaleX:0.4415,scaleY:0.4415,x:1590.8992,y:1872.9655},0).wait(1).to({scaleX:0.4548,scaleY:0.4548,x:1566.6861,y:1892.4495},0).wait(1).to({scaleX:0.4679,scaleY:0.4679,x:1542.9751,y:1912.0676},0).wait(1).to({scaleX:0.4808,scaleY:0.4808,x:1519.7757,y:1931.8253},0).wait(1).to({scaleX:0.4935,scaleY:0.4935,x:1497.0948,y:1951.7318},0).wait(1).to({scaleX:0.5059,scaleY:0.5059,x:1474.9441,y:1971.7929},0).wait(1).to({scaleX:0.518,scaleY:0.518,x:1453.3387,y:1992.0122},0).wait(1).to({regX:0.2,regY:0.4,scaleX:0.53,scaleY:0.53,x:1432.35,y:2012.45},0).wait(1).to({regX:0,regY:0,x:1411.6867,y:2032.9287},0).wait(1).to({x:1391.9594,y:2053.3883},0).wait(1).to({x:1373.8385,y:2072.9973},0).wait(1).to({x:1357.7049,y:2091.5525},0).wait(1).to({x:1342.1892,y:2110.5803},0).wait(1).to({x:1327.4765,y:2129.9412},0).wait(1).to({x:1313.6062,y:2149.6687},0).wait(1).to({x:1300.6487,y:2169.7609},0).wait(1).to({x:1288.6779,y:2190.2122},0).wait(1).to({regX:0.4,regY:0.3,x:1277.85,y:2211.2},0).to({regX:0.3,scaleX:0.19,scaleY:0.19,guide:{path:[1277.9,2211.2,1245.2,2277.3,1239,2342.6,1231.6,2472,1307,2590.6,1316.3,2603.9,1326.2,2617.3]}},20).to({scaleX:0.34,scaleY:0.34,guide:{path:[1326.3,2617.3,1352.1,2652.1,1381.8,2687.4,1422.8,2737.6,1442.8,2770,1444.6,2772.9,1446.3,2775.7]}},14).to({regX:0.6,regY:0,scaleX:0.28,scaleY:0.28,guide:{path:[1446.3,2775.7,1463.5,2805,1470.2,2832.2,1477.6,2862.4,1472.2,2892.8,1467.2,2919.8,1449.4,2958.6,1405.4,3048,1425,3117.4,1433,3144.7,1451.2,3173.6]}},28).to({guide:{path:[1451.3,3173.6,1458.3,3184.7,1466.8,3196,1498.4,3238,1549,3280.2,1639.4,3356,1759.4,3446.2,1847.8,3513.4,1921.4,3604.2,1947.7,3638.7,1964.3,3668.7]}},54).to({regX:0,scaleX:0.5,scaleY:0.5,guide:{path:[1964.3,3668.7,2012.5,3755.6,1980.6,3804.6,1972.1,3817.1,1960.9,3825.4]}},10).to({regX:0.2,regY:0.2,scaleX:0.25,scaleY:0.25,guide:{path:[1960.9,3825.4,1907.6,3864.3,1791.4,3809,1733.5,3777.3,1686.3,3751.8]}},12).to({regX:0,regY:0,scaleX:1.5499,scaleY:1.5499,guide:{path:[1686.2,3751.8,1618,3715,1572.2,3691.4,1397.8,3601.2,1365.6,3625.8,1363.7,3627.2,1362,3628.8]}},16,cjs.Ease.get(0.3)).to({regX:0.2,regY:0.2,scaleX:0.25,scaleY:0.25,guide:{path:[1362.1,3628.9,1344.7,3645.7,1354.5,3678.7,1364.3,3711.7,1401.4,3761,1432.6,3795.8,1482.6,3839.4,1537.8,3887.8,1560.2,3911,1575.2,3926.8,1587.6,3942.9]}},20,cjs.Ease.get(-0.3)).to({regX:0.3,regY:0.1,scaleX:0.35,scaleY:0.35,guide:{path:[1587.6,3942.8,1648.7,4021.9,1648.2,4109.4,1648,4127.9,1645.4,4147.6]}},8).to({regX:0.7,regY:0,scaleX:0.16,scaleY:0.16,guide:{path:[1645.3,4147.6,1637.1,4211.4,1605.4,4287.8,1597.2,4307.6,1576.2,4355.4,1558.2,4396.8,1549.2,4419,1517.4,4495,1513,4569.4,1510.8,4608.7,1512.7,4636.9]}},26,cjs.Ease.get(0.3)).to({regX:-0.1,regY:-0.1,scaleX:0.48,scaleY:0.48,guide:{path:[1512.7,4636.9,1515.4,4676.8,1526.2,4694.6,1544.4,4726,1619.8,4759.8,1745.8,4820.6,1805.8,4892.2,1876.6,4972.4,1871.8,5095,1871.8,5118.5,1865.5,5141.4]}},54,cjs.Ease.get(-0.4)).to({regX:0.3,regY:-0.4,scaleX:0.59,scaleY:0.59,guide:{path:[1865.5,5141.4,1843,5224.3,1739.8,5300.2,1681.2,5343.2,1584,5397.6,1529.2,5428.2,1415.8,5490.6,1391.7,5502.6,1371.5,5515.2]}},40).to({regX:0.5,regY:-0.6,scaleX:1.22,scaleY:1.22,guide:{path:[1371.5,5515.2,1265.1,5581.6,1267.8,5662.6,1268.3,5667.9,1269.5,5673.1]}},16).to({regX:0.4,scaleX:0.8,scaleY:0.8,guide:{path:[1269.5,5673.1,1288.2,5759.2,1462.6,5838.2,1585.6,5895,1644.2,5951.2,1700.8,6005.7,1692.4,6055.5]}},30).to({regX:1.7,regY:-1,scaleX:0.15,scaleY:0.15,guide:{path:[1692.3,6055.5,1691.8,6058.7,1691,6061.8,1673.6,6129.8,1605.2,6151,1549.6,6168.4,1468.2,6153,1402.7,6137.9,1323.2,6095.7]}},30,cjs.Ease.get(-0.5)).to({regX:1.9,regY:-0.7,scaleX:0.32,scaleY:0.32,guide:{path:[1323.2,6095.6,1283.8,6074.6,1241,6047,1144.2,5985.2,1047.4,5923.4,944,5853.2,863.7,5801.8]}},12,cjs.Ease.get(-0.3)).to({regX:1.7,regY:-1,scaleX:0.15,scaleY:0.15,guide:{path:[863.7,5801.8,742.8,5724.5,674.2,5689.8,542.4,5625.1,437.5,5603.7]}},12).to({regX:0,regY:0,scaleX:0.9,scaleY:0.9,guide:{path:[437.5,5603.8,397.7,5595.7,361.8,5593.8,91.1,5581.2,42.7,5703,36,5721.8,34.8,5742.7]}},12).to({regX:0.3,regY:0.1,scaleX:0.4,scaleY:0.4,guide:{path:[34.7,5742.7,29,5849.5,170.7,6011.4,300.2,6140.2,466,6222.1]}},12).to({regX:0.5,regY:-0.1,scaleX:0.48,scaleY:0.48,guide:{path:[466.1,6222.1,537.7,6257.5,616.2,6284.2,706.7,6315.1,759.8,6334.5]}},10).to({regX:0.7,regY:-0.3,scaleX:0.22,scaleY:0.22,guide:{path:[759.8,6334.5,792.6,6346.4,811.2,6354,900.8,6390.6,935.8,6424.2,965.2,6450.5,988.4,6486.2]}},14).to({regX:1,regY:-0.6,scaleX:0.27,scaleY:0.27,guide:{path:[988.4,6486.2,1033.2,6555.3,1054.6,6659.8,1104.4,6850.2,1207,6910.2,1259,6940.6,1330.8,6931,1346,6928.9,1361.1,6925.3]}},12).to({regX:1.3,regY:-1,scaleX:0.53,scaleY:0.53,guide:{path:[1361,6925.3,1414.9,6912.1,1466.2,6877.8,1474.6,6871.6,1482.3,6865.9]}},8).to({regX:1.4,scaleX:0.7,scaleY:0.7,guide:{path:[1482.4,6865.9,1539.3,6824.2,1562,6812.4,1601.8,6792.2,1645.4,6813.4,1737.2,6871.4,1726.2,7073,1726.4,7107,1723.2,7140.1]}},14,cjs.Ease.get(0.3)).to({guide:{path:[1723.3,7140,1715.1,7225.8,1684,7305.8,1641.8,7415.2,1572.2,7475,1528.4,7512.6,1495.6,7543.7]}},18,cjs.Ease.get(0.2)).to({regX:1.7,regY:-1.6,scaleX:0.47,scaleY:0.47,guide:{path:[1495.6,7543.7,1450.4,7586.7,1426.2,7617.2,1364.6,7695,1347.4,7788.6,1346.7,7793.7,1346,7798.7]}},30,cjs.Ease.get(0.4)).to({guide:{path:[1346,7798.7,1316.2,8013.7,1399.8,8188.6,1474.6,8332.4,1583.4,8456.2,1681,8565.6,1654.6,8657.8,1639.4,8706.1,1588.2,8743.8]}},86).to({regX:2.2,regY:-1.8,scaleX:0.55,scaleY:0.55,guide:{path:[1588.1,8743.9,1587.7,8744.3,1587.2,8744.6,1534.6,8782.8,1438.6,8815,1382.4,8834,1272.8,8855.4,1259.7,8858,1247.5,8860.5]}},12,cjs.Ease.get(-0.5)).to({regX:1.7,regY:-1.6,scaleX:0.47,scaleY:0.47,guide:{path:[1247.5,8860.4,1156.6,8879,1109,8895,993,8934.7,918.8,8982.4]}},12).to({regX:2.1,regY:-1.8,scaleX:0.28,scaleY:0.28,guide:{path:[918.8,8982.4,845.8,9029.3,813.4,9083.8,778.6,9143.8,788.4,9193,798.4,9242,849.8,9267,849.8,9267.1,849.8,9267.1]}},10).to({guide:{path:[849.8,9267,905.2,9290.8,972.8,9270.6,1032.8,9252.6,1127.4,9191,1236.6,9121.5,1306.4,9082.4]}},8,cjs.Ease.get(0.3)).to({regX:0.3,regY:-0.4,scaleX:0.85,scaleY:0.85,guide:{path:[1306.4,9082.3,1332,9068,1352.2,9057.8,1443.4,9015,1449,9077.4,1451.6,9114.2,1439.6,9164.8,1426.2,9221.4,1395.8,9289.6,1385.5,9312.8,1377.9,9333.7]}},12,cjs.Ease.get(-0.3)).to({regX:0.6,regY:-0.8,scaleX:0.8,scaleY:0.8,guide:{path:[1377.9,9333.6,1358.2,9388.3,1357.8,9427.2,1357.5,9456.8,1368.4,9478.6]}},4).to({regX:0.3,regY:-0.5,scaleX:0.2,scaleY:0.2,guide:{path:[1368.3,9478.7,1378,9498.2,1396.6,9511.4,1407.6,9520,1440.2,9535.2,1457.4,9543.2,1501.2,9562.7]}},4).to({regX:0.4,regY:-0.4,scaleX:0.3,scaleY:0.3,guide:{path:[1501.3,9562.6,1508.9,9566,1517.4,9569.8,1600.9,9607.7,1671.9,9667.3]}},8).to({regX:0.5,regY:-0.5,scaleX:0.1,scaleY:0.1,guide:{path:[1671.9,9667.3,1684.8,9678.1,1697.2,9689.6,1782.6,9768.2,1830.6,9863.8,1861.1,9924.7,1877.1,9995.7]}},52,cjs.Ease.cubicOut).to({_off:true},1).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.4,0,2272,9999.8);


(lib.line_drawing = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_4 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_0 = new cjs.Graphics().p("EBBTLONQk8jGoui6QocikhkgoQlaiKiciWQkckQAAngQAAlsCqnwQBkkmEQpqQEMpkBmkwQCqn2AAl2QAAiIhigsQgugUhcAAQlEAArOGCQkmCcnEEcQ2lOGlcC6QsWGmnqAAQrWAAmQmIQlelWAAoEQAWkgC+mIQF4sQNEoaQHIkmOOl2QL2k2FFhUQB6ggJ8iAQKuiKHEhsQYSl2JKmWQMUokAAqKQAAjYhMjqQhkk0j2maQg6hgiejGIqktAQhGhWlmoMQlSnwhOh8QpUuuj4wAQjAsUAAugQAAqaAsk+QAgjWCKoAQDKreHspuQFinAEqj4QHGl8F4mEQNMtmE0w4QDisYAw3WQAiv4mSoiQk2mimOAAQjoAAi6B8QiKBcoeGIQkyDMn0CkQpaDEoeAGQxiAMrHzCQm4rwk81CQhimojqnSQkio+lkk+Qk4kWrwkaQsykQkChkQmgiSl2iMQrMkMp4kiQ+quCzUxiQpGoOo6s2QrcwgAAqWQAAsuMimeQL+mKWWAAUAdmAAAAjGASGQMqGiPAJwQA8AmWGO4QKEGyPlKkQLyH0HAD6QIWEqGqCCQG6CGHYAAQN0AAFekoQEWjsAAn+QACq23ctMQm0j2rElOQqMkyhqhCQoGlAjckUQkIlMACm2QAEqCPwqwQGAkIKEleISYpwQXmsoKkpcQPatygcvOQgcwTpirmQo0qqzIpkQmcjOighmQjeiOhoiaQhsiggokCQgijkAAnAQAAqcDKqSQByl2FIruQE8rWB+m2QDKq8AAriQgCs0pqrQQjikElkk0QmYlQi6icQpmoEnCqsQmapsAAksQAAikBEhsQBkigDmAAQFWAAQyJIQIuEuLGGiQMWGYOYIIQJkE8G2AAQFmAAD+joQD0jgAAkoQAAskwQxwQkYkyjmjIQlUkYk0kCQosm4n+mYQvssglSkmQnGmMj4mAQlkoqAApiQAAmWECqkQCOlyAkh0QBQkCAAiyQAAmGkooGQg6hkmqpwQjgkqjUkeQmko0hkiyQkwoWiMl6QjspwAApeQAAjQAykOQAUhwBQleQAeiCBCikQAWg2BkjoQBojwDOlGQEMmsE2lWQG2nmFUk6QBohgIinWQJgoMDMiQQG2k4E2jyQjElGhImgQgykaAAmgQAAsQGiwIQHSx+NQw4QBChUDekKQDgkUCYjaQHUqiAAmuQAAnAkKjmQkCjgnqAAQlWAAmOBiQjuA8nUCeQnUCejwA8QmQBilYAAQt4AAkys4QhokYhEnSIhqrMIhooQQh4m0jslAQrGvC6vBYQh4AGlKAoQlmAql0AYQswAypInKQjqi0iIjeQh+jMAAiiQAAncKkoUQDmi0JMlYQEmisKsjCQLijSGoAAQK1AAJ8CuQJWCmL0F8QCABAFMC6QEkCkCwBSQIyEGIgAAQF+AAHMjeQIYkCGknUUAROgTOgCIgg8QAMBSAkF2QAqGkAABkQAAX6sSPmQlkHEnOD2QnOD0nwAAQs+AAnCiiQicg6i8hoQkSiag2gaQqumGnsjIQt2lmvnAAQmAAApgCmQsGDSpUFwQl0DmkOFiQkIFaAQDaQA4LCQeB+QIyBCNuhIQW/AAOAK0QMMJcBqOCQAQCGAkHUQAyHYBkFKQEwPoM0gIQFcgEHSiKQD0hIJmjmQI4jUFIhYQH8iIGmAAQJQAAEeEiQDCDEAADeQAAEck8IIQkaHQpsMUQ2wc2nsUCQpYYYJiSKQFQjuGsk4QG8kuQ+pyQGOjmIkioQJOi4GqAAQLiAAHiFGQJ0GqAAOCQAAIQkOIMQjQGWmgHeQjyEWq0EIQrYEWpGAAQvMgCssqUQoam2pwuKQxcNqpAIyQluFkq+MAQmqHEk8IcQo+PUAAOsQAASAJWOMQC0ESEeE+QEiFCAoA8QA+BcCiC0QCyDGBiCIQDkE6BcEAQBqEmAAGOQAADihcEIQg0CWiWFGQkmJ8AAGGQAAGSEUHIQDaFsHMHqQFEFaLQIoQGWE4MYJIQP8MCLeN+QNeQaAALGQAAFwjsD8QjyEClwAAQpGAAwIoOQkciSmijwQlYjEg8gYQpYkumYjCQsElyl2AAQkAAAiKCsQhuCKAGCmQAKEEGyG+QEEEKKsJ8QJwJkE4H6QGmKsgEKeQgCJCkoMoQioHKnCPGQleLyhCGyQhiKCAAFAQAAGuBkEIQBYDuDECiQCQB2E2CUQHmDsByA+QVALgH0LqQGgJugIOtQgGI+hMESQh6G2mKGoUgN8APAgxoAaOQoEEQjyEQQk6FgAAHiQAAJQJ8H6QF2EqQAIIQP0ICGCE8QJ8IEAAJmQAALYqQF0QnqEWpsAAQsWAA1QrWQookmteoMI6bwUQkEigx4sAQvWqUqel+Ugg0gSygYiAAAQ2IAAq+GuQqCGKAALSQAAOqMoQgQN2SGaGQEQMOHkTaGwQFQB2KSDaQI6DEFyCeQP4G0JCJwQLIMAEcToQEkUMJtL8QKCMWM2AAQHcAAHOjeQE4iUHAlSQIEmECohiQF6jeFIAAQNcAAFiQwQDyLYAAS+QAASqmOQQQoUVuyuO6QyQOimWPyQkYK6AARiQAAQaFyPsQE4NQJ8PAQFMH2GsIGQDmEYF2HAQEYFmB6EgQCQFYAAGmQAAPOyEKSQmWDoo4DIQi4BCp4DCQl+B2oIBkQpMBmk0A+QxpDkvmH0QtMGonCGiQpiI4AAJ8QAAGwE0EaQFOEwJCAAQI6AAQ6p6QE0i2HzlEQHQkuCChKQI+loFijMQKQl0E+AAQEuAACEEAQBgC8AAEyQAAGoiGIqQheGAi+IoQjaJ8hGD+QhSEkAAC2QAAG6EsE8QDkDwHCDMIKgEwQKOFOIiG2UAaeAVMAFIAgGUgJagk4gpYgZ+gECJXoLuQrWBau2ImQo6FKyGNEQOOQIJmE4QEcCQFCA2QDuAqGiAIQEqAGFqhYQHSh0F0jwQQCqWAC1iQAAouoMjuQlXidoNAAQjvAAkVAhg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:mask_graphics_0,x:1134.5,y:4998.7}).wait(999).to({graphics:null,x:0,y:0}).wait(1));

	// Layer_3
	this.instance = new lib.element_4("synched",0);
	this.instance.setTransform(193,193,1,1,0,0,0,193,193);

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).to({_off:true},999).wait(1));

	// Layer_11 (mask)
	var mask_1 = new cjs.Shape();
	mask_1._off = true;
	var mask_1_graphics_1 = new cjs.Graphics().p("EApIBGYMAAAiMvMCMwAAAMAAACMvg");
	var mask_1_graphics_2 = new cjs.Graphics().p("EApFBGWMAAFiMvMCMwAAEMgAFCMvg");
	var mask_1_graphics_3 = new cjs.Graphics().p("EAo9BGPMAATiMvMCMwAASMgATCMvg");
	var mask_1_graphics_4 = new cjs.Graphics().p("EAowBGDMAAqiMvMCMwAAqMgAqCMvg");
	var mask_1_graphics_5 = new cjs.Graphics().p("EAodBFyMABLiMuMCMwABLMgBLCMug");
	var mask_1_graphics_6 = new cjs.Graphics().p("EAoFBFdMAB1iMuMCMwAB1MgB2CMug");
	var mask_1_graphics_7 = new cjs.Graphics().p("EAnoBFDMACpiMuMCMuACpMgCpCMug");
	var mask_1_graphics_8 = new cjs.Graphics().p("EAnGBEkMADmiMtMCMtADmMgDmCMtg");
	var mask_1_graphics_9 = new cjs.Graphics().p("EAmfBD/MAEsiMqMCMrAEtMgEsCMqg");
	var mask_1_graphics_10 = new cjs.Graphics().p("EAlzBDWMAF8iMnMCMoAF8MgF8CMng");
	var mask_1_graphics_11 = new cjs.Graphics().p("EAlDBCnMAHViMjMCMkAHWMgHVCMjg");
	var mask_1_graphics_12 = new cjs.Graphics().p("EAkOBBzMAI4iMdMCMeAI4MgI4CMdg");
	var mask_1_graphics_13 = new cjs.Graphics().p("EAjWBA6MAKjiMWMCMXAKjMgKjCMWg");
	var mask_1_graphics_14 = new cjs.Graphics().p("EAiaA/6MAMYiMMMCMNAMZMgMYCMMg");
	var mask_1_graphics_15 = new cjs.Graphics().p("EAhaA+1MAOXiMAMCMBAOXMgOXCMAg");
	var mask_1_graphics_16 = new cjs.Graphics().p("EAgXA9qMAQeiLxMCLyAQeMgQeCLxg");
	var mask_1_graphics_17 = new cjs.Graphics().p("EAfRA8ZMASviLfMCLgASuMgSvCLfg");
	var mask_1_graphics_18 = new cjs.Graphics().p("EAeJA7BMAVIiLJMCLKAVIMgVICLJg");
	var mask_1_graphics_19 = new cjs.Graphics().p("EAc/A5jMAXriKvMCKvAXqMgXqCKvg");
	var mask_1_graphics_20 = new cjs.Graphics().p("EAb0A3+MAaViKQMCKRAaVMgaVCKQg");
	var mask_1_graphics_21 = new cjs.Graphics().p("EAanA2SMAdJiJsMCJtAdJMgdJCJsg");
	var mask_1_graphics_22 = new cjs.Graphics().p("EAZaA0fMAgFiJCMCJDAgFMggFCJCg");
	var mask_1_graphics_23 = new cjs.Graphics().p("EAYNAylMAjJiISMCISAjJMgjICISg");
	var mask_1_graphics_24 = new cjs.Graphics().p("EAXAAwjMAmViHaMCHcAmVMgmVCHag");
	var mask_1_graphics_25 = new cjs.Graphics().p("EAV1AuaMApoiGcMCGdAppMgpoCGcg");
	var mask_1_graphics_26 = new cjs.Graphics().p("EAUrAsJMAtDiFVMCFWAtEMgtDCFVg");
	var mask_1_graphics_27 = new cjs.Graphics().p("EATkApxMAwliEGMCEGAwlMgwlCEGg");
	var mask_1_graphics_28 = new cjs.Graphics().p("EASfAnQMA0OiCsMCCtA0NMg0NCCsg");
	var mask_1_graphics_29 = new cjs.Graphics().p("EARfAknMA38iBJMCBKA38Mg38CBJg");
	var mask_1_graphics_30 = new cjs.Graphics().p("EAQjAh2MA7wh/bMB/cA7wMg7wB/bg");
	var mask_1_graphics_31 = new cjs.Graphics().p("APte9MA/oh9iMB9jA/pMg/pB9ig");
	var mask_1_graphics_32 = new cjs.Graphics().p("AO8b7MBDmh7cMB7eBDnMhDnB7cg");
	var mask_1_graphics_33 = new cjs.Graphics().p("AOTYyMBHnh5KMB5LBHnMhHnB5Kg");
	var mask_1_graphics_34 = new cjs.Graphics().p("ANyVgMBLrh2qMB2rBLrMhLrB2qg");
	var mask_1_graphics_35 = new cjs.Graphics().p("ANZSGMBPxhz8MBz+BPxMhPyBz8g");
	var mask_1_graphics_36 = new cjs.Graphics().p("ANKOkMBT5hxAMBxBBT5MhT5BxAg");
	var mask_1_graphics_37 = new cjs.Graphics().p("ANFK6MBYBht0MBt2BYBMhYCBt0g");
	var mask_1_graphics_38 = new cjs.Graphics().p("ANMHJMBcIhqZMBqaBcIMhcIBqZg");
	var mask_1_graphics_39 = new cjs.Graphics().p("ANfDQMBgOhmtMBmvBgOMhgPBmtg");
	var mask_1_graphics_40 = new cjs.Graphics().p("AOAgvMBkRhixMBixBkQMhkRBixg");
	var mask_1_graphics_41 = new cjs.Graphics().p("AOuk1MBoQhekMBekBoPMhoQBekg");
	var mask_1_graphics_42 = new cjs.Graphics().p("APrpBMBsKhaFMBaFBsIMhsJBaFg");
	var mask_1_graphics_60 = new cjs.Graphics().p("EATjAjXMA5riAYMCAZA5rMg5rCAYg");
	var mask_1_graphics_61 = new cjs.Graphics().p("EAS8Am0MA00iCcMCCeA01Mg01CCcg");
	var mask_1_graphics_62 = new cjs.Graphics().p("EASbAqdMAvliEdMCEeAvkMgvlCEdg");
	var mask_1_graphics_63 = new cjs.Graphics().p("EASFAuPMAp5iGWMCGXAp5Mgp5CGWg");
	var mask_1_graphics_64 = new cjs.Graphics().p("EAR5AyLMAjyiIHMCIIAjyMgjyCIHg");
	var mask_1_graphics_65 = new cjs.Graphics().p("EAR6A2NMAdRiJqMCJqAdRMgdQCJqg");
	var mask_1_graphics_66 = new cjs.Graphics().p("EASJA6VMAWUiK9MCK+AWUMgWUCK9g");
	var mask_1_graphics_67 = new cjs.Graphics().p("EASpA+hMAO7iL8MCL9AO7MgO7CL8g");
	var mask_1_graphics_68 = new cjs.Graphics().p("EATZBCtMAHKiMjMCMkAHKMgHKCMjg");
	var mask_1_graphics_69 = new cjs.Graphics().p("EAS6hF2MCMwgBBMABBCMuMiMwABBg");
	var mask_1_graphics_70 = new cjs.Graphics().p("EAHehBbMCMbgJjMAJiCMaMiMbAJjg");
	var mask_1_graphics_71 = new cjs.Graphics().p("EgEKg8kMCLigSYMASYCLhMiLiASYg");
	var mask_1_graphics_72 = new cjs.Graphics().p("EgP6g3QMCKBgbgMAbhCKBMiKCAbgg");
	var mask_1_graphics_82 = new cjs.Graphics().p("EhA0g6eMCK+gWCMAWDCK/MiK/AWCg");
	var mask_1_graphics_83 = new cjs.Graphics().p("EhZYgkcMCBCg4JMA4JCBCMiBCA4Jg");
	var mask_1_graphics_84 = new cjs.Graphics().p("EhiggOCMBwjhUeMBUeBwjMhwjBUeg");
	var mask_1_graphics_85 = new cjs.Graphics().p("EhjPAHLMBcFhqaMBqaBcFMhcFBqag");
	var mask_1_graphics_86 = new cjs.Graphics().p("Ehf/AaLMBF1h6KMB6KBF1MhF1B6Kg");
	var mask_1_graphics_87 = new cjs.Graphics().p("Ehf/AaLMBF1h6KMB6KBF1MhF1B6Kg");
	var mask_1_graphics_88 = new cjs.Graphics().p("EhKfBGwMAKxiMSMCMSAKxMgKxCMTg");
	var mask_1_graphics_89 = new cjs.Graphics().p("EhH/BGwMAKxiMSMCMSAKxMgKxCMTg");
	var mask_1_graphics_90 = new cjs.Graphics().p("Eg/lg6ZMCMsgCBMACACMsMiMsACAg");
	var mask_1_graphics_91 = new cjs.Graphics().p("Eg9Zg6ZMCMsgCBMACACMsMiMsACAg");
	var mask_1_graphics_92 = new cjs.Graphics().p("Eg97BLGMAF0iMlMCMlAF0MgF0CMlg");
	var mask_1_graphics_93 = new cjs.Graphics().p("EhFOA9IMAT3iLTMCLTAT3MgT3CLTg");
	var mask_1_graphics_94 = new cjs.Graphics().p("EhKxAz0MAhJiIwMCIwAhJMghJCIwg");
	var mask_1_graphics_95 = new cjs.Graphics().p("EhOrArxMAtliFHMCFIAtmMgtmCFHg");
	var mask_1_graphics_96 = new cjs.Graphics().p("EhRGAjwMA5IiAmMCAmA5HMg5HCAmg");
	var mask_1_graphics_97 = new cjs.Graphics().p("EhSKAb2MBDth7XMB7XBDsMhDtB7Xg");
	var mask_1_graphics_98 = new cjs.Graphics().p("EhSCAUJMBNVh1lMB1kBNUMhNUB1lg");
	var mask_1_graphics_99 = new cjs.Graphics().p("EhQ4AMtMBV/hvYMBvZBV/MhV/BvYg");
	var mask_1_graphics_100 = new cjs.Graphics().p("EhO3AFnMBdvho8MBo9BdvMhdvBo8g");
	var mask_1_graphics_101 = new cjs.Graphics().p("EhMIgBGMBkmhiZMBiZBkmMhknBiZg");
	var mask_1_graphics_102 = new cjs.Graphics().p("EhI2gHaMBqphb0MBb0BqpMhqqBb0g");
	var mask_1_graphics_112 = new cjs.Graphics().p("Egbsg0wMCJIgfnMAfnCJIMiJJAfng");
	var mask_1_graphics_113 = new cjs.Graphics().p("EgdbgzRMCIjgh/MAh/CIiMiIkAh/g");
	var mask_1_graphics_114 = new cjs.Graphics().p("EgfVgxnMCH3gkoMAkpCH3MiH4Akog");
	var mask_1_graphics_115 = new cjs.Graphics().p("EghXgvvMCHCgnjMAnjCHCMiHEAnjg");
	var mask_1_graphics_116 = new cjs.Graphics().p("EgjigtrMCGFgqtMAqtCGEMiGFAqtg");
	var mask_1_graphics_117 = new cjs.Graphics().p("EglygraMCE8guHMAuICE8MiE9AuHg");
	var mask_1_graphics_118 = new cjs.Graphics().p("EgoHgo6MCDngxyMAxyCDnMiDpAxyg");
	var mask_1_graphics_119 = new cjs.Graphics().p("EgqfgmNMCCFg1qMA1qCCFMiCGA1qg");
	var mask_1_graphics_120 = new cjs.Graphics().p("Egs5gjRMCAUg5xMA5xCAUMiAWA5xg");
	var mask_1_graphics_121 = new cjs.Graphics().p("EgvRggHMB+Tg+DMA+DB+SMh+UA+Dg");
	var mask_1_graphics_122 = new cjs.Graphics().p("EgxmgcuMB8AhCiMBChB7/Mh8ABCig");
	var mask_1_graphics_123 = new cjs.Graphics().p("Egz1gZGMB5ZhHLMBHLB5YMh5aBHLg");
	var mask_1_graphics_124 = new cjs.Graphics().p("Eg18gVPMB2dhL9MBL9B2cMh2eBL9g");
	var mask_1_graphics_125 = new cjs.Graphics().p("Eg34gRJMBzLhQ3MBQ2BzKMhzLBQ3g");
	var mask_1_graphics_126 = new cjs.Graphics().p("Eg5lgM1MBvghV1MBV1BvgMhvgBV1g");
	var mask_1_graphics_127 = new cjs.Graphics().p("Eg7BgHEMBrbha4MBa4BrcMhrcBa4g");
	var mask_1_graphics_128 = new cjs.Graphics().p("Eg8JAAaMBm8hf7MBf8Bm8Mhm9Bf8g");
	var mask_1_graphics_136 = new cjs.Graphics().p("EglBggjMCJggdzMAd0CJgMiJhAd0g");
	var mask_1_graphics_137 = new cjs.Graphics().p("EgaegpBMCLpgRSMARSCLpMiLpARRg");
	var mask_1_graphics_138 = new cjs.Graphics().p("EgPAgwPMCMngE/MAE/CMoMiMnAE+g");
	var mask_1_graphics_139 = new cjs.Graphics().p("EgNSBS1MAG/iMiMCMiAG/MgG/CMig");
	var mask_1_graphics_140 = new cjs.Graphics().p("EgR3BHMMAShiLeMCLgASiMgSjCLfg");
	var mask_1_graphics_141 = new cjs.Graphics().p("EgVWA8DMAdmiJjMCJkAdnMgdmCJjg");
	var mask_1_graphics_142 = new cjs.Graphics().p("EgXyAxdMAoIiG3MCG4AoIMgoJCG3g");
	var mask_1_graphics_143 = new cjs.Graphics().p("EgZQAouMAyEiDfMCDgAyEMgyFCDfg");
	var mask_1_graphics_144 = new cjs.Graphics().p("EgZ2AiFMA7Yh/jMB/kA7aMg7aB/jg");
	var mask_1_graphics_145 = new cjs.Graphics().p("A5qbhMBEGh7IMB7IBEHMhEHB7Ig");
	var mask_1_graphics_146 = new cjs.Graphics().p("A4wVFMBMJh2TMB2UBMKMhMKB2Tg");
	var mask_1_graphics_147 = new cjs.Graphics().p("A30Q4MBRKhy7MBy8BRMMhRLBy7g");
	var mask_1_graphics_148 = new cjs.Graphics().p("A2hMdMBWQhvKMBvLBWRMhWRBvKg");
	var mask_1_graphics_149 = new cjs.Graphics().p("A00HyMBbZhq9MBq/BbaMhbaBq9g");
	var mask_1_graphics_150 = new cjs.Graphics().p("AysC5MBgjhmVMBmXBgkMhgkBmVg");
	var mask_1_graphics_151 = new cjs.Graphics().p("AwEiMMBlqhhRMBhRBlqMhlrBhRg");
	var mask_1_graphics_152 = new cjs.Graphics().p("As8ngMBquhbtMBbtBquMhqvBbtg");
	var mask_1_graphics_153 = new cjs.Graphics().p("ApQs/MBvphVpMBVpBvoMhvqBVpg");
	var mask_1_graphics_154 = new cjs.Graphics().p("Ak9yqMB0ZhPEMBPEB0ZMh0ZBPEg");
	var mask_1_graphics_155 = new cjs.Graphics().p("AgB4dMB45hH/MBH/B46Mh46BH/g");
	var mask_1_graphics_156 = new cjs.Graphics().p("AFk+XMB9IhAYMBAZB9HMh9IBAYg");
	var mask_1_graphics_166 = new cjs.Graphics().p("EgCXA88MAe/iJPMCJQAfAMgfACJPg");
	var mask_1_graphics_167 = new cjs.Graphics().p("EgDGA9EMAf2iJDMCJEAf4Mgf3CJCg");
	var mask_1_graphics_168 = new cjs.Graphics().p("EgD3A9OMAgxiI1MCI2AgyMggzCI1g");
	var mask_1_graphics_169 = new cjs.Graphics().p("EgEtA9YMAhyiIlMCImAhyMghyCIlg");
	var mask_1_graphics_170 = new cjs.Graphics().p("EgFlA9jMAi2iIUMCIVAi3Mgi3CIUg");
	var mask_1_graphics_171 = new cjs.Graphics().p("EgGhA9vMAj/iIBMCICAkAMgkACIBg");
	var mask_1_graphics_172 = new cjs.Graphics().p("EgHgA98MAlNiHsMCHtAlOMglOCHsg");
	var mask_1_graphics_173 = new cjs.Graphics().p("EgIiA+KMAmfiHVMCHWAmgMgmfCHVg");
	var mask_1_graphics_174 = new cjs.Graphics().p("EgJmA+ZMAn1iG8MCG9An2Mgn2CG8g");
	var mask_1_graphics_175 = new cjs.Graphics().p("EgKtA+qMApPiGhMCGiApRMgpRCGhg");
	var mask_1_graphics_176 = new cjs.Graphics().p("EgL3A+7MAqviGDMCGEAqwMgqwCGDg");
	var mask_1_graphics_177 = new cjs.Graphics().p("EgNDA/OMAsTiFjMCFjAsUMgsUCFig");
	var mask_1_graphics_178 = new cjs.Graphics().p("EgORA/hMAt6iE/MCFBAt8Mgt8CE/g");
	var mask_1_graphics_179 = new cjs.Graphics().p("EgPhA/3MAvmiEaMCEaAvoMgvnCEZg");
	var mask_1_graphics_180 = new cjs.Graphics().p("EgQzBANMAxXiDwMCDxAxYMgxYCDwg");
	var mask_1_graphics_181 = new cjs.Graphics().p("EgSGBAlMAzKiDDMCDFAzLMgzLCDEg");
	var mask_1_graphics_182 = new cjs.Graphics().p("EgTbBA/MA1DiCUMCCVA1EMg1ECCTg");
	var mask_1_graphics_183 = new cjs.Graphics().p("EgUwBBaMA2+iBgMCBiA2/Mg2/CBhg");
	var mask_1_graphics_184 = new cjs.Graphics().p("EgWGBB4MA49iAqMCArA4/Mg4/CApg");
	var mask_1_graphics_185 = new cjs.Graphics().p("EgXdBCXMA7Bh/uMB/vA7BMg7CB/ug");
	var mask_1_graphics_186 = new cjs.Graphics().p("EgY0BC4MA9Hh+uMB+wA9GMg9IB+wg");
	var mask_1_graphics_187 = new cjs.Graphics().p("EgaKBDcMA/Qh9rMB9sA/QMg/SB9sg");
	var mask_1_graphics_188 = new cjs.Graphics().p("EgbhBEBMBBdh8iMB8jBBdMhBeB8kg");
	var mask_1_graphics_189 = new cjs.Graphics().p("EgbhBG1MBBdh8iMB8jBBdMhBeB8kg");
	var mask_1_graphics_190 = new cjs.Graphics().p("EgW3BfJMAsmiFcMCFdAsmMgsoCFdg");
	var mask_1_graphics_191 = new cjs.Graphics().p("EgYXBftMAuNiE4MCE5AuOMguOCE5g");
	var mask_1_graphics_218 = new cjs.Graphics().p("EA/RDOCMAF9iMjMCMjAF9MgF9CMjg");
	var mask_1_graphics_219 = new cjs.Graphics().p("EBBwBEPMCMkgFqMAFqCMkMiMkAFqg");
	var mask_1_graphics_220 = new cjs.Graphics().p("EAkpBdhMB9rg/PMA/PB9qMh9rA/Qg");
	var mask_1_graphics_221 = new cjs.Graphics().p("EAjABnTMBy6hRKMBRKBy6Mhy6BRKg");
	var mask_1_graphics_222 = new cjs.Graphics().p("EAkKBx5MBlohhSMBhSBloMhloBhSg");
	var mask_1_graphics_223 = new cjs.Graphics().p("EAoEB9FMBWGhvRMBvQBWGMhWGBvRg");
	var mask_1_graphics_224 = new cjs.Graphics().p("EAunCImMBErh6zMB6yBErMhEqB6yg");
	var mask_1_graphics_225 = new cjs.Graphics().p("EA3qCUMMAxviDnMCDmAxvMgxuCDmg");
	var mask_1_graphics_226 = new cjs.Graphics().p("EBC/CfnMAdsiJhMCJhAdrMgdsCJig");
	var mask_1_graphics_227 = new cjs.Graphics().p("EBQUCqmMAI/iMZMCMaAI/MgI/CMZg");
	var mask_1_graphics_228 = new cjs.Graphics().p("EBNfAiyMCMLgL6MAL6CMLMiMLAL6g");
	var mask_1_graphics_229 = new cjs.Graphics().p("EBHsAj0MCLsgQyMAQyCLrMiLrAQzg");
	var mask_1_graphics_230 = new cjs.Graphics().p("EBCAAk/MCLAgVqMAVqCLBMiLBAVqg");
	var mask_1_graphics_231 = new cjs.Graphics().p("EA8aAmSMCKLgafMAafCKLMiKLAafg");
	var mask_1_graphics_232 = new cjs.Graphics().p("EA27AntMCJLgfSMAfSCJKMiJKAfSg");
	var mask_1_graphics_233 = new cjs.Graphics().p("EAxkApQMCH/gkDMAkDCH/MiH/AkDg");
	var mask_1_graphics_234 = new cjs.Graphics().p("EAsUAq7MCGpgoxMAoyCGpMiGqAoxg");
	var mask_1_graphics_235 = new cjs.Graphics().p("EAnLAsuMCFJgtcMAtdCFJMiFJAtcg");
	var mask_1_graphics_236 = new cjs.Graphics().p("EAiLAuoMCDegyDMAyECDeMiDfAyDg");
	var mask_1_graphics_237 = new cjs.Graphics().p("EAfXAuoMCDegyDMAyECDeMiDfAyDg");
	var mask_1_graphics_258 = new cjs.Graphics().p("EgawB2oMBIVh4qMB4qBIVMhIVB4qg");
	var mask_1_graphics_259 = new cjs.Graphics().p("EgaKB8hMBCzh7zMB70BC0MhC0B7zg");
	var mask_1_graphics_260 = new cjs.Graphics().p("EgZUCCdMA9Nh+rMB+rA9OMg9OB+rg");
	var mask_1_graphics_261 = new cjs.Graphics().p("EgYPCIaMA3jiBPMCBQA3kMg3kCBPg");
	var mask_1_graphics_262 = new cjs.Graphics().p("EgW7COZMAx2iDjMCDjAx3Mgx3CDjg");
	var mask_1_graphics_263 = new cjs.Graphics().p("EgVZCUXMAsHiFlMCFlAsJMgsICFlg");
	var mask_1_graphics_264 = new cjs.Graphics().p("EgTrCaWMAmYiHWMCHWAmZMgmZCHWg");
	var mask_1_graphics_265 = new cjs.Graphics().p("EgRvCgTMAgoiI2MCI2AgpMggpCI2g");
	var mask_1_graphics_266 = new cjs.Graphics().p("EgPoCmOMAa5iKFMCKFAa6Mga5CKFg");
	var mask_1_graphics_267 = new cjs.Graphics().p("EgNVCsIMAVKiLFMCLFAVMMgVLCLEg");
	var mask_1_graphics_268 = new cjs.Graphics().p("EgK4Cx+MAPfiL1MCL0APgMgPfCL1g");
	var mask_1_graphics_269 = new cjs.Graphics().p("EgIRC3yMAJ1iMWMCMWAJ3MgJ3CMVg");
	var mask_1_graphics_270 = new cjs.Graphics().p("EgFhC9hMAERiMnMCMmAEQMgEQCMog");
	var mask_1_graphics_271 = new cjs.Graphics().p("EgEiA15MCMqgBSMABSCMsMiMrABRg");
	var mask_1_graphics_272 = new cjs.Graphics().p("EgJtA47MCMggGvMAGvCMhMiMhAGvg");
	var mask_1_graphics_273 = new cjs.Graphics().p("EgOqA8IMCMJgMIMAMHCMLMiMJAMHg");
	var mask_1_graphics_274 = new cjs.Graphics().p("EgTYA/eMCLlgRaMARbCLnMiLmARag");
	var mask_1_graphics_275 = new cjs.Graphics().p("EgX3BC/MCK2gWnMAWnCK2MiK2AWng");
	var mask_1_graphics_276 = new cjs.Graphics().p("EgcHBGoMCJ6gbuMAbuCJ7MiJ6Abug");
	var mask_1_graphics_277 = new cjs.Graphics().p("EggIBKZMCIzgguMAguCI1MiI1Agtg");
	var mask_1_graphics_278 = new cjs.Graphics().p("Egj7BORMCHjglmMAlnCHjMiHkAlng");
	var mask_1_graphics_292 = new cjs.Graphics().p("EgaPBaxMCHjglmMAlnCHjMiHkAlng");
	var mask_1_graphics_293 = new cjs.Graphics().p("EgaOBb4MCHjglnMAlmCHkMiHjAlng");
	var mask_1_graphics_294 = new cjs.Graphics().p("EgaOBc/MCHjglnMAlnCHkMiHkAlng");
	var mask_1_graphics_295 = new cjs.Graphics().p("EgaNBeGMCHjglnMAlnCHjMiHkAlng");
	var mask_1_graphics_296 = new cjs.Graphics().p("EgaMBfMMCHjglnMAlnCHkMiHkAlng");
	var mask_1_graphics_297 = new cjs.Graphics().p("EgaMBgTMCHjglnMAlnCHkMiHkAlng");
	var mask_1_graphics_298 = new cjs.Graphics().p("EgaLBhaMCHjglnMAlnCHkMiHkAlng");
	var mask_1_graphics_299 = new cjs.Graphics().p("EgaKBigMCHjglnMAlnCHkMiHkAlng");
	var mask_1_graphics_300 = new cjs.Graphics().p("EgaJBjnMCHjglnMAlmCHkMiHjAlng");
	var mask_1_graphics_301 = new cjs.Graphics().p("EgaJBkuMCHjglnMAlnCHkMiHkAlng");
	var mask_1_graphics_302 = new cjs.Graphics().p("EgaIBl1MCHjglnMAlnCHjMiHkAlng");
	var mask_1_graphics_303 = new cjs.Graphics().p("EgaHBm7MCHjglnMAlnCHkMiHkAlng");
	var mask_1_graphics_304 = new cjs.Graphics().p("EgaHBoCMCHjglnMAlnCHkMiHkAlng");
	var mask_1_graphics_305 = new cjs.Graphics().p("EgaGBpJMCHjglnMAlnCHkMiHkAlng");
	var mask_1_graphics_306 = new cjs.Graphics().p("EgaFBqPMCHjglnMAlnCHkMiHkAlng");
	var mask_1_graphics_307 = new cjs.Graphics().p("EgaEBrWMCHjglnMAlmCHkMiHjAlng");
	var mask_1_graphics_308 = new cjs.Graphics().p("EgaEBsdMCHjglnMAlnCHkMiHkAlng");
	var mask_1_graphics_309 = new cjs.Graphics().p("EgaDBtkMCHjglnMAlnCHjMiHkAlng");
	var mask_1_graphics_310 = new cjs.Graphics().p("EgaCBuqMCHjglnMAlnCHkMiHkAlng");
	var mask_1_graphics_311 = new cjs.Graphics().p("EgaCBvxMCHjglnMAlnCHkMiHkAlng");
	var mask_1_graphics_312 = new cjs.Graphics().p("EgaBBw4MCHjglnMAlnCHkMiHkAlng");
	var mask_1_graphics_313 = new cjs.Graphics().p("EgaABx+MCHjglnMAlnCHkMiHkAlng");
	var mask_1_graphics_314 = new cjs.Graphics().p("EgZ/BzFMCHiglnMAlnCHkMiHjAlng");
	var mask_1_graphics_315 = new cjs.Graphics().p("EgZ/B0MMCHjglnMAlnCHkMiHkAlng");
	var mask_1_graphics_316 = new cjs.Graphics().p("EgZ+B1TMCHjglnMAlnCHjMiHkAlng");
	var mask_1_graphics_317 = new cjs.Graphics().p("EgZ9B2ZMCHjglnMAlnCHkMiHkAlng");
	var mask_1_graphics_318 = new cjs.Graphics().p("EgZ9B3gMCHjglnMAlnCHkMiHkAlng");
	var mask_1_graphics_319 = new cjs.Graphics().p("EgZ8B4nMCHjglnMAlnCHkMiHkAlng");
	var mask_1_graphics_320 = new cjs.Graphics().p("EgZ7B5tMCHjglmMAlnCHjMiHkAlng");
	var mask_1_graphics_374 = new cjs.Graphics().p("EAEmC0/MBvRhWEMBWEBvRMhvQBWEg");
	var mask_1_graphics_375 = new cjs.Graphics().p("EAOYCreMB48hH1MBH1B48Mh48BH1g");
	var mask_1_graphics_376 = new cjs.Graphics().p("EAaICjwMCA0g4hMA4hCA1MiA0A4gg");
	var mask_1_graphics_377 = new cjs.Graphics().p("EAntCd+MCGwgoWMAoWCGwMiGwAoWg");
	var mask_1_graphics_378 = new cjs.Graphics().p("EA25CaMMCKrgXkMAXkCKrMiKrAXlg");
	var mask_1_graphics_379 = new cjs.Graphics().p("EBCoCZHMCMOgLHMALICMOMiMOALIg");
	var mask_1_graphics_380 = new cjs.Graphics().p("EBMTEkjMACoiMpMCMpACnMgCoCMpg");
	var mask_1_graphics_381 = new cjs.Graphics().p("EBFdEdqMARhiLlMCLlARhMgRhCLlg");
	var mask_1_graphics_382 = new cjs.Graphics().p("EA/EEWVMAhZiIqMCIqAhZMghZCIpg");
	var mask_1_graphics_383 = new cjs.Graphics().p("EA5dEOtMAx7iDhMCDhAx8Mgx7CDhg");
	var mask_1_graphics_396 = new cjs.Graphics().p("EAjEEM3MA8gh+/MB++A8gMg8gB+/g");
	var mask_1_graphics_397 = new cjs.Graphics().p("EAhWELvMA8gh+/MB+/A8gMg8gB+/g");
	var mask_1_graphics_398 = new cjs.Graphics().p("EAfpEKnMA8gh+/MB+/A8gMg8gB+/g");
	var mask_1_graphics_399 = new cjs.Graphics().p("EAd7EJfMA8gh+/MB+/A8gMg8gB+/g");
	var mask_1_graphics_400 = new cjs.Graphics().p("EAcOEIXMA8gh+/MB+/A8gMg8gB+/g");
	var mask_1_graphics_401 = new cjs.Graphics().p("EAahEHPMA8gh+/MB+/A8gMg8gB+/g");
	var mask_1_graphics_402 = new cjs.Graphics().p("EAYzEGHMA8gh+/MB+/A8gMg8gB+/g");
	var mask_1_graphics_403 = new cjs.Graphics().p("EAXGEFAMA8gh+/MB+/A8gMg8gB+/g");
	var mask_1_graphics_404 = new cjs.Graphics().p("EAVZED4MA8gh+/MB+/A8gMg8gB+/g");
	var mask_1_graphics_405 = new cjs.Graphics().p("EATrECwMA8gh+/MB+/A8gMg8gB+/g");
	var mask_1_graphics_406 = new cjs.Graphics().p("EAR+EBoMA8gh+/MB+/A8gMg8gB++g");
	var mask_1_graphics_410 = new cjs.Graphics().p("EAO2D+gMA8gh+/MB+/A8gMg8gB++g");
	var mask_1_graphics_432 = new cjs.Graphics().p("EggfDG/MB8LhCBMBCBB8NMh8MBCBg");
	var mask_1_graphics_433 = new cjs.Graphics().p("EgaYDDAMCALg56MA56CAMMiAMA56g");
	var mask_1_graphics_434 = new cjs.Graphics().p("EgTwC/jMCDogxkMAxjCDpMiDoAxkg");
	var mask_1_graphics_435 = new cjs.Graphics().p("EgMqC8pMCGigpBMApACGjMiGiApBg");
	var mask_1_graphics_436 = new cjs.Graphics().p("EgFHC6SMCI4ggSMAgTCI5MiI6AgTg");
	var mask_1_graphics_437 = new cjs.Graphics().p("EAC1C4gMCKsgXcMAXcCKsMiKsAXcg");
	var mask_1_graphics_438 = new cjs.Graphics().p("EALLC3SMCL6gOfMAOfCL6MiL6AOfg");
	var mask_1_graphics_439 = new cjs.Graphics().p("EAT3C2oMCMjgFeMAFfCMjMiMjAFfg");
	var mask_1_graphics_440 = new cjs.Graphics().p("EAXiFBZMADkiMnMCMnADkMgDkCMng");
	var mask_1_graphics_466 = new cjs.Graphics().p("EALKDh2MCK0gWsMAWrCK0MiKzAWsg");
	var mask_1_graphics_467 = new cjs.Graphics().p("EAFpDkSMCJcgd8MAd8CJbMiJbAd9g");
	var mask_1_graphics_468 = new cjs.Graphics().p("EAAiDnCMCHtgk8MAk9CHtMiHtAk9g");
	var mask_1_graphics_469 = new cjs.Graphics().p("EgELDqEMCFrgrsMArsCFtMiFsArsg");
	var mask_1_graphics_470 = new cjs.Graphics().p("EgIhDtWMCDZgyKMAyKCDaMiDaAyKg");
	var mask_1_graphics_471 = new cjs.Graphics().p("EgMfDw1MCA3g4VMA4WCA4MiA4A4Vg");
	var mask_1_graphics_472 = new cjs.Graphics().p("EgQFD0gMB+Ig+PMA+PB+JMh+JA+Pg");
	var mask_1_graphics_473 = new cjs.Graphics().p("EgTUD4WMB7NhD2MBD1B7NMh7NBD2g");
	var mask_1_graphics_474 = new cjs.Graphics().p("EgWND8TMB4HhJKMBJKB4IMh4IBJKg");
	var mask_1_graphics_475 = new cjs.Graphics().p("EgYyEAYMB06hOMMBOMB06Mh07BOMg");
	var mask_1_graphics_476 = new cjs.Graphics().p("EgbCEEhMBxlhS7MBS8BxmMhxnBS7g");
	var mask_1_graphics_477 = new cjs.Graphics().p("Egc/EIuMBuLhXZMBXZBuNMhuMBXZg");
	var mask_1_graphics_478 = new cjs.Graphics().p("EgerEM9MBquhblMBbmBqwMhqwBblg");
	var mask_1_graphics_479 = new cjs.Graphics().p("EggGERNMBnPhfgMBfgBnQMhnQBfgg");
	var mask_1_graphics_480 = new cjs.Graphics().p("EghSEVcMBjuhjLMBjLBjvMhjvBjLg");
	var mask_1_graphics_481 = new cjs.Graphics().p("EggCEUXMBlPhhnMBhnBlQMhlQBhng");
	var mask_1_graphics_482 = new cjs.Graphics().p("EgexETUMBmvhgCMBgDBmwMhmwBgCg");
	var mask_1_graphics_483 = new cjs.Graphics().p("EgddESSMBoOhecMBecBoPMhoPBecg");
	var mask_1_graphics_484 = new cjs.Graphics().p("EgcHERSMBpqhc0MBc0BprMhprBc0g");
	var mask_1_graphics_485 = new cjs.Graphics().p("EgauEQTMBrFhbKMBbLBrGMhrHBbLg");
	var mask_1_graphics_486 = new cjs.Graphics().p("EgZUEPXMBsfhZgMBZgBsfMhsgBZgg");
	var mask_1_graphics_487 = new cjs.Graphics().p("EgX3EObMBt2hX0MBX0Bt4Mht3BX0g");
	var mask_1_graphics_488 = new cjs.Graphics().p("EgWYENiMBvMhWHMBWHBvOMhvOBWGg");
	var mask_1_graphics_489 = new cjs.Graphics().p("EgU3EMqMBwhhUYMBUYBwiMhwiBUYg");
	var mask_1_graphics_490 = new cjs.Graphics().p("EgTUEL0MBx0hSoMBSoBx1Mhx1BSng");
	var mask_1_graphics_491 = new cjs.Graphics().p("EgRuEK/MBzEhQ2MBQ3BzFMhzGBQ3g");
	var mask_1_graphics_492 = new cjs.Graphics().p("EgQHEKNMB0UhPEMBPEB0UMh0VBPEg");
	var mask_1_graphics_493 = new cjs.Graphics().p("EgOdEJcMB1hhNQMBNQB1iMh1iBNQg");
	var mask_1_graphics_494 = new cjs.Graphics().p("EgMyEItMB2thLbMBLbB2uMh2tBLbg");
	var mask_1_graphics_495 = new cjs.Graphics().p("EgLHEHtMB4chIpMBIoB4cMh4cBIpg");
	var mask_1_graphics_496 = new cjs.Graphics().p("EgJYEGxMB6GhF0MBF0B6GMh6GBF0g");
	var mask_1_graphics_497 = new cjs.Graphics().p("EgHkEF6MB7qhC/MBC+B7sMh7rBC+g");
	var mask_1_graphics_498 = new cjs.Graphics().p("EgFtEFHMB9LhAHMBAHB9MMh9MBAHg");
	var mask_1_graphics_499 = new cjs.Graphics().p("EgDyEEYMB+ng9OMA9OB+oMh+oA9Og");
	var mask_1_graphics_500 = new cjs.Graphics().p("EgB0EDuMB/+g6UMA6VCAAMiAAA6Ug");
	var mask_1_graphics_501 = new cjs.Graphics().p("EAANEDJMCBSg3ZMA3ZCBSMiBSA3Zg");
	var mask_1_graphics_502 = new cjs.Graphics().p("EACSECnMCCgg0cMA0dCCgMiCgA0dg");
	var mask_1_graphics_503 = new cjs.Graphics().p("EAEbECLMCDqgxgMAxfCDqMiDpAxfg");
	var mask_1_graphics_504 = new cjs.Graphics().p("EAGmEByMCEvguiMAuiCEvMiEuAuig");
	var mask_1_graphics_505 = new cjs.Graphics().p("EAI1EBeMCFvgrkMArkCFvMiFvArkg");
	var mask_1_graphics_506 = new cjs.Graphics().p("EALGEBOMCGrgokMAolCGqMiGrAolg");
	var mask_1_graphics_507 = new cjs.Graphics().p("EANbEBDMCHiglmMAlmCHjMiHjAllg");
	var mask_1_graphics_508 = new cjs.Graphics().p("EAPxEA8MCIVgimMAinCIVMiIVAimg");
	var mask_1_graphics_509 = new cjs.Graphics().p("EASLEA5MCJDgfmMAfnCJDMiJEAfmg");
	var mask_1_graphics_510 = new cjs.Graphics().p("EAUmEA6MCJugcmMAcmCJtMiJtAcng");
	var mask_1_graphics_511 = new cjs.Graphics().p("EAXEEBAMCKTgZnMAZnCKTMiKTAZng");
	var mask_1_graphics_512 = new cjs.Graphics().p("EAZkEBJMCK0gWnMAWoCK0MiK1AWog");
	var mask_1_graphics_513 = new cjs.Graphics().p("EAcGEBXMCLRgToMAToCLSMiLRATng");
	var mask_1_graphics_514 = new cjs.Graphics().p("EAeqEBoMCLqgQoMAQpCLqMiLrAQpg");
	var mask_1_graphics_515 = new cjs.Graphics().p("EAhPEB+MCL/gNqMANqCL/MiL/ANqg");
	var mask_1_graphics_516 = new cjs.Graphics().p("EAj2ECYMCMQgKsMAKsCMPMiMQAKsg");
	var mask_1_graphics_517 = new cjs.Graphics().p("EAmfEC1MCMcgHuMAHuCMcMiMcAHug");
	var mask_1_graphics_518 = new cjs.Graphics().p("EApJEDWMCMkgExMAEyCMkMiMlAEyg");
	var mask_1_graphics_519 = new cjs.Graphics().p("EAr0ED7MCMpgB1MAB2CMpMiMpAB1g");
	var mask_1_graphics_520 = new cjs.Graphics().p("EAs4GQrMABGiMpMCMpABFMgBFCMpg");
	var mask_1_graphics_560 = new cjs.Graphics().p("EgRJFkWMBjbhjdMBjdBjdMhjdBjcg");
	var mask_1_graphics_561 = new cjs.Graphics().p("EgSlFqjMBcShqIMBqHBcTMhcSBqHg");
	var mask_1_graphics_562 = new cjs.Graphics().p("EgTYFw8MBU2hwKMBwJBU3MhU3BwKg");
	var mask_1_graphics_563 = new cjs.Graphics().p("EgTnF3eMBNMh1jMB1kBNNMhNNB1jg");
	var mask_1_graphics_564 = new cjs.Graphics().p("EgTSF+IMBFYh6VMB6VBFYMhFZB6Vg");
	var mask_1_graphics_565 = new cjs.Graphics().p("EgScGE1MA9ch+fMB+gA9dMg9dB+gg");
	var mask_1_graphics_566 = new cjs.Graphics().p("EgRIGLkMA1diCEMCCFA1eMg1eCCFg");
	var mask_1_graphics_567 = new cjs.Graphics().p("EgPZGSTMAteiFFMCFFAteMgteCFFg");
	var mask_1_graphics_568 = new cjs.Graphics().p("EgNQGY/MAlgiHjMCHiAlhMglgCHig");
	var mask_1_graphics_569 = new cjs.Graphics().p("EgKwGfnMAdmiJfMCJfAdnMgdnCJeg");
	var mask_1_graphics_570 = new cjs.Graphics().p("EgH8GmIMAVziK8MCK8AV0MgV1CK8g");
	var mask_1_graphics_571 = new cjs.Graphics().p("EgE3GsiMAOJiL7MCL7AOKMgOKCL7g");
	var mask_1_graphics_572 = new cjs.Graphics().p("EgBhGyzMAGpiMfMCMeAGqMgGqCMfg");
	var mask_1_graphics_573 = new cjs.Graphics().p("EABCEr8MCMogApMAAqCMoMiMpAAqg");
	var mask_1_graphics_574 = new cjs.Graphics().p("EgF5EuiMCMagHwMAHvCMbMiMbAHwg");
	var mask_1_graphics_575 = new cjs.Graphics().p("EgMXExZMCL3gOoMAOoCL4MiL4AOog");
	var mask_1_graphics_576 = new cjs.Graphics().p("EgSXE0eMCLAgVQMAVRCLBMiLCAVQg");
	var mask_1_graphics_606 = new cjs.Graphics().p("EAMEG+HMAkaiH3MCH2AkaMgkZCH2g");
	var mask_1_graphics_607 = new cjs.Graphics().p("EAI8G3cMAt/iE6MCE6At/Mgt/CE6g");
	var mask_1_graphics_608 = new cjs.Graphics().p("EAGbGw9MA3WiBTMCBTA3VMg3VCBUg");
	var mask_1_graphics_609 = new cjs.Graphics().p("EAEjGqsMBAah9CMB9CBAZMhAaB9Cg");
	var mask_1_graphics_610 = new cjs.Graphics().p("EADUGkqMBJJh4IMB4JBJJMhJJB4Ig");
	var mask_1_graphics_611 = new cjs.Graphics().p("EACuGe6MBRhhynMBynBRgMhRgByog");
	var mask_1_graphics_612 = new cjs.Graphics().p("EACyGZeMBZdhshMBsiBZeMhZeBshg");
	var mask_1_graphics_613 = new cjs.Graphics().p("EADeGUYMBg+hl4MBl4Bg+Mhg+Bl4g");
	var mask_1_graphics_614 = new cjs.Graphics().p("EAEzGPpMBn+hetMBetBn+Mhn+Beug");
	var mask_1_graphics_615 = new cjs.Graphics().p("EAGwGLUMBudhXEMBXEBudMhudBXEg");
	var mask_1_graphics_616 = new cjs.Graphics().p("EAJUGHaMB0YhO/MBO+B0YMh0YBO/g");
	var mask_1_graphics_617 = new cjs.Graphics().p("EAMeGD8MB5thGgMBGfB5tMh5tBGgg");
	var mask_1_graphics_618 = new cjs.Graphics().p("EAQNGA7MB+ag9pMA9pB+bMh+aA9pg");
	var mask_1_graphics_619 = new cjs.Graphics().p("EAUgF+aMCCeg0fMA0fCCfMiCfA0fg");
	var mask_1_graphics_620 = new cjs.Graphics().p("EAZUF8YMCF5grEMArDCF5MiF4ArDg");
	var mask_1_graphics_621 = new cjs.Graphics().p("EAaoF66MCHNgmuMAmuCHNMiHNAmug");
	var mask_1_graphics_622 = new cjs.Graphics().p("EAcCF5iMCIZgiVMAiWCIYMiIZAiWg");
	var mask_1_graphics_623 = new cjs.Graphics().p("EAdjF4SMCJbgd7MAd7CJbMiJbAd7g");
	var mask_1_graphics_624 = new cjs.Graphics().p("EAfKF3IMCKUgZeMAZeCKUMiKUAZeg");
	var mask_1_graphics_625 = new cjs.Graphics().p("EAg3F2FMCLEgVAMAVACLEMiLEAVAg");
	var mask_1_graphics_626 = new cjs.Graphics().p("EAipF1JMCLrgQgMAQgCLrMiLrAQgg");
	var mask_1_graphics_627 = new cjs.Graphics().p("EAkhF0UMCMIgL/MAMACMIMiMJAMAg");
	var mask_1_graphics_628 = new cjs.Graphics().p("EAmeFznMCMcgHeMAHeCMcMiMcAHeg");
	var mask_1_graphics_629 = new cjs.Graphics().p("EAogFzAMCMngC8MAC8CMnMiMnAC8g");
	var mask_1_graphics_630 = new cjs.Graphics().p("EAoNH+VMABniMoMCMpABmMgBnCMpg");
	var mask_1_graphics_631 = new cjs.Graphics().p("EAjmH7jMAGIiMgMCMhAGIMgGJCMhg");
	var mask_1_graphics_632 = new cjs.Graphics().p("EAfDH4vMAKqiMPMCMQAKqMgKrCMQg");
	var mask_1_graphics_633 = new cjs.Graphics().p("EAalH15MAPMiL0MCL1APLMgPMCL1g");
	var mask_1_graphics_634 = new cjs.Graphics().p("EAWNHzBMATsiLQMCLRATrMgTsCLRg");
	var mask_1_graphics_635 = new cjs.Graphics().p("EAR7HwIMAYKiKkMCKkAYLMgYLCKjg");
	var mask_1_graphics_636 = new cjs.Graphics().p("EANuHtNMAcoiJtMCJtAcoMgcoCJtg");
	var mask_1_graphics_648 = new cjs.Graphics().p("EgWhHXpMAcniJtMCJtAcoMgcoCJtg");
	var mask_1_graphics_649 = new cjs.Graphics().p("EgZLHWdMAcmiJtMCJuAcnMgcoCJug");
	var mask_1_graphics_650 = new cjs.Graphics().p("Egb3HVQMAcniJtMCJtAcnMgcoCJug");
	var mask_1_graphics_651 = new cjs.Graphics().p("EgekHUCMAcoiJtMCJsAcoMgcoCJtg");
	var mask_1_graphics_652 = new cjs.Graphics().p("EghTHS0MAcoiJtMCJsAcoMgcoCJtg");
	var mask_1_graphics_653 = new cjs.Graphics().p("EgkDHRlMAcoiJtMCJsAcnMgcoCJug");
	var mask_1_graphics_654 = new cjs.Graphics().p("Egm0HQVMAcniJtMCJsAcoMgcnCJtg");
	var mask_1_graphics_655 = new cjs.Graphics().p("EgpnHPFMAcniJtMCJsAcnMgcnCJug");
	var mask_1_graphics_656 = new cjs.Graphics().p("EgscHN0MAcoiJtMCJsAcnMgcoCJug");
	var mask_1_graphics_657 = new cjs.Graphics().p("EgvSHMiMAcoiJtMCJsAcoMgcoCJtg");
	var mask_1_graphics_658 = new cjs.Graphics().p("EgyKHLQMAcoiJtMCJsAcnMgcnCJug");
	var mask_1_graphics_659 = new cjs.Graphics().p("Eg1DHJ9MAcoiJtMCJsAcnMgcnCJug");
	var mask_1_graphics_660 = new cjs.Graphics().p("Eg39HIpMAcoiJtMCJsAcoMgcoCJtg");
	var mask_1_graphics_672 = new cjs.Graphics().p("EhGTE0gMCMnAAAMAAACMoMiMnAABg");
	var mask_1_graphics_673 = new cjs.Graphics().p("EhLnE5MMCMLgLEMALECMNMiMLALEg");
	var mask_1_graphics_674 = new cjs.Graphics().p("EhQgE+yMCK3gWJMAWKCK4MiK3AWKg");
	var mask_1_graphics_675 = new cjs.Graphics().p("EhU6FFSMCIpghMMAhMCIqMiIpAhMg");
	var mask_1_graphics_676 = new cjs.Graphics().p("EhY0FMrMCFigsHMAsHCFiMiFiAsHg");
	var mask_1_graphics_677 = new cjs.Graphics().p("EhcKFU7MCBgg21MA21CBgMiBgA21g");
	var mask_1_graphics_678 = new cjs.Graphics().p("Ehe6FeCMB8jhBSMBBSB8kMh8jBBSg");
	var mask_1_graphics_679 = new cjs.Graphics().p("EhhCFn9MB2uhLXMBLXB2vMh2uBLXg");
	var mask_1_graphics_680 = new cjs.Graphics().p("EhigFypMBwBhVAMBVABwCMhwBBVAg");
	var mask_1_graphics_681 = new cjs.Graphics().p("EhjTF+EMBofheIMBeIBofMhofBeIg");
	var mask_1_graphics_682 = new cjs.Graphics().p("EhjYGKKMBgIhmpMBmpBgIMhgIBmqg");
	var mask_1_graphics_683 = new cjs.Graphics().p("EhivGW4MBXAhufMBufBXAMhXABufg");
	var mask_1_graphics_684 = new cjs.Graphics().p("EhhWGkIMBNKh1lMB1jBNKMhNKB1lg");
	var mask_1_graphics_694 = new cjs.Graphics().p("EhhMHANMBMRh2JMB2IBMSMhMRB2Jg");
	var mask_1_graphics_695 = new cjs.Graphics().p("EhhmG/iMBOph0lMB0kBOqMhOpB0lg");
	var mask_1_graphics_696 = new cjs.Graphics().p("Ehh9G+5MBQ8hzAMBy/BQ8MhQ8BzAg");
	var mask_1_graphics_697 = new cjs.Graphics().p("EhiRG+TMBTJhxcMBxaBTJMhTJBxbg");
	var mask_1_graphics_698 = new cjs.Graphics().p("EhiiG9uMBVQhv2MBv1BVQMhVQBv2g");
	var mask_1_graphics_699 = new cjs.Graphics().p("EhiwG9MMBXRhuRMBuQBXRMhXRBuSg");
	var mask_1_graphics_700 = new cjs.Graphics().p("Ehi9G8sMBZOhstMBstBZOMhZOBstg");
	var mask_1_graphics_701 = new cjs.Graphics().p("EhjGG8OMBbFhrJMBrIBbFMhbFBrKg");
	var mask_1_graphics_702 = new cjs.Graphics().p("EhjOG7yMBc4hpmMBplBc4Mhc4Bpmg");
	var mask_1_graphics_703 = new cjs.Graphics().p("EhjUG7YMBelhoEMBoEBelMhelBoFg");
	var mask_1_graphics_704 = new cjs.Graphics().p("EhjYG7AMBgOhmjMBmjBgOMhgOBmjg");
	var mask_1_graphics_705 = new cjs.Graphics().p("EhjaG6qMBhyhlEMBlDBhyMhhyBlEg");
	var mask_1_graphics_706 = new cjs.Graphics().p("EhjbG6WMBjRhjnMBjmBjRMhjRBjng");
	var mask_1_graphics_707 = new cjs.Graphics().p("EhjbG6DMBkshiLMBiLBksMhksBiLg");
	var mask_1_graphics_708 = new cjs.Graphics().p("EhjZG5yMBmChgxMBgxBmDMhmCBgxg");
	var mask_1_graphics_720 = new cjs.Graphics().p("Eg3vGvCMCKCgaxMAayCKDMiKDAayg");
	var mask_1_graphics_721 = new cjs.Graphics().p("Eg5bGyAMCJFgfSMAfSCJHMiJGAfSg");
	var mask_1_graphics_722 = new cjs.Graphics().p("Eg68G1DMCH/gjvMAjwCIAMiH/Ajwg");
	var mask_1_graphics_723 = new cjs.Graphics().p("Eg8SG4NMCGwgoMMAoMCGyMiGwAoLg");
	var mask_1_graphics_724 = new cjs.Graphics().p("Eg9cG7cMCFXgskMAskCFYMiFXAskg");
	var mask_1_graphics_725 = new cjs.Graphics().p("Eg+bG+wMCD1gw6MAw6CD3MiD1Aw6g");
	var mask_1_graphics_726 = new cjs.Graphics().p("Eg/PHCJMCCLg1MMA1MCCMMiCKA1Mg");
	var mask_1_graphics_727 = new cjs.Graphics().p("Eg/2HFoMCAXg5cMA5bCAYMiAXA5bg");
	var mask_1_graphics_728 = new cjs.Graphics().p("EhASHJLMB+ag9mMA9mB+bMh+aA9mg");
	var mask_1_graphics_742 = new cjs.Graphics().p("EAdGIuEMAAAiMoMCMpAAAMAAACMog");
	var mask_1_graphics_743 = new cjs.Graphics().p("EAYhIrCMAG5iMdMCMdAG5MgG5CMeg");
	var mask_1_graphics_744 = new cjs.Graphics().p("EAUSIoGMANniL+MCL+ANnMgNnCL9g");
	var mask_1_graphics_745 = new cjs.Graphics().p("EAQbIlQMAUGiLLMCLMAUGMgUGCLLg");
	var mask_1_graphics_746 = new cjs.Graphics().p("EAM6IiiMAaXiKJMCKJAaYMgaYCKIg");
	var mask_1_graphics_747 = new cjs.Graphics().p("EAJuIf7MAgaiI2MCI2AgaMggbCI2g");
	var mask_1_graphics_748 = new cjs.Graphics().p("EAG2IdbMAmOiHVMCHVAmOMgmOCHVg");
	var mask_1_graphics_749 = new cjs.Graphics().p("EAEQIbEMArziFpMCFoArzMgryCFog");
	var mask_1_graphics_750 = new cjs.Graphics().p("EAB9IY0MAxHiDxMCDxAxIMgxHCDxg");
	var mask_1_graphics_751 = new cjs.Graphics().p("EgAFIWtMA2MiBxMCBwA2NMg2MCBxg");
	var mask_1_graphics_752 = new cjs.Graphics().p("EgB4IUtMA7Ch/oMB/oA7DMg7DB/og");
	var mask_1_graphics_753 = new cjs.Graphics().p("EgDQITRMA/Dh9sMB9sA/FMg/FB9sg");
	var mask_1_graphics_754 = new cjs.Graphics().p("EgEhIR3MBDCh7nMB7nBDDMhDDB7ng");
	var mask_1_graphics_755 = new cjs.Graphics().p("EgFpIQhMBG8h5aMB5aBG8MhG9B5bg");
	var mask_1_graphics_756 = new cjs.Graphics().p("EgGpIPOMBKxh3GMB3GBKyMhKyB3Gg");
	var mask_1_graphics_757 = new cjs.Graphics().p("EgHhIN/MBOih0qMB0pBOjMhOiB0pg");
	var mask_1_graphics_758 = new cjs.Graphics().p("EgIQIMzMBSMhyFMByGBSNMhSOByGg");
	var mask_1_graphics_759 = new cjs.Graphics().p("EgI3ILsMBVyhvaMBvaBVzMhVzBvbg");
	var mask_1_graphics_760 = new cjs.Graphics().p("EgJXIKpMBZThsoMBsoBZUMhZUBsog");
	var mask_1_graphics_790 = new cjs.Graphics().p("EgR2I9oMAvmiEVMCEUAvmMgvmCEVg");
	var mask_1_graphics_791 = new cjs.Graphics().p("EgQ6JBsMArfiFuMCFuArgMgrgCFug");
	var mask_1_graphics_792 = new cjs.Graphics().p("EgP3JFxMAnYiHAMCG/AnZMgnZCG/g");
	var mask_1_graphics_793 = new cjs.Graphics().p("EgOtJJ2MAjPiIIMCIIAjQMgjQCIIg");
	var mask_1_graphics_794 = new cjs.Graphics().p("EgNdJN7MAfHiJJMCJIAfJMgfICJIg");
	var mask_1_graphics_795 = new cjs.Graphics().p("EgMGJR/MAa+iKAMCKAAbAMga/CKAg");
	var mask_1_graphics_796 = new cjs.Graphics().p("EgKqJWEMAW2iKwMCKwAW3MgW3CKvg");
	var mask_1_graphics_797 = new cjs.Graphics().p("EgJHJaIMAStiLYMCLYASvMgSvCLXg");
	var mask_1_graphics_798 = new cjs.Graphics().p("EgHeJeKMAOliL2MCL3AOmMgOnCL3g");
	var mask_1_graphics_799 = new cjs.Graphics().p("EgFwJiMMAKeiMOMCMPAKgMgKgCMOg");
	var mask_1_graphics_800 = new cjs.Graphics().p("EgD9JmNMAGZiMeMCMeAGaMgGaCMeg");
	var mask_1_graphics_801 = new cjs.Graphics().p("EgCEJqNMACUiMmMCMmACUMgCVCMmg");
	var mask_1_graphics_802 = new cjs.Graphics().p("EgCsHgtMCMmgBvMABuCMnMiMmABug");
	var mask_1_graphics_803 = new cjs.Graphics().p("EgGsHivMCMfgFwMAFwCMgMiMfAFwg");
	var mask_1_graphics_804 = new cjs.Graphics().p("EgKkHk4MCMQgJvMAJvCMRMiMQAJwg");
	var mask_1_graphics_805 = new cjs.Graphics().p("EgLGHmJMCMMgKzMAKzCMMMiMMAKzg");
	var mask_1_graphics_806 = new cjs.Graphics().p("EgLmHnZMCMGgL2MAL2CMIMiMHAL2g");
	var mask_1_graphics_807 = new cjs.Graphics().p("EgMHHorMCMBgM5MAM5CMBMiMCAM5g");
	var mask_1_graphics_808 = new cjs.Graphics().p("EgMmHp8MCL6gN8MAN7CL7MiL6AN8g");
	var mask_1_graphics_809 = new cjs.Graphics().p("EgNFHrOMCLzgO/MAO+CL0MiL0AO+g");
	var mask_1_graphics_810 = new cjs.Graphics().p("EgNkHsfMCLsgQAMAQACLsMiLsAQBg");
	var mask_1_graphics_811 = new cjs.Graphics().p("EgOCHtxMCLkgRCMARCCLlMiLkARCg");
	var mask_1_graphics_812 = new cjs.Graphics().p("EgOfHvEMCLbgSFMASECLdMiLcASEg");
	var mask_1_graphics_813 = new cjs.Graphics().p("EgO9HwWMCLTgTGMATGCLUMiLUATGg");
	var mask_1_graphics_814 = new cjs.Graphics().p("EgPZHxpMCLJgUHMAUICLKMiLLAUHg");
	var mask_1_graphics_815 = new cjs.Graphics().p("EgP1Hy8MCLAgVIMAVICLBMiLBAVIg");
	var mask_1_graphics_816 = new cjs.Graphics().p("EgQRH0PMCK2gWJMAWJCK3MiK3AWJg");
	var mask_1_graphics_817 = new cjs.Graphics().p("EgQsH1iMCKsgXJMAXJCKsMiKsAXKg");
	var mask_1_graphics_818 = new cjs.Graphics().p("EgRGH22MCKggYKMAYKCKiMiKiAYJg");
	var mask_1_graphics_819 = new cjs.Graphics().p("EgRgH4JMCKVgZJMAZJCKWMiKWAZJg");
	var mask_1_graphics_820 = new cjs.Graphics().p("EgR5H5dMCKJgaJMAaJCKKMiKLAaJg");
	var mask_1_graphics_821 = new cjs.Graphics().p("EgSRH6sMCJ+gbEMAbECJ+MiJ/AbFg");
	var mask_1_graphics_822 = new cjs.Graphics().p("EgSoH77MCJygcAMAb/CJzMiJyAcAg");
	var mask_1_graphics_823 = new cjs.Graphics().p("EgS/H9LMCJlgc7MAc8CJmMiJnAc8g");
	var mask_1_graphics_824 = new cjs.Graphics().p("EgTWH+bMCJZgd2MAd3CJaMiJaAd2g");
	var mask_1_graphics_825 = new cjs.Graphics().p("EgTsH/sMCJMgeyMAeyCJNMiJNAexg");
	var mask_1_graphics_826 = new cjs.Graphics().p("EgUBIA8MCI+gfsMAftCI/MiI/Aftg");
	var mask_1_graphics_827 = new cjs.Graphics().p("EgUWICOMCIwggoMAgoCIxMiIyAgog");
	var mask_1_graphics_828 = new cjs.Graphics().p("EgUrIDfMCIighiMAhjCIjMiIkAhjg");
	var mask_1_graphics_829 = new cjs.Graphics().p("EgVAIExMCIUgidMAidCIUMiIUAieg");
	var mask_1_graphics_830 = new cjs.Graphics().p("EgVUIGDMCIFgjYMAjYCIGMiIGAjYg");
	var mask_1_graphics_831 = new cjs.Graphics().p("EgVoIHWMCH2gkTMAkSCH3MiH2AkSg");
	var mask_1_graphics_832 = new cjs.Graphics().p("EgV7IIpMCHmglNMAlNCHmMiHnAlOg");
	var mask_1_graphics_833 = new cjs.Graphics().p("EgWOIJ9MCHWgmIMAmHCHWMiHWAmIg");
	var mask_1_graphics_834 = new cjs.Graphics().p("EgWgILQMCHEgnCMAnCCHGMiHFAnCg");
	var mask_1_graphics_835 = new cjs.Graphics().p("EgWyIMlMCGzgn8MAn8CG0MiG0An8g");
	var mask_1_graphics_836 = new cjs.Graphics().p("EgXEIN5MCGigo2MAo2CGjMiGjAo2g");
	var mask_1_graphics_837 = new cjs.Graphics().p("EgXVIPOMCGQgpwMApwCGRMiGRApwg");
	var mask_1_graphics_838 = new cjs.Graphics().p("EgXmIQjMCF+gqqMAqqCGAMiF/Aqpg");
	var mask_1_graphics_839 = new cjs.Graphics().p("EgX2IR5MCFrgrkMArkCFtMiFtArjg");
	var mask_1_graphics_840 = new cjs.Graphics().p("EgYGITPMCFYgsdMAsdCFZMiFZAsdg");
	var mask_1_graphics_841 = new cjs.Graphics().p("EgYWIUlMCFFgtXMAtXCFHMiFGAtWg");
	var mask_1_graphics_842 = new cjs.Graphics().p("EgYlIV8MCEyguQMAuPCEyMiEyAuQg");
	var mask_1_graphics_843 = new cjs.Graphics().p("EgY0IXTMCEegvJMAvJCEeMiEfAvJg");
	var mask_1_graphics_844 = new cjs.Graphics().p("EgZCIYqMCEJgwCMAwCCEKMiEKAwCg");
	var mask_1_graphics_845 = new cjs.Graphics().p("EgZQIaCMCD0gw7MAw7CD1MiD1Aw7g");
	var mask_1_graphics_846 = new cjs.Graphics().p("EgZdIbaMCDegx0MAx0CDgMiDgAx0g");
	var mask_1_graphics_847 = new cjs.Graphics().p("EgZqIcyMCDJgysMAysCDKMiDKAysg");
	var mask_1_graphics_848 = new cjs.Graphics().p("EgZ3IeLMCCzgzlMAzlCC0MiC0Azlg");
	var mask_1_graphics_849 = new cjs.Graphics().p("EgaDIfkMCCdg0dMA0dCCeMiCeA0dg");
	var mask_1_graphics_850 = new cjs.Graphics().p("EgaPIg9MCCGg1VMA1VCCHMiCHA1Vg");
	var mask_1_graphics_851 = new cjs.Graphics().p("EgaaIiXMCBvg2NMA2NCBwMiBwA2Ng");
	var mask_1_graphics_852 = new cjs.Graphics().p("EgalIjxMCBXg3FMA3FCBZMiBYA3Fg");
	var mask_1_graphics_853 = new cjs.Graphics().p("EgavIlMMCA/g39MA39CBAMiBBA39g");
	var mask_1_graphics_854 = new cjs.Graphics().p("Ega5ImnMCAng41MA40CAoMiAoA41g");
	var mask_1_graphics_855 = new cjs.Graphics().p("EgbDIoCMCAPg5sMA5rCAQMiAPA5rg");
	var mask_1_graphics_856 = new cjs.Graphics().p("EgbMIpdMB/2g6jMA6iB/3Mh/2A6ig");
	var mask_1_graphics_857 = new cjs.Graphics().p("EgbUIq5MB/cg7aMA7ZB/dMh/dA7ag");
	var mask_1_graphics_858 = new cjs.Graphics().p("EgbdIsVMB/Dg8RMA8QB/EMh/DA8Qg");
	var mask_1_graphics_859 = new cjs.Graphics().p("EgbkItxMB+og9HMA9HB+pMh+pA9Hg");
	var mask_1_graphics_860 = new cjs.Graphics().p("EgbsIvOMB+Og9+MA9+B+PMh+PA99g");
	var mask_1_graphics_861 = new cjs.Graphics().p("EgcUIqjMCKxg4UMA03CCTMiKyA4Ug");
	var mask_1_graphics_862 = new cjs.Graphics().p("EgciImLMCXHgxMMArhCFuMiXIAxMg");
	var mask_1_graphics_863 = new cjs.Graphics().p("EgcQIiGMCjFgokMAh8CIdMijHAokg");
	var mask_1_graphics_864 = new cjs.Graphics().p("EgbcIeVMCulgefMAYMCKhMiulAefg");
	var mask_1_graphics_865 = new cjs.Graphics().p("EgaAIa6MC5ZgS/MAOVCL5Mi5ZAS+g");
	var mask_1_graphics_866 = new cjs.Graphics().p("EgX5IX1MDDbgGFMAEYCMjMjDbAGFg");
	var mask_1_graphics_867 = new cjs.Graphics().p("EgdcKdlMAFmiMgMDMgAIIMgFlCMgg");
	var mask_1_graphics_868 = new cjs.Graphics().p("EgouKSzMAPhiLxMDUhAXnMgPhCLwg");
	var mask_1_graphics_869 = new cjs.Graphics().p("EgzEKHIMAZYiKUMDbUAoQMgZYCKUg");
	var mask_1_graphics_870 = new cjs.Graphics().p("Eg8VJ6qMAjGiIKMDgyA59MgjHCIKg");
	var mask_1_graphics_871 = new cjs.Graphics().p("EhEcJteMAsqiFWMDkzBMpMgsrCFVg");
	var mask_1_graphics_872 = new cjs.Graphics().p("EhLQJfpMA1/iB2MDnPBgKMg1/CB2g");
	var mask_1_graphics_873 = new cjs.Graphics().p("EhQsJRSMA/Dh9sMDoCB0ZMg/DB9sg");
	var mask_1_graphics_874 = new cjs.Graphics().p("EhUqJCfMBHzh46MDnECJNMhHyB46g");
	var mask_1_graphics_875 = new cjs.Graphics().p("EhXFIzYMBQLhzhMDkTCecMhQLBzhg");
	var mask_1_graphics_876 = new cjs.Graphics().p("EhX3IkFMBYIhtjMDfqCz8MhYKBtkg");
	var mask_1_graphics_888 = new cjs.Graphics().p("EgLdI1vMCFWgskMAsjCFXMiFXAskg");
	var mask_1_graphics_889 = new cjs.Graphics().p("EgOjI3tMCE0guJMAuJCE0MiE0AuKg");
	var mask_1_graphics_890 = new cjs.Graphics().p("EgRnI5tMCEPgvvMAvvCERMiEQAvvg");
	var mask_1_graphics_891 = new cjs.Graphics().p("EgUqI7vMCDqgxVMAxUCDsMiDrAxUg");
	var mask_1_graphics_892 = new cjs.Graphics().p("EgXsI9xMCDEgy5MAy5CDFMiDFAy5g");
	var mask_1_graphics_893 = new cjs.Graphics().p("EgasI/0MCCcg0dMA0eCCdMiCeA0dg");
	var mask_1_graphics_894 = new cjs.Graphics().p("EgdrJB5MCB0g2BMA2BCB0MiB1A2Bg");
	var mask_1_graphics_895 = new cjs.Graphics().p("EggoJD+MCBJg3kMA3kCBKMiBKA3lg");
	var mask_1_graphics_896 = new cjs.Graphics().p("EgjkJGFMCAeg5HMA5HCAfMiAfA5Hg");
	var mask_1_graphics_897 = new cjs.Graphics().p("EgmfJIMMB/yg6pMA6pB/zMh/zA6pg");
	var mask_1_graphics_898 = new cjs.Graphics().p("EgpYJKVMB/Eg8LMA8LB/FMh/FA8Lg");
	var mask_1_graphics_899 = new cjs.Graphics().p("EgsQJMfMB+Wg9sMA9sB+WMh+XA9sg");
	var mask_1_graphics_900 = new cjs.Graphics().p("EgvGJOqMB9mg/NMA/MB9mMh9mA/Ng");
	var mask_1_graphics_910 = new cjs.Graphics().p("EhcRKeeMA4siArMCAqA4tMg4tCAqg");
	var mask_1_graphics_911 = new cjs.Graphics().p("EhVtKfaMA4siArMCAqA4tMg4tCAqg");
	var mask_1_graphics_912 = new cjs.Graphics().p("EhX6JnYMCGUgpgMAphCGVMiGUApgg");
	var mask_1_graphics_913 = new cjs.Graphics().p("EhZOJnrMCFEgtZMAtZCFFMiFEAtZg");
	var mask_1_graphics_914 = new cjs.Graphics().p("EhaYJoBMCDygw/MAw/CDzMiDyAw/g");
	var mask_1_graphics_915 = new cjs.Graphics().p("EhZBJoYMCChg0RMA0RCChMiCgA0Sg");
	var mask_1_graphics_916 = new cjs.Graphics().p("EhW6JowMCBRg3RMA3SCBSMiBRA3Rg");
	var mask_1_graphics_917 = new cjs.Graphics().p("EhU5JpIMCAFg5/MA5+CAGMiAEA5/g");
	var mask_1_graphics_918 = new cjs.Graphics().p("EhTBJpfMB+9g8aMA8aB++Mh+8A8ag");
	var mask_1_graphics_931 = new cjs.Graphics().p("EgCyLVaMAI6iMVMCMUAI8MgI7CMUg");
	var mask_1_graphics_932 = new cjs.Graphics().p("EgDaLYOMAI6iMVMCMUAI8MgI7CMUg");
	var mask_1_graphics_933 = new cjs.Graphics().p("EgDuLbCMAI6iMVMCMUAI8MgI7CMUg");
	var mask_1_graphics_938 = new cjs.Graphics().p("Eg6hKtXMBY+hs2MBs2BY/MhY/Bs2g");
	var mask_1_graphics_939 = new cjs.Graphics().p("Eg42KuYMBY/hs2MBs2BY/MhZABs2g");
	var mask_1_graphics_940 = new cjs.Graphics().p("Eg3KKvZMBY+hs2MBs2BY/MhY/Bs2g");
	var mask_1_graphics_941 = new cjs.Graphics().p("Eg1fKwaMBY/hs2MBs2BY/MhZABs2g");
	var mask_1_graphics_942 = new cjs.Graphics().p("EgzzKxbMBY+hs2MBs2BY/MhY/Bs2g");
	var mask_1_graphics_943 = new cjs.Graphics().p("EgyIKycMBY/hs2MBs2BY/MhZABs2g");
	var mask_1_graphics_944 = new cjs.Graphics().p("EgwcKzdMBY+hs2MBs2BY/MhY/Bs2g");
	var mask_1_graphics_945 = new cjs.Graphics().p("EguxK0eMBY/hs2MBs2BY/MhZABs2g");
	var mask_1_graphics_946 = new cjs.Graphics().p("EgtFK1fMBY+hs2MBs2BY/MhY/Bs2g");

	this.timeline.addTween(cjs.Tween.get(mask_1).to({graphics:null,x:0,y:0}).wait(1).to({graphics:mask_1_graphics_1,x:1164,y:-448.8}).wait(1).to({graphics:mask_1_graphics_2,x:1164.1991,y:-448.5774}).wait(1).to({graphics:mask_1_graphics_3,x:1164.796,y:-447.9087}).wait(1).to({graphics:mask_1_graphics_4,x:1165.7895,y:-446.7918}).wait(1).to({graphics:mask_1_graphics_5,x:1167.1779,y:-445.223}).wait(1).to({graphics:mask_1_graphics_6,x:1168.9585,y:-443.1972}).wait(1).to({graphics:mask_1_graphics_7,x:1171.1279,y:-440.7077}).wait(1).to({graphics:mask_1_graphics_8,x:1173.6818,y:-437.7466}).wait(1).to({graphics:mask_1_graphics_9,x:1176.6151,y:-434.3041}).wait(1).to({graphics:mask_1_graphics_10,x:1179.9217,y:-430.3694}).wait(1).to({graphics:mask_1_graphics_11,x:1183.5947,y:-425.93}).wait(1).to({graphics:mask_1_graphics_12,x:1187.6259,y:-420.9719}).wait(1).to({graphics:mask_1_graphics_13,x:1192.0062,y:-415.4798}).wait(1).to({graphics:mask_1_graphics_14,x:1196.7253,y:-409.437}).wait(1).to({graphics:mask_1_graphics_15,x:1201.7715,y:-402.8251}).wait(1).to({graphics:mask_1_graphics_16,x:1207.1319,y:-395.6246}).wait(1).to({graphics:mask_1_graphics_17,x:1212.7921,y:-387.8145}).wait(1).to({graphics:mask_1_graphics_18,x:1218.7362,y:-379.3724}).wait(1).to({graphics:mask_1_graphics_19,x:1224.9467,y:-370.2747}).wait(1).to({graphics:mask_1_graphics_20,x:1231.4042,y:-360.4964}).wait(1).to({graphics:mask_1_graphics_21,x:1238.0877,y:-350.0115}).wait(1).to({graphics:mask_1_graphics_22,x:1244.9742,y:-338.7925}).wait(1).to({graphics:mask_1_graphics_23,x:1252.0385,y:-326.811}).wait(1).to({graphics:mask_1_graphics_24,x:1259.2536,y:-314.0376}).wait(1).to({graphics:mask_1_graphics_25,x:1266.5899,y:-300.4419}).wait(1).to({graphics:mask_1_graphics_26,x:1274.0156,y:-285.9928}).wait(1).to({graphics:mask_1_graphics_27,x:1281.4967,y:-270.6583}).wait(1).to({graphics:mask_1_graphics_28,x:1288.9963,y:-254.4059}).wait(1).to({graphics:mask_1_graphics_29,x:1296.4753,y:-237.2028}).wait(1).to({graphics:mask_1_graphics_30,x:1303.8916,y:-219.0156}).wait(1).to({graphics:mask_1_graphics_31,x:1311.2007,y:-199.8112}).wait(1).to({graphics:mask_1_graphics_32,x:1318.3552,y:-179.5564}).wait(1).to({graphics:mask_1_graphics_33,x:1325.3049,y:-158.2183}).wait(1).to({graphics:mask_1_graphics_34,x:1331.997,y:-135.7646}).wait(1).to({graphics:mask_1_graphics_35,x:1338.3758,y:-112.1638}).wait(1).to({graphics:mask_1_graphics_36,x:1344.3829,y:-87.3857}).wait(1).to({graphics:mask_1_graphics_37,x:1349.9573,y:-61.4011}).wait(1).to({graphics:mask_1_graphics_38,x:1355.0355,y:-34.1827}).wait(1).to({graphics:mask_1_graphics_39,x:1359.5514,y:-5.7054}).wait(1).to({graphics:mask_1_graphics_40,x:1363.4367,y:24.0538}).wait(1).to({graphics:mask_1_graphics_41,x:1366.621,y:55.1149}).wait(1).to({graphics:mask_1_graphics_42,x:1368.999,y:87.4874}).wait(1).to({graphics:null,x:0,y:0}).wait(17).to({graphics:mask_1_graphics_60,x:1315.8841,y:208.2213}).wait(1).to({graphics:mask_1_graphics_61,x:1294.1596,y:198.0333}).wait(1).to({graphics:mask_1_graphics_62,x:1270.1556,y:186.8802}).wait(1).to({graphics:mask_1_graphics_63,x:1243.7257,y:174.7413}).wait(1).to({graphics:mask_1_graphics_64,x:1214.7114,y:161.6027}).wait(1).to({graphics:mask_1_graphics_65,x:1182.9486,y:147.4609}).wait(1).to({graphics:mask_1_graphics_66,x:1148.2691,y:132.3255}).wait(1).to({graphics:mask_1_graphics_67,x:1110.5026,y:116.2207}).wait(1).to({graphics:mask_1_graphics_68,x:1069.4789,y:99.1886}).wait(1).to({graphics:mask_1_graphics_69,x:1028.2697,y:81.2906}).wait(1).to({graphics:mask_1_graphics_70,x:1007.5021,y:62.6105}).wait(1).to({graphics:mask_1_graphics_71,x:984.0348,y:43.2566}).wait(1).to({graphics:mask_1_graphics_72,x:957.5543,y:23.3263}).wait(1).to({graphics:null,x:0,y:0}).wait(9).to({graphics:mask_1_graphics_82,x:615.6607,y:46.9786}).wait(1).to({graphics:mask_1_graphics_83,x:613.0707,y:105.7411}).wait(1).to({graphics:mask_1_graphics_84,x:567.5067,y:183.3385}).wait(1).to({graphics:mask_1_graphics_85,x:519.8113,y:271.6006}).wait(1).to({graphics:mask_1_graphics_86,x:489.8114,y:363.4319}).wait(1).to({graphics:mask_1_graphics_87,x:521.8114,y:363.4319}).wait(1).to({graphics:mask_1_graphics_88,x:490.0295,y:521.7534}).wait(1).to({graphics:mask_1_graphics_89,x:506.0295,y:521.7534}).wait(1).to({graphics:mask_1_graphics_90,x:506.3129,y:526.6434}).wait(1).to({graphics:mask_1_graphics_91,x:520.3129,y:526.6434}).wait(1).to({graphics:mask_1_graphics_92,x:540.6236,y:517.8095}).wait(1).to({graphics:mask_1_graphics_93,x:575.5806,y:518.3345}).wait(1).to({graphics:mask_1_graphics_94,x:608.8151,y:489.739}).wait(1).to({graphics:mask_1_graphics_95,x:640.1799,y:454.6778}).wait(1).to({graphics:mask_1_graphics_96,x:669.5767,y:422.3251}).wait(1).to({graphics:mask_1_graphics_97,x:696.9508,y:392.7905}).wait(1).to({graphics:mask_1_graphics_98,x:722.285,y:366.1078}).wait(1).to({graphics:mask_1_graphics_99,x:745.5943,y:342.2471}).wait(1).to({graphics:mask_1_graphics_100,x:766.92,y:321.1255}).wait(1).to({graphics:mask_1_graphics_101,x:786.3243,y:302.6185}).wait(1).to({graphics:mask_1_graphics_102,x:803.8638,y:286.6725}).wait(1).to({graphics:null,x:0,y:0}).wait(9).to({graphics:mask_1_graphics_112,x:902.6564,y:336.9871}).wait(1).to({graphics:mask_1_graphics_113,x:903.0718,y:346.783}).wait(1).to({graphics:mask_1_graphics_114,x:903.4556,y:357.8925}).wait(1).to({graphics:mask_1_graphics_115,x:903.7843,y:370.3624}).wait(1).to({graphics:mask_1_graphics_116,x:904.0302,y:384.2487}).wait(1).to({graphics:mask_1_graphics_117,x:904.163,y:399.6118}).wait(1).to({graphics:mask_1_graphics_118,x:904.1503,y:416.5163}).wait(1).to({graphics:mask_1_graphics_119,x:903.9582,y:435.0307}).wait(1).to({graphics:mask_1_graphics_120,x:903.5505,y:455.2268}).wait(1).to({graphics:mask_1_graphics_121,x:902.8902,y:477.1794}).wait(1).to({graphics:mask_1_graphics_122,x:901.9386,y:500.9655}).wait(1).to({graphics:mask_1_graphics_123,x:900.6567,y:526.6634}).wait(1).to({graphics:mask_1_graphics_124,x:899.0049,y:554.3523}).wait(1).to({graphics:mask_1_graphics_125,x:896.9438,y:584.1111}).wait(1).to({graphics:mask_1_graphics_126,x:894.4348,y:616.0174}).wait(1).to({graphics:mask_1_graphics_127,x:891.4403,y:642.3887}).wait(1).to({graphics:mask_1_graphics_128,x:887.9249,y:661.4702}).wait(1).to({graphics:null,x:0,y:0}).wait(7).to({graphics:mask_1_graphics_136,x:833.8921,y:671.7887}).wait(1).to({graphics:mask_1_graphics_137,x:834.8682,y:631.1456}).wait(1).to({graphics:mask_1_graphics_138,x:835.8168,y:591.2465}).wait(1).to({graphics:mask_1_graphics_139,x:859.0999,y:574.7816}).wait(1).to({graphics:mask_1_graphics_140,x:896.963,y:574.2718}).wait(1).to({graphics:mask_1_graphics_141,x:933.2425,y:573.7788}).wait(1).to({graphics:mask_1_graphics_142,x:967.7604,y:573.3025}).wait(1).to({graphics:mask_1_graphics_143,x:1000.3832,y:564.6408}).wait(1).to({graphics:mask_1_graphics_144,x:1031.018,y:546.5208}).wait(1).to({graphics:mask_1_graphics_145,x:1059.6088,y:531.9927}).wait(1).to({graphics:mask_1_graphics_146,x:1086.133,y:520.7907}).wait(1).to({graphics:mask_1_graphics_147,x:1102.6208,y:515.0477}).wait(1).to({graphics:mask_1_graphics_148,x:1119.399,y:510.2654}).wait(1).to({graphics:mask_1_graphics_149,x:1136.358,y:506.6329}).wait(1).to({graphics:mask_1_graphics_150,x:1153.3768,y:504.3444}).wait(1).to({graphics:mask_1_graphics_151,x:1170.3226,y:503.6007}).wait(1).to({graphics:mask_1_graphics_152,x:1187.0499,y:504.6073}).wait(1).to({graphics:mask_1_graphics_153,x:1203.4006,y:507.5726}).wait(1).to({graphics:mask_1_graphics_154,x:1219.2045,y:512.7062}).wait(1).to({graphics:mask_1_graphics_155,x:1234.279,y:520.2161}).wait(1).to({graphics:mask_1_graphics_156,x:1248.458,y:530.2744}).wait(1).to({graphics:null,x:0,y:0}).wait(9).to({graphics:mask_1_graphics_166,x:1061.6214,y:588.4096}).wait(1).to({graphics:mask_1_graphics_167,x:1061.2178,y:594.7283}).wait(1).to({graphics:mask_1_graphics_168,x:1060.7721,y:601.5966}).wait(1).to({graphics:mask_1_graphics_169,x:1060.2853,y:609.0142}).wait(1).to({graphics:mask_1_graphics_170,x:1059.7561,y:616.9814}).wait(1).to({graphics:mask_1_graphics_171,x:1059.1828,y:625.498}).wait(1).to({graphics:mask_1_graphics_172,x:1058.5636,y:634.5641}).wait(1).to({graphics:mask_1_graphics_173,x:1057.8965,y:644.1797}).wait(1).to({graphics:mask_1_graphics_174,x:1057.1793,y:654.3447}).wait(1).to({graphics:mask_1_graphics_175,x:1056.4096,y:665.0592}).wait(1).to({graphics:mask_1_graphics_176,x:1055.5845,y:676.3232}).wait(1).to({graphics:mask_1_graphics_177,x:1054.7011,y:688.1367}).wait(1).to({graphics:mask_1_graphics_178,x:1053.7561,y:700.4997}).wait(1).to({graphics:mask_1_graphics_179,x:1052.7458,y:713.4121}).wait(1).to({graphics:mask_1_graphics_180,x:1051.6664,y:726.874}).wait(1).to({graphics:mask_1_graphics_181,x:1050.5134,y:740.8854}).wait(1).to({graphics:mask_1_graphics_182,x:1049.2822,y:755.4464}).wait(1).to({graphics:mask_1_graphics_183,x:1047.9676,y:770.5568}).wait(1).to({graphics:mask_1_graphics_184,x:1046.5642,y:786.2167}).wait(1).to({graphics:mask_1_graphics_185,x:1045.066,y:802.4261}).wait(1).to({graphics:mask_1_graphics_186,x:1043.4663,y:819.1851}).wait(1).to({graphics:mask_1_graphics_187,x:1041.7584,y:836.4936}).wait(1).to({graphics:mask_1_graphics_188,x:1039.9332,y:854.3515}).wait(1).to({graphics:mask_1_graphics_189,x:1039.9332,y:872.3515}).wait(1).to({graphics:mask_1_graphics_190,x:993.1519,y:894.3818}).wait(1).to({graphics:mask_1_graphics_191,x:990.3335,y:908.3909}).wait(1).to({graphics:null,x:0,y:0}).wait(26).to({graphics:mask_1_graphics_218,x:1342.5123,y:1356.7181}).wait(1).to({graphics:mask_1_graphics_219,x:1356.6215,y:1336.2851}).wait(1).to({graphics:mask_1_graphics_220,x:1443.5011,y:1402.7584}).wait(1).to({graphics:mask_1_graphics_221,x:1478.8224,y:1396.5198}).wait(1).to({graphics:mask_1_graphics_222,x:1504.3565,y:1379.3004}).wait(1).to({graphics:mask_1_graphics_223,x:1519.437,y:1351.4731}).wait(1).to({graphics:mask_1_graphics_224,x:1523.6254,y:1313.6405}).wait(1).to({graphics:mask_1_graphics_225,x:1516.7229,y:1266.6259}).wait(1).to({graphics:mask_1_graphics_226,x:1498.7759,y:1211.4553}).wait(1).to({graphics:mask_1_graphics_227,x:1470.074,y:1149.3344}).wait(1).to({graphics:mask_1_graphics_228,x:1469.2476,y:1119.7325}).wait(1).to({graphics:mask_1_graphics_229,x:1460.221,y:1123.1945}).wait(1).to({graphics:mask_1_graphics_230,x:1450.605,y:1126.3783}).wait(1).to({graphics:mask_1_graphics_231,x:1440.4058,y:1129.2916}).wait(1).to({graphics:mask_1_graphics_232,x:1429.6174,y:1131.936}).wait(1).to({graphics:mask_1_graphics_233,x:1418.2342,y:1134.3134}).wait(1).to({graphics:mask_1_graphics_234,x:1406.2517,y:1136.4262}).wait(1).to({graphics:mask_1_graphics_235,x:1393.6658,y:1138.2769}).wait(1).to({graphics:mask_1_graphics_236,x:1380.4818,y:1139.8337}).wait(1).to({graphics:mask_1_graphics_237,x:1362.4818,y:1139.8337}).wait(1).to({graphics:null,x:0,y:0}).wait(20).to({graphics:mask_1_graphics_258,x:1063.8929,y:1222.0805}).wait(1).to({graphics:mask_1_graphics_259,x:1052.4842,y:1224.501}).wait(1).to({graphics:mask_1_graphics_260,x:1040.3685,y:1226.6799}).wait(1).to({graphics:mask_1_graphics_261,x:1027.6026,y:1228.6429}).wait(1).to({graphics:mask_1_graphics_262,x:1014.2391,y:1230.4135}).wait(1).to({graphics:mask_1_graphics_263,x:1000.3297,y:1232.0145}).wait(1).to({graphics:mask_1_graphics_264,x:985.9253,y:1233.4678}).wait(1).to({graphics:mask_1_graphics_265,x:971.0761,y:1234.7944}).wait(1).to({graphics:mask_1_graphics_266,x:955.8311,y:1236.014}).wait(1).to({graphics:mask_1_graphics_267,x:940.2378,y:1237.1455}).wait(1).to({graphics:mask_1_graphics_268,x:924.3428,y:1238.207}).wait(1).to({graphics:mask_1_graphics_269,x:908.1911,y:1239.2153}).wait(1).to({graphics:mask_1_graphics_270,x:891.8262,y:1240.1861}).wait(1).to({graphics:mask_1_graphics_271,x:879.3538,y:1245.198}).wait(1).to({graphics:mask_1_graphics_272,x:880.1592,y:1263.6098}).wait(1).to({graphics:mask_1_graphics_273,x:880.6219,y:1281.7751}).wait(1).to({graphics:mask_1_graphics_274,x:880.7554,y:1299.682}).wait(1).to({graphics:mask_1_graphics_275,x:880.5733,y:1317.3197}).wait(1).to({graphics:mask_1_graphics_276,x:880.0893,y:1334.6783}).wait(1).to({graphics:mask_1_graphics_277,x:879.3175,y:1351.7488}).wait(1).to({graphics:mask_1_graphics_278,x:878.2695,y:1368.5227}).wait(1).to({graphics:null,x:0,y:0}).wait(13).to({graphics:mask_1_graphics_292,x:940.2695,y:1448.5227}).wait(1).to({graphics:mask_1_graphics_293,x:940.3432,y:1455.5947}).wait(1).to({graphics:mask_1_graphics_294,x:940.4146,y:1462.6662}).wait(1).to({graphics:mask_1_graphics_295,x:940.4861,y:1469.7376}).wait(1).to({graphics:mask_1_graphics_296,x:940.5575,y:1476.809}).wait(1).to({graphics:mask_1_graphics_297,x:940.6289,y:1483.8805}).wait(1).to({graphics:mask_1_graphics_298,x:940.7003,y:1490.9519}).wait(1).to({graphics:mask_1_graphics_299,x:940.7718,y:1498.0233}).wait(1).to({graphics:mask_1_graphics_300,x:940.8432,y:1505.0947}).wait(1).to({graphics:mask_1_graphics_301,x:940.9146,y:1512.1662}).wait(1).to({graphics:mask_1_graphics_302,x:940.9861,y:1519.2376}).wait(1).to({graphics:mask_1_graphics_303,x:941.0575,y:1526.309}).wait(1).to({graphics:mask_1_graphics_304,x:941.1289,y:1533.3805}).wait(1).to({graphics:mask_1_graphics_305,x:941.2003,y:1540.4519}).wait(1).to({graphics:mask_1_graphics_306,x:941.2718,y:1547.5233}).wait(1).to({graphics:mask_1_graphics_307,x:941.3432,y:1554.5947}).wait(1).to({graphics:mask_1_graphics_308,x:941.4146,y:1561.6662}).wait(1).to({graphics:mask_1_graphics_309,x:941.4861,y:1568.7376}).wait(1).to({graphics:mask_1_graphics_310,x:941.5575,y:1575.809}).wait(1).to({graphics:mask_1_graphics_311,x:941.6289,y:1582.8805}).wait(1).to({graphics:mask_1_graphics_312,x:941.7003,y:1589.9519}).wait(1).to({graphics:mask_1_graphics_313,x:941.7718,y:1597.0233}).wait(1).to({graphics:mask_1_graphics_314,x:941.8432,y:1604.0947}).wait(1).to({graphics:mask_1_graphics_315,x:941.9146,y:1611.1662}).wait(1).to({graphics:mask_1_graphics_316,x:941.9861,y:1618.2376}).wait(1).to({graphics:mask_1_graphics_317,x:942.0575,y:1625.309}).wait(1).to({graphics:mask_1_graphics_318,x:942.1289,y:1632.3805}).wait(1).to({graphics:mask_1_graphics_319,x:942.2003,y:1639.4519}).wait(1).to({graphics:mask_1_graphics_320,x:942.2695,y:1646.5227}).wait(1).to({graphics:null,x:0,y:0}).wait(53).to({graphics:mask_1_graphics_374,x:1292.3483,y:1870.4408}).wait(1).to({graphics:mask_1_graphics_375,x:1325.7294,y:1871.4339}).wait(1).to({graphics:mask_1_graphics_376,x:1353.3067,y:1872.4268}).wait(1).to({graphics:mask_1_graphics_377,x:1374.705,y:1873.4196}).wait(1).to({graphics:mask_1_graphics_378,x:1389.6306,y:1874.3632}).wait(1).to({graphics:mask_1_graphics_379,x:1395.0015,y:1877.3673}).wait(1).to({graphics:mask_1_graphics_380,x:1405.166,y:1889.038}).wait(1).to({graphics:mask_1_graphics_381,x:1449.9112,y:1940.2924}).wait(1).to({graphics:mask_1_graphics_382,x:1491.8577,y:1994.9234}).wait(1).to({graphics:mask_1_graphics_383,x:1528.8869,y:2052.0582}).wait(1).to({graphics:null,x:0,y:0}).wait(12).to({graphics:mask_1_graphics_396,x:1424.2484,y:2107.9}).wait(1).to({graphics:mask_1_graphics_397,x:1413.3189,y:2100.7093}).wait(1).to({graphics:mask_1_graphics_398,x:1402.382,y:2093.5163}).wait(1).to({graphics:mask_1_graphics_399,x:1391.4451,y:2086.3233}).wait(1).to({graphics:mask_1_graphics_400,x:1380.5082,y:2079.1303}).wait(1).to({graphics:mask_1_graphics_401,x:1369.5713,y:2071.9373}).wait(1).to({graphics:mask_1_graphics_402,x:1358.6344,y:2064.7442}).wait(1).to({graphics:mask_1_graphics_403,x:1347.6975,y:2057.5512}).wait(1).to({graphics:mask_1_graphics_404,x:1336.7606,y:2050.3582}).wait(1).to({graphics:mask_1_graphics_405,x:1325.8237,y:2043.1652}).wait(1).to({graphics:mask_1_graphics_406,x:1314.8881,y:2035.9466}).wait(4).to({graphics:mask_1_graphics_410,x:1294.8881,y:2015.9466}).wait(2).to({graphics:null,x:0,y:0}).wait(20).to({graphics:mask_1_graphics_432,x:1009.345,y:2068.3744}).wait(1).to({graphics:mask_1_graphics_433,x:1022.0842,y:2068.3708}).wait(1).to({graphics:mask_1_graphics_434,x:1033.1344,y:2068.3675}).wait(1).to({graphics:mask_1_graphics_435,x:1042.4479,y:2068.3645}).wait(1).to({graphics:mask_1_graphics_436,x:1049.9864,y:2068.362}).wait(1).to({graphics:mask_1_graphics_437,x:1055.7187,y:2068.3598}).wait(1).to({graphics:mask_1_graphics_438,x:1059.6211,y:2068.3581}).wait(1).to({graphics:mask_1_graphics_439,x:1061.6775,y:2068.3568}).wait(1).to({graphics:mask_1_graphics_440,x:1073.261,y:2079.7189}).wait(1).to({graphics:null,x:0,y:0}).wait(25).to({graphics:mask_1_graphics_466,x:1104.947,y:2333.7905}).wait(1).to({graphics:mask_1_graphics_467,x:1107.2968,y:2340.5795}).wait(1).to({graphics:mask_1_graphics_468,x:1108.4135,y:2347.2053}).wait(1).to({graphics:mask_1_graphics_469,x:1108.4025,y:2353.6677}).wait(1).to({graphics:mask_1_graphics_470,x:1107.362,y:2359.9668}).wait(1).to({graphics:mask_1_graphics_471,x:1105.3878,y:2366.1024}).wait(1).to({graphics:mask_1_graphics_472,x:1102.5734,y:2372.0747}).wait(1).to({graphics:mask_1_graphics_473,x:1099.0091,y:2377.8834}).wait(1).to({graphics:mask_1_graphics_474,x:1094.7819,y:2383.5287}).wait(1).to({graphics:mask_1_graphics_475,x:1089.9752,y:2389.0105}).wait(1).to({graphics:mask_1_graphics_476,x:1084.6688,y:2394.3287}).wait(1).to({graphics:mask_1_graphics_477,x:1078.9381,y:2399.4834}).wait(1).to({graphics:mask_1_graphics_478,x:1072.8548,y:2404.4745}).wait(1).to({graphics:mask_1_graphics_479,x:1066.4863,y:2409.302}).wait(1).to({graphics:mask_1_graphics_480,x:1059.8989,y:2413.8952}).wait(1).to({graphics:mask_1_graphics_481,x:1067.6254,y:2416.7493}).wait(1).to({graphics:mask_1_graphics_482,x:1075.2772,y:2419.6035}).wait(1).to({graphics:mask_1_graphics_483,x:1082.8504,y:2422.4577}).wait(1).to({graphics:mask_1_graphics_484,x:1090.344,y:2425.3119}).wait(1).to({graphics:mask_1_graphics_485,x:1097.7566,y:2428.1661}).wait(1).to({graphics:mask_1_graphics_486,x:1105.0874,y:2431.0203}).wait(1).to({graphics:mask_1_graphics_487,x:1112.3352,y:2433.8746}).wait(1).to({graphics:mask_1_graphics_488,x:1119.4989,y:2436.7289}).wait(1).to({graphics:mask_1_graphics_489,x:1126.5776,y:2439.5833}).wait(1).to({graphics:mask_1_graphics_490,x:1133.5702,y:2442.4376}).wait(1).to({graphics:mask_1_graphics_491,x:1140.4757,y:2445.292}).wait(1).to({graphics:mask_1_graphics_492,x:1147.2931,y:2448.1464}).wait(1).to({graphics:mask_1_graphics_493,x:1154.0216,y:2451.0009}).wait(1).to({graphics:mask_1_graphics_494,x:1160.6153,y:2453.8821}).wait(1).to({graphics:mask_1_graphics_495,x:1164.5208,y:2458.5139}).wait(1).to({graphics:mask_1_graphics_496,x:1168.1981,y:2463.1255}).wait(1).to({graphics:mask_1_graphics_497,x:1171.6498,y:2467.7169}).wait(1).to({graphics:mask_1_graphics_498,x:1174.876,y:2472.2882}).wait(1).to({graphics:mask_1_graphics_499,x:1177.8768,y:2476.8394}).wait(1).to({graphics:mask_1_graphics_500,x:1180.6528,y:2481.3704}).wait(1).to({graphics:mask_1_graphics_501,x:1183.2045,y:2485.8812}).wait(1).to({graphics:mask_1_graphics_502,x:1185.5325,y:2490.372}).wait(1).to({graphics:mask_1_graphics_503,x:1187.6379,y:2494.8425}).wait(1).to({graphics:mask_1_graphics_504,x:1189.5214,y:2499.293}).wait(1).to({graphics:mask_1_graphics_505,x:1191.1845,y:2503.7232}).wait(1).to({graphics:mask_1_graphics_506,x:1192.6282,y:2508.1334}).wait(1).to({graphics:mask_1_graphics_507,x:1193.8542,y:2512.5234}).wait(1).to({graphics:mask_1_graphics_508,x:1194.8639,y:2516.8933}).wait(1).to({graphics:mask_1_graphics_509,x:1195.659,y:2521.243}).wait(1).to({graphics:mask_1_graphics_510,x:1196.2413,y:2525.5726}).wait(1).to({graphics:mask_1_graphics_511,x:1196.6129,y:2529.8821}).wait(1).to({graphics:mask_1_graphics_512,x:1196.7756,y:2534.1714}).wait(1).to({graphics:mask_1_graphics_513,x:1196.7317,y:2538.4406}).wait(1).to({graphics:mask_1_graphics_514,x:1196.4834,y:2542.6897}).wait(1).to({graphics:mask_1_graphics_515,x:1196.033,y:2546.9186}).wait(1).to({graphics:mask_1_graphics_516,x:1195.383,y:2551.1274}).wait(1).to({graphics:mask_1_graphics_517,x:1194.5359,y:2555.3161}).wait(1).to({graphics:mask_1_graphics_518,x:1193.4944,y:2559.4846}).wait(1).to({graphics:mask_1_graphics_519,x:1192.2611,y:2563.633}).wait(1).to({graphics:mask_1_graphics_520,x:1194.3091,y:2571.2388}).wait(1).to({graphics:null,x:0,y:0}).wait(39).to({graphics:mask_1_graphics_560,x:1163.0586,y:2917.035}).wait(1).to({graphics:mask_1_graphics_561,x:1150.8445,y:2910.9345}).wait(1).to({graphics:mask_1_graphics_562,x:1136.7131,y:2904.272}).wait(1).to({graphics:mask_1_graphics_563,x:1120.862,y:2897.1436}).wait(1).to({graphics:mask_1_graphics_564,x:1103.4913,y:2889.6414}).wait(1).to({graphics:mask_1_graphics_565,x:1084.7965,y:2881.8522}).wait(1).to({graphics:mask_1_graphics_566,x:1064.9679,y:2873.8566}).wait(1).to({graphics:mask_1_graphics_567,x:1044.1885,y:2865.7293}).wait(1).to({graphics:mask_1_graphics_568,x:1022.6336,y:2857.5386}).wait(1).to({graphics:mask_1_graphics_569,x:1000.4694,y:2849.3467}).wait(1).to({graphics:mask_1_graphics_570,x:977.8528,y:2841.2095}).wait(1).to({graphics:mask_1_graphics_571,x:954.931,y:2833.1768}).wait(1).to({graphics:mask_1_graphics_572,x:931.8407,y:2825.2924}).wait(1).to({graphics:mask_1_graphics_573,x:910.7813,y:2819.6677}).wait(1).to({graphics:mask_1_graphics_574,x:910.4456,y:2834.9118}).wait(1).to({graphics:mask_1_graphics_575,x:909.571,y:2849.684}).wait(1).to({graphics:mask_1_graphics_576,x:908.1519,y:2863.9442}).wait(1).to({graphics:null,x:0,y:0}).wait(29).to({graphics:mask_1_graphics_606,x:1179.6483,y:3088.0165}).wait(1).to({graphics:mask_1_graphics_607,x:1202.1361,y:3106.7258}).wait(1).to({graphics:mask_1_graphics_608,x:1222.7956,y:3125.0611}).wait(1).to({graphics:mask_1_graphics_609,x:1241.5053,y:3142.9469}).wait(1).to({graphics:mask_1_graphics_610,x:1258.1527,y:3160.3094}).wait(1).to({graphics:mask_1_graphics_611,x:1272.6355,y:3177.0777}).wait(1).to({graphics:mask_1_graphics_612,x:1284.8625,y:3193.1836}).wait(1).to({graphics:mask_1_graphics_613,x:1294.7541,y:3208.5627}).wait(1).to({graphics:mask_1_graphics_614,x:1302.2423,y:3223.154}).wait(1).to({graphics:mask_1_graphics_615,x:1307.2715,y:3236.9005}).wait(1).to({graphics:mask_1_graphics_616,x:1309.7984,y:3249.7497}).wait(1).to({graphics:mask_1_graphics_617,x:1309.7927,y:3261.6534}).wait(1).to({graphics:mask_1_graphics_618,x:1307.2368,y:3272.5683}).wait(1).to({graphics:mask_1_graphics_619,x:1302.1258,y:3282.4562}).wait(1).to({graphics:mask_1_graphics_620,x:1294.4258,y:3291.2427}).wait(1).to({graphics:mask_1_graphics_621,x:1283.5023,y:3290.2747}).wait(1).to({graphics:mask_1_graphics_622,x:1272.0608,y:3289.0804}).wait(1).to({graphics:mask_1_graphics_623,x:1260.1083,y:3287.6591}).wait(1).to({graphics:mask_1_graphics_624,x:1247.6485,y:3286.0092}).wait(1).to({graphics:mask_1_graphics_625,x:1234.686,y:3284.1291}).wait(1).to({graphics:mask_1_graphics_626,x:1221.2256,y:3282.0177}).wait(1).to({graphics:mask_1_graphics_627,x:1207.2727,y:3279.6739}).wait(1).to({graphics:mask_1_graphics_628,x:1192.8335,y:3277.097}).wait(1).to({graphics:mask_1_graphics_629,x:1177.9143,y:3274.2863}).wait(1).to({graphics:mask_1_graphics_630,x:1167.651,y:3276.3706}).wait(1).to({graphics:mask_1_graphics_631,x:1166.3037,y:3287.6023}).wait(1).to({graphics:mask_1_graphics_632,x:1164.4784,y:3298.5798}).wait(1).to({graphics:mask_1_graphics_633,x:1162.1686,y:3309.2886}).wait(1).to({graphics:mask_1_graphics_634,x:1159.3682,y:3319.7143}).wait(1).to({graphics:mask_1_graphics_635,x:1156.0714,y:3329.8428}).wait(1).to({graphics:mask_1_graphics_636,x:1152.2647,y:3339.6826}).wait(1).to({graphics:null,x:0,y:0}).wait(11).to({graphics:mask_1_graphics_648,x:920.2647,y:3201.6826}).wait(1).to({graphics:mask_1_graphics_649,x:903.2523,y:3194.0516}).wait(1).to({graphics:mask_1_graphics_650,x:886.0877,y:3186.3516}).wait(1).to({graphics:mask_1_graphics_651,x:868.7745,y:3178.5849}).wait(1).to({graphics:mask_1_graphics_652,x:851.3127,y:3170.7516}).wait(1).to({graphics:mask_1_graphics_653,x:833.7023,y:3162.8516}).wait(1).to({graphics:mask_1_graphics_654,x:815.9432,y:3154.8849}).wait(1).to({graphics:mask_1_graphics_655,x:798.0356,y:3146.8516}).wait(1).to({graphics:mask_1_graphics_656,x:779.9793,y:3138.7516}).wait(1).to({graphics:mask_1_graphics_657,x:761.7745,y:3130.5849}).wait(1).to({graphics:mask_1_graphics_658,x:743.421,y:3122.3516}).wait(1).to({graphics:mask_1_graphics_659,x:724.9189,y:3114.0516}).wait(1).to({graphics:mask_1_graphics_660,x:706.2647,y:3105.6826}).wait(1).to({graphics:null,x:0,y:0}).wait(11).to({graphics:mask_1_graphics_672,x:411.1431,y:2874.4534}).wait(1).to({graphics:mask_1_graphics_673,x:392.2927,y:2901.692}).wait(1).to({graphics:mask_1_graphics_674,x:373.5005,y:2929.0569}).wait(1).to({graphics:mask_1_graphics_675,x:354.999,y:2956.4321}).wait(1).to({graphics:mask_1_graphics_676,x:337.0292,y:2983.697}).wait(1).to({graphics:mask_1_graphics_677,x:319.8398,y:3010.7273}).wait(1).to({graphics:mask_1_graphics_678,x:303.6848,y:3037.3959}).wait(1).to({graphics:mask_1_graphics_679,x:288.8224,y:3063.5739}).wait(1).to({graphics:mask_1_graphics_680,x:275.5122,y:3089.1313}).wait(1).to({graphics:mask_1_graphics_681,x:264.0136,y:3113.9385}).wait(1).to({graphics:mask_1_graphics_682,x:254.5834,y:3137.8671}).wait(1).to({graphics:mask_1_graphics_683,x:247.4731,y:3160.7913}).wait(1).to({graphics:mask_1_graphics_684,x:242.8549,y:3182.5975}).wait(1).to({graphics:null,x:0,y:0}).wait(9).to({graphics:mask_1_graphics_694,x:384.1906,y:3356.6797}).wait(1).to({graphics:mask_1_graphics_695,x:393.6079,y:3367.5547}).wait(1).to({graphics:mask_1_graphics_696,x:402.945,y:3378.0944}).wait(1).to({graphics:mask_1_graphics_697,x:412.1894,y:3388.3048}).wait(1).to({graphics:mask_1_graphics_698,x:421.3292,y:3398.1921}).wait(1).to({graphics:mask_1_graphics_699,x:430.3526,y:3407.7621}).wait(1).to({graphics:mask_1_graphics_700,x:439.2485,y:3417.0204}).wait(1).to({graphics:mask_1_graphics_701,x:448.0061,y:3425.9722}).wait(1).to({graphics:mask_1_graphics_702,x:456.6153,y:3434.6228}).wait(1).to({graphics:mask_1_graphics_703,x:465.0663,y:3442.977}).wait(1).to({graphics:mask_1_graphics_704,x:473.3498,y:3451.0394}).wait(1).to({graphics:mask_1_graphics_705,x:481.457,y:3458.8145}).wait(1).to({graphics:mask_1_graphics_706,x:489.3793,y:3466.3065}).wait(1).to({graphics:mask_1_graphics_707,x:497.1088,y:3473.5194}).wait(1).to({graphics:mask_1_graphics_708,x:504.6,y:3480.475}).wait(1).to({graphics:null,x:0,y:0}).wait(11).to({graphics:mask_1_graphics_720,x:698.0651,y:3642.1677}).wait(1).to({graphics:mask_1_graphics_721,x:710.0216,y:3655.0586}).wait(1).to({graphics:mask_1_graphics_722,x:721.9241,y:3667.6038}).wait(1).to({graphics:mask_1_graphics_723,x:733.7655,y:3679.7935}).wait(1).to({graphics:mask_1_graphics_724,x:745.538,y:3691.6148}).wait(1).to({graphics:mask_1_graphics_725,x:757.2344,y:3703.0549}).wait(1).to({graphics:mask_1_graphics_726,x:768.847,y:3714.1015}).wait(1).to({graphics:mask_1_graphics_727,x:780.3685,y:3724.7429}).wait(1).to({graphics:mask_1_graphics_728,x:791.8241,y:3734.9747}).wait(1).to({graphics:null,x:0,y:0}).wait(13).to({graphics:mask_1_graphics_742,x:1086.2541,y:3571.6}).wait(1).to({graphics:mask_1_graphics_743,x:1099.9227,y:3596.3542}).wait(1).to({graphics:mask_1_graphics_744,x:1112.6654,y:3620.44}).wait(1).to({graphics:mask_1_graphics_745,x:1124.4821,y:3643.8359}).wait(1).to({graphics:mask_1_graphics_746,x:1135.3798,y:3666.5242}).wait(1).to({graphics:mask_1_graphics_747,x:1145.3719,y:3688.4908}).wait(1).to({graphics:mask_1_graphics_748,x:1154.4779,y:3709.7249}).wait(1).to({graphics:mask_1_graphics_749,x:1162.7221,y:3730.2187}).wait(1).to({graphics:mask_1_graphics_750,x:1170.1335,y:3749.9673}).wait(1).to({graphics:mask_1_graphics_751,x:1176.7448,y:3768.9684}).wait(1).to({graphics:mask_1_graphics_752,x:1182.5775,y:3787.1883}).wait(1).to({graphics:mask_1_graphics_753,x:1187.1031,y:3803.752}).wait(1).to({graphics:mask_1_graphics_754,x:1191.2177,y:3820.2128}).wait(1).to({graphics:mask_1_graphics_755,x:1194.914,y:3836.5648}).wait(1).to({graphics:mask_1_graphics_756,x:1198.1828,y:3852.8017}).wait(1).to({graphics:mask_1_graphics_757,x:1201.0155,y:3868.9172}).wait(1).to({graphics:mask_1_graphics_758,x:1203.4038,y:3884.9054}).wait(1).to({graphics:mask_1_graphics_759,x:1205.3398,y:3900.7601}).wait(1).to({graphics:mask_1_graphics_760,x:1206.7636,y:3916.4977}).wait(1).to({graphics:null,x:0,y:0}).wait(29).to({graphics:mask_1_graphics_790,x:1037.2007,y:3975.7833}).wait(1).to({graphics:mask_1_graphics_791,x:1025.9382,y:3975.5964}).wait(1).to({graphics:mask_1_graphics_792,x:1014.4125,y:3975.3458}).wait(1).to({graphics:mask_1_graphics_793,x:1002.6379,y:3975.0382}).wait(1).to({graphics:mask_1_graphics_794,x:990.6304,y:3974.6807}).wait(1).to({graphics:mask_1_graphics_795,x:978.406,y:3974.2802}).wait(1).to({graphics:mask_1_graphics_796,x:965.9806,y:3973.8435}).wait(1).to({graphics:mask_1_graphics_797,x:953.3698,y:3973.3772}).wait(1).to({graphics:mask_1_graphics_798,x:940.5892,y:3972.8876}).wait(1).to({graphics:mask_1_graphics_799,x:927.654,y:3972.3808}).wait(1).to({graphics:mask_1_graphics_800,x:914.5793,y:3971.863}).wait(1).to({graphics:mask_1_graphics_801,x:901.3798,y:3971.3398}).wait(1).to({graphics:mask_1_graphics_802,x:893.5873,y:3976.3339}).wait(1).to({graphics:mask_1_graphics_803,x:893.052,y:3988.6869}).wait(1).to({graphics:mask_1_graphics_804,x:892.3087,y:4000.9551}).wait(1).to({graphics:mask_1_graphics_805,x:895.2527,y:4008.5201}).wait(1).to({graphics:mask_1_graphics_806,x:898.1711,y:4016.0615}).wait(1).to({graphics:mask_1_graphics_807,x:901.0683,y:4023.5794}).wait(1).to({graphics:mask_1_graphics_808,x:903.9444,y:4031.0739}).wait(1).to({graphics:mask_1_graphics_809,x:906.7995,y:4038.5449}).wait(1).to({graphics:mask_1_graphics_810,x:909.6338,y:4045.9921}).wait(1).to({graphics:mask_1_graphics_811,x:912.4475,y:4053.4157}).wait(1).to({graphics:mask_1_graphics_812,x:915.2405,y:4060.8154}).wait(1).to({graphics:mask_1_graphics_813,x:918.0131,y:4068.1913}).wait(1).to({graphics:mask_1_graphics_814,x:920.7653,y:4075.5432}).wait(1).to({graphics:mask_1_graphics_815,x:923.4973,y:4082.8711}).wait(1).to({graphics:mask_1_graphics_816,x:926.2092,y:4090.1749}).wait(1).to({graphics:mask_1_graphics_817,x:928.9011,y:4097.4545}).wait(1).to({graphics:mask_1_graphics_818,x:931.5732,y:4104.7099}).wait(1).to({graphics:mask_1_graphics_819,x:934.2255,y:4111.941}).wait(1).to({graphics:mask_1_graphics_820,x:936.8524,y:4119.123}).wait(1).to({graphics:mask_1_graphics_821,x:939.3023,y:4125.8714}).wait(1).to({graphics:mask_1_graphics_822,x:941.7411,y:4132.6176}).wait(1).to({graphics:mask_1_graphics_823,x:944.1699,y:4139.3618}).wait(1).to({graphics:mask_1_graphics_824,x:946.5888,y:4146.1039}).wait(1).to({graphics:mask_1_graphics_825,x:948.9977,y:4152.8439}).wait(1).to({graphics:mask_1_graphics_826,x:951.3966,y:4159.5816}).wait(1).to({graphics:mask_1_graphics_827,x:953.7856,y:4166.317}).wait(1).to({graphics:mask_1_graphics_828,x:956.1646,y:4173.0501}).wait(1).to({graphics:mask_1_graphics_829,x:958.5338,y:4179.7807}).wait(1).to({graphics:mask_1_graphics_830,x:960.8931,y:4186.5088}).wait(1).to({graphics:mask_1_graphics_831,x:963.2425,y:4193.2344}).wait(1).to({graphics:mask_1_graphics_832,x:965.5821,y:4199.9574}).wait(1).to({graphics:mask_1_graphics_833,x:967.9118,y:4206.6776}).wait(1).to({graphics:mask_1_graphics_834,x:970.2318,y:4213.3951}).wait(1).to({graphics:mask_1_graphics_835,x:972.5419,y:4220.1097}).wait(1).to({graphics:mask_1_graphics_836,x:974.8423,y:4226.8215}).wait(1).to({graphics:mask_1_graphics_837,x:977.1329,y:4233.5303}).wait(1).to({graphics:mask_1_graphics_838,x:979.4137,y:4240.2361}).wait(1).to({graphics:mask_1_graphics_839,x:981.6849,y:4246.9388}).wait(1).to({graphics:mask_1_graphics_840,x:983.9463,y:4253.6383}).wait(1).to({graphics:mask_1_graphics_841,x:986.1981,y:4260.3346}).wait(1).to({graphics:mask_1_graphics_842,x:988.4402,y:4267.0276}).wait(1).to({graphics:mask_1_graphics_843,x:990.6727,y:4273.7173}).wait(1).to({graphics:mask_1_graphics_844,x:992.8956,y:4280.4035}).wait(1).to({graphics:mask_1_graphics_845,x:995.109,y:4287.0863}).wait(1).to({graphics:mask_1_graphics_846,x:997.3127,y:4293.7655}).wait(1).to({graphics:mask_1_graphics_847,x:999.5069,y:4300.4411}).wait(1).to({graphics:mask_1_graphics_848,x:1001.6916,y:4307.113}).wait(1).to({graphics:mask_1_graphics_849,x:1003.8669,y:4313.7812}).wait(1).to({graphics:mask_1_graphics_850,x:1006.0326,y:4320.4456}).wait(1).to({graphics:mask_1_graphics_851,x:1008.189,y:4327.1062}).wait(1).to({graphics:mask_1_graphics_852,x:1010.3359,y:4333.7628}).wait(1).to({graphics:mask_1_graphics_853,x:1012.4734,y:4340.4153}).wait(1).to({graphics:mask_1_graphics_854,x:1014.6016,y:4347.0639}).wait(1).to({graphics:mask_1_graphics_855,x:1016.7205,y:4353.7083}).wait(1).to({graphics:mask_1_graphics_856,x:1018.83,y:4360.3486}).wait(1).to({graphics:mask_1_graphics_857,x:1020.9303,y:4366.9846}).wait(1).to({graphics:mask_1_graphics_858,x:1023.0214,y:4373.6162}).wait(1).to({graphics:mask_1_graphics_859,x:1025.1032,y:4380.2436}).wait(1).to({graphics:mask_1_graphics_860,x:1027.1639,y:4386.8435}).wait(1).to({graphics:mask_1_graphics_861,x:1045.2313,y:4383.0362}).wait(1).to({graphics:mask_1_graphics_862,x:1062.9631,y:4376.8532}).wait(1).to({graphics:mask_1_graphics_863,x:1080.1233,y:4368.3019}).wait(1).to({graphics:mask_1_graphics_864,x:1096.4744,y:4357.4111}).wait(1).to({graphics:mask_1_graphics_865,x:1111.7807,y:4344.2314}).wait(1).to({graphics:mask_1_graphics_866,x:1125.8099,y:4328.8351}).wait(1).to({graphics:mask_1_graphics_867,x:1156.206,y:4337.3301}).wait(1).to({graphics:mask_1_graphics_868,x:1198.7984,y:4367.3111}).wait(1).to({graphics:mask_1_graphics_869,x:1239.2068,y:4399.1626}).wait(1).to({graphics:mask_1_graphics_870,x:1277.0731,y:4432.7297}).wait(1).to({graphics:mask_1_graphics_871,x:1312.0537,y:4467.8385}).wait(1).to({graphics:mask_1_graphics_872,x:1343.8221,y:4504.2976}).wait(1).to({graphics:mask_1_graphics_873,x:1372.0713,y:4541.8985}).wait(1).to({graphics:mask_1_graphics_874,x:1396.516,y:4580.4178}).wait(1).to({graphics:mask_1_graphics_875,x:1416.8946,y:4619.618}).wait(1).to({graphics:mask_1_graphics_876,x:1433.0574,y:4659.3837}).wait(1).to({graphics:null,x:0,y:0}).wait(11).to({graphics:mask_1_graphics_888,x:1065.2508,y:4474.1606}).wait(1).to({graphics:mask_1_graphics_889,x:1052.216,y:4483.3809}).wait(1).to({graphics:mask_1_graphics_890,x:1039.1465,y:4492.5891}).wait(1).to({graphics:mask_1_graphics_891,x:1026.0467,y:4501.7863}).wait(1).to({graphics:mask_1_graphics_892,x:1012.9166,y:4510.9722}).wait(1).to({graphics:mask_1_graphics_893,x:999.7565,y:4520.1465}).wait(1).to({graphics:mask_1_graphics_894,x:986.5664,y:4529.3086}).wait(1).to({graphics:mask_1_graphics_895,x:973.3466,y:4538.4584}).wait(1).to({graphics:mask_1_graphics_896,x:960.0972,y:4547.5954}).wait(1).to({graphics:mask_1_graphics_897,x:946.8182,y:4556.7193}).wait(1).to({graphics:mask_1_graphics_898,x:933.51,y:4565.8297}).wait(1).to({graphics:mask_1_graphics_899,x:920.1726,y:4574.9263}).wait(1).to({graphics:mask_1_graphics_900,x:906.8389,y:4584.0194}).wait(1).to({graphics:null,x:0,y:0}).wait(9).to({graphics:mask_1_graphics_910,x:595.6859,y:4653.8356}).wait(1).to({graphics:mask_1_graphics_911,x:637.6859,y:4659.8356}).wait(1).to({graphics:mask_1_graphics_912,x:462.449,y:4798.1308}).wait(1).to({graphics:mask_1_graphics_913,x:516.8379,y:4791.975}).wait(1).to({graphics:mask_1_graphics_914,x:567.8026,y:4785.9695}).wait(1).to({graphics:mask_1_graphics_915,x:600.1037,y:4780.1686}).wait(1).to({graphics:mask_1_graphics_916,x:624.8683,y:4774.6232}).wait(1).to({graphics:mask_1_graphics_917,x:647.4385,y:4769.3817}).wait(1).to({graphics:mask_1_graphics_918,x:667.8341,y:4764.5107}).wait(1).to({graphics:null,x:0,y:0}).wait(12).to({graphics:mask_1_graphics_931,x:937.2364,y:4699.6777}).wait(1).to({graphics:mask_1_graphics_932,x:933.2364,y:4717.6777}).wait(1).to({graphics:mask_1_graphics_933,x:931.2364,y:4735.6777}).wait(1).to({graphics:null,x:0,y:0}).wait(4).to({graphics:mask_1_graphics_938,x:891.5141,y:4955.8256}).wait(1).to({graphics:mask_1_graphics_939,x:902.2638,y:4962.3252}).wait(1).to({graphics:mask_1_graphics_940,x:913.0138,y:4968.8252}).wait(1).to({graphics:mask_1_graphics_941,x:923.7638,y:4975.3252}).wait(1).to({graphics:mask_1_graphics_942,x:934.5138,y:4981.8252}).wait(1).to({graphics:mask_1_graphics_943,x:945.2638,y:4988.3252}).wait(1).to({graphics:mask_1_graphics_944,x:956.0138,y:4994.8252}).wait(1).to({graphics:mask_1_graphics_945,x:966.7638,y:5001.3252}).wait(1).to({graphics:mask_1_graphics_946,x:977.5141,y:5007.8256}).wait(1).to({graphics:null,x:0,y:0}).wait(53));

	// Layer_10
	this.instance_1 = new lib.element_2("synched",0);
	this.instance_1.setTransform(1134.5,4998.7,1,1,0,0,0,1134.5,4998.7);
	this.instance_1._off = true;

	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#D3BD93","#966A40"],[0,0.529],-3493,3123.6,2647.2,4236.7).s().p("EgZwA8bQn/mgpNtGIg+haQhMhshEiMQjElGhImgQgykaAAmgQAAmTBunUQBpm6DLn2QHSx+NQw4QBChUDekKQDgkUCYjaQCxj+BujcIFXAAQkAFvmGHvQ2wc2nsUCQhjEBhBD1QlQTgH+PLQBhC0AxA6IBPBZQNaO9JLEqQEcCQFCA2QDuAqGhAIQEqAGFqhYQHSh0F0jwQQCqWAC1iQAAouoMjuQn0jkt0BoQrVBau2ImQmWDrq/HrIiXjuICWhuQG8kuQ+pyQGOjmIjioQJOi4GqAAQLiAAHiFGQJ0GqAAOCQAAIQkOIMQjQGWmgHeQjyEWq0EIQrYEWpGAAQvLgCssqUg");
	this.shape.setTransform(1941.7,1585.275);

	var maskedShapeInstanceList = [this.instance_1,this.shape];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_1;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_1}]},1).to({state:[]},41).to({state:[{t:this.instance_1}]},18).to({state:[]},12).to({state:[{t:this.instance_1}]},10).to({state:[]},20).to({state:[{t:this.instance_1}]},10).to({state:[]},16).to({state:[{t:this.instance_1}]},8).to({state:[]},20).to({state:[{t:this.shape}]},10).to({state:[]},26).to({state:[{t:this.shape}]},26).to({state:[{t:this.instance_1}]},19).to({state:[]},1).to({state:[{t:this.instance_1}]},20).to({state:[]},20).to({state:[{t:this.instance_1}]},14).to({state:[]},28).to({state:[{t:this.instance_1}]},54).to({state:[]},10).to({state:[{t:this.instance_1}]},12).to({state:[]},16).to({state:[{t:this.instance_1}]},20).to({state:[]},8).to({state:[{t:this.instance_1}]},26).to({state:[]},54).to({state:[{t:this.instance_1}]},40).to({state:[]},16).to({state:[{t:this.instance_1}]},30).to({state:[]},30).to({state:[{t:this.instance_1}]},12).to({state:[]},12).to({state:[{t:this.instance_1}]},12).to({state:[]},12).to({state:[{t:this.instance_1}]},10).to({state:[]},14).to({state:[{t:this.instance_1}]},12).to({state:[]},8).to({state:[{t:this.instance_1}]},14).to({state:[]},18).to({state:[{t:this.instance_1}]},30).to({state:[]},86).to({state:[{t:this.instance_1}]},12).to({state:[]},12).to({state:[{t:this.instance_1}]},10).to({state:[]},8).to({state:[{t:this.instance_1}]},13).to({state:[]},3).to({state:[{t:this.instance_1}]},4).to({state:[]},8).wait(54));
	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1).to({_off:false},0).to({_off:true},41).wait(18).to({_off:false},0).to({_off:true},12).wait(10).to({_off:false},0).to({_off:true},20).wait(10).to({_off:false},0).to({_off:true},16).wait(8).to({_off:false},0).to({_off:true},20).wait(81).to({_off:false},0).to({_off:true},1).wait(20).to({_off:false},0).to({_off:true},20).wait(14).to({_off:false},0).to({_off:true},28).wait(54).to({_off:false},0).to({_off:true},10).wait(12).to({_off:false},0).to({_off:true},16).wait(20).to({_off:false},0).to({_off:true},8).wait(26).to({_off:false},0).to({_off:true},54).wait(40).to({_off:false},0).to({_off:true},16).wait(30).to({_off:false},0).to({_off:true},30).wait(12).to({_off:false},0).to({_off:true},12).wait(12).to({_off:false},0).to({_off:true},12).wait(10).to({_off:false},0).to({_off:true},14).wait(12).to({_off:false},0).to({_off:true},8).wait(14).to({_off:false},0).to({_off:true},18).wait(30).to({_off:false},0).to({_off:true},86).wait(12).to({_off:false},0).to({_off:true},12).wait(10).to({_off:false},0).to({_off:true},8).wait(13).to({_off:false},0).to({_off:true},3).wait(4).to({_off:false},0).to({_off:true},8).wait(54));

	// Layer_21 (mask)
	var mask_2 = new cjs.Shape();
	mask_2._off = true;
	var mask_2_graphics_42 = new cjs.Graphics().p("AjrpBMBsIhaFMBaFBsIMhsJBaFg");
	var mask_2_graphics_43 = new cjs.Graphics().p("AgtvJMBxghTNMBTNBxgMhxhBTNg");
	var mask_2_graphics_44 = new cjs.Graphics().p("ACu1OMB2dhMAMBMBB2dMh2eBMAg");
	var mask_2_graphics_45 = new cjs.Graphics().p("AGm7NMB68hEhMBEhB68Mh68BEhg");
	var mask_2_graphics_46 = new cjs.Graphics().p("EAK6ghFMB+9g8xMA8xB+8Mh+9A8xg");
	var mask_2_graphics_47 = new cjs.Graphics().p("EAPpgm2MCCfg0xMA0xCCeMiCfA0xg");
	var mask_2_graphics_48 = new cjs.Graphics().p("EAUygscMCFggsmMAslCFfMiFgAsmg");
	var mask_2_graphics_49 = new cjs.Graphics().p("EAaTgx4MCIAgkOMAkPCH/MiIBAkOg");
	var mask_2_graphics_50 = new cjs.Graphics().p("EAgMg3IMCJ/gbuMAbuCJ/MiJ/Abug");
	var mask_2_graphics_51 = new cjs.Graphics().p("EAmbg8KMCLcgTHMATHCLcMiLcATHg");
	var mask_2_graphics_52 = new cjs.Graphics().p("EAs+hA9MCMXgKbMAKcCMWMiMXAKbg");
	var mask_2_graphics_53 = new cjs.Graphics().p("EAz1hFgMCMvgBtMABuCMuMiMwABtg");
	var mask_2_graphics_54 = new cjs.Graphics().p("EAwcBCyMAHBiMkMCMlAHBMgHCCMkg");
	var mask_2_graphics_55 = new cjs.Graphics().p("EAqxA+FMAPuiL3MCL3APuMgPuCL3g");
	var mask_2_graphics_56 = new cjs.Graphics().p("EAlaA5IMAYXiKnMCKoAYYMgYXCKng");
	var mask_2_graphics_57 = new cjs.Graphics().p("EAgZAz+MAg6iI1MCI2Ag6Mgg6CI1g");
	var mask_2_graphics_58 = new cjs.Graphics().p("EAbvAunMApViGiMCGiApVMgpVCGig");
	var mask_2_graphics_59 = new cjs.Graphics().p("EAXdApEMAxmiDtMCDuAxmMgxmCDtg");
	var mask_2_graphics_60 = new cjs.Graphics().p("EATjAjXMA5riAYMCAZA5rMg5rCAYg");
	var mask_2_graphics_72 = new cjs.Graphics().p("EgXrgxwMCH7gkbMAkcCH8MiH9Akbg");
	var mask_2_graphics_73 = new cjs.Graphics().p("EgmbgmEMCCAg13MA13CCAMiCBA13g");
	var mask_2_graphics_74 = new cjs.Graphics().p("EgzJgZvMB53hGYMBGYB53Mh55BGYg");
	var mask_2_graphics_75 = new cjs.Graphics().p("Eg9ogM+MBvphVsMBVrBvpMhvqBVsg");
	var mask_2_graphics_76 = new cjs.Graphics().p("EhFrAAAMBjghjgMBjhBjgMhjhBjhg");
	var mask_2_graphics_77 = new cjs.Graphics().p("EhLLAM/MBVqhvpMBvqBVsMhVrBvpg");
	var mask_2_graphics_78 = new cjs.Graphics().p("EhOCAZwMBGXh53MB54BGYMhGYB53g");
	var mask_2_graphics_79 = new cjs.Graphics().p("EhONAmFMA13iCAMCCBA13Mg13CCAg");
	var mask_2_graphics_80 = new cjs.Graphics().p("EhLsAxxMAkbiH8MCH8AkbMgkcCH8g");
	var mask_2_graphics_81 = new cjs.Graphics().p("EhGiA8lMASYiLhMCLhASYMgSYCLhg");
	var mask_2_graphics_82 = new cjs.Graphics().p("Eg+2BGYMAAAiMvMCMvAAAMAAACMvg");
	var mask_2_graphics_102 = new cjs.Graphics().p("EhI2gHaMBqphb0MBb0BqpMhqqBb0g");
	var mask_2_graphics_103 = new cjs.Graphics().p("EhGCgMNMBu9hWiMBWjBu9Mhu+BWig");
	var mask_2_graphics_104 = new cjs.Graphics().p("EhC1gQ+MBzBhREMBRFBzBMhzCBREg");
	var mask_2_graphics_105 = new cjs.Graphics().p("Eg/OgVsMB2zhLaMBLaB2zMh20BLag");
	var mask_2_graphics_106 = new cjs.Graphics().p("Eg7PgaXMB6UhFlMBFlB6UMh6VBFlg");
	var mask_2_graphics_107 = new cjs.Graphics().p("Eg23ge/MB9ig/kMA/kB9jMh9jA/kg");
	var mask_2_graphics_108 = new cjs.Graphics().p("EgyIgjhMCAeg5bMA5aCAeMiAfA5bg");
	var mask_2_graphics_109 = new cjs.Graphics().p("EgtCgn+MCDGgzJMAzICDGMiDHAzJg");
	var mask_2_graphics_110 = new cjs.Graphics().p("EgnlgsWMCFagsuMAsuCFbMiFbAsug");
	var mask_2_graphics_111 = new cjs.Graphics().p("EghzgwmMCHbgmOMAmOCHbMiHdAmOg");
	var mask_2_graphics_112 = new cjs.Graphics().p("Egbsg0wMCJIgfnMAfnCJIMiJJAfng");
	var mask_2_graphics_128 = new cjs.Graphics().p("Eg8JAAaMBm8hf7MBf8Bm8Mhm9Bf8g");
	var mask_2_graphics_129 = new cjs.Graphics().p("Eg65ABqMBm8hf7MBf8Bm8Mhm9Bf8g");
	var mask_2_graphics_130 = new cjs.Graphics().p("Eg7GAgQMBN+h1IMB1JBN+MhN/B1Ig");
	var mask_2_graphics_131 = new cjs.Graphics().p("Eg5OAgQMBN+h1IMB1JBN+MhN/B1Ig");
	var mask_2_graphics_132 = new cjs.Graphics().p("EgupA80MAwviD/MCEAAwwMgwxCD/g");
	var mask_2_graphics_133 = new cjs.Graphics().p("EgsxA80MAwviD/MCEAAwwMgwxCD/g");
	var mask_2_graphics_134 = new cjs.Graphics().p("EgbkBcuMAQ6iLsMCLsAQ6MgQ7CLsg");
	var mask_2_graphics_135 = new cjs.Graphics().p("EgaABcuMAQ6iLsMCLsAQ6MgQ7CLsg");
	var mask_2_graphics_156 = new cjs.Graphics().p("AHc+XMB9IhAYMBAZB9HMh9IBAYg");
	var mask_2_graphics_157 = new cjs.Graphics().p("EAPIglsMCByg2YMA2YCBxMiByA2Yg");
	var mask_2_graphics_158 = new cjs.Graphics().p("EAXjgszMCFqgsBMAsBCFoMiFqAsBg");
	var mask_2_graphics_159 = new cjs.Graphics().p("EAgpgxOMCItghZMAhZCIrMiIsAhag");
	var mask_2_graphics_160 = new cjs.Graphics().p("EAqYgzcMCK5gWlMAWlCK5MiK6AWkg");
	var mask_2_graphics_161 = new cjs.Graphics().p("EA0rg00MCMPgLnMALnCMOMiMPALng");
	var mask_2_graphics_162 = new cjs.Graphics().p("EA/eg1VMCMugAkMAAkCMtMiMuAAkg");
	var mask_2_graphics_163 = new cjs.Graphics().p("EA7ABSHMAKeiMUMCMVAKfMgKfCMUg");
	var mask_2_graphics_164 = new cjs.Graphics().p("EA2IBMmMAVeiLEMCLEAVdMgVdCLEg");
	var mask_2_graphics_165 = new cjs.Graphics().p("EAxxBHJMAgTiI9MCI+AgUMggUCI8g");
	var mask_2_graphics_166 = new cjs.Graphics().p("EAt7BByMAq9iF/MCGAAq9Mgq9CF/g");
	var mask_2_graphics_192 = new cjs.Graphics().p("EhAMBSHMBo/hdpMBdqBo+MhpABdrg");
	var mask_2_graphics_193 = new cjs.Graphics().p("Eg+oBTrMBo/hdpMBdqBo+MhpABdrg");
	var mask_2_graphics_194 = new cjs.Graphics().p("Eg9oBsvMBZShsvMBsuBZTMhZTBsvg");
	var mask_2_graphics_195 = new cjs.Graphics().p("Eg8EBuTMBZShsvMBsuBZTMhZTBsvg");
	var mask_2_graphics_196 = new cjs.Graphics().p("Eg0KCFDMBHdh5MMB5MBHdMhHeB5Ng");
	var mask_2_graphics_197 = new cjs.Graphics().p("EgymCGnMBHdh5MMB5MBHdMhHeB5Ng");
	var mask_2_graphics_198 = new cjs.Graphics().p("EgnBCdoMAz6iCwMCCwAz7Mgz7CCwg");
	var mask_2_graphics_199 = new cjs.Graphics().p("EgldCfMMAz6iCwMCCwAz7Mgz7CCwg");
	var mask_2_graphics_200 = new cjs.Graphics().p("EgV0C09MAfIiJNMCJMAfJMgfICJNg");
	var mask_2_graphics_201 = new cjs.Graphics().p("EgUQC2hMAfIiJNMCJMAfJMgfICJNg");
	var mask_2_graphics_202 = new cjs.Graphics().p("EgBODKIMAJmiMXMCMXAJnMgJnCMXg");
	var mask_2_graphics_203 = new cjs.Graphics().p("EAA9DKIMAJniMXMCMXAJnMgJnCMXg");
	var mask_2_graphics_204 = new cjs.Graphics().p("EAG+BK2MCMhgHHMAHHCMhMiMhAHHg");
	var mask_2_graphics_205 = new cjs.Graphics().p("EABZBSjMCLBgVyMAVxCLBMiLAAVxg");
	var mask_2_graphics_206 = new cjs.Graphics().p("EgCeBaxMCIAgkAMAkACIBMiIBAkAg");
	var mask_2_graphics_207 = new cjs.Graphics().p("EgEsBjbMCDogxsMAxrCDpMiDpAxrg");
	var mask_2_graphics_208 = new cjs.Graphics().p("EgFSBsYMB99g+rMA+rB9+Mh9+A+rg");
	var mask_2_graphics_209 = new cjs.Graphics().p("EgETB1hMB3IhK3MBK2B3JMh3JBK2g");
	var mask_2_graphics_210 = new cjs.Graphics().p("EgB1B+vMBvPhWJMBWJBvQMhvPBWJg");
	var mask_2_graphics_211 = new cjs.Graphics().p("EACECH9MBmahgeMBgeBmbMhmaBgeg");
	var mask_2_graphics_212 = new cjs.Graphics().p("EAHRCRFMBcyhpyMBpxBcyMhcxBpxg");
	var mask_2_graphics_213 = new cjs.Graphics().p("EANtCaAMBSdhyAMByABSdMhSdByAg");
	var mask_2_graphics_214 = new cjs.Graphics().p("EAVPCiqMBHlh5IMB5IBHlMhHlB5Ig");
	var mask_2_graphics_215 = new cjs.Graphics().p("EAdxCq/MA8Qh/JMB/JA8QMg8QB/Jg");
	var mask_2_graphics_216 = new cjs.Graphics().p("EAnLCy6MAwoiECMCEBAwoMgwnCECg");
	var mask_2_graphics_217 = new cjs.Graphics().p("EAxWC6ZMAkxiH0MCH0AkyMgkyCHzg");
	var mask_2_graphics_218 = new cjs.Graphics().p("EA8JDBYMAY1iKfMCKfAY1MgY1CKfg");
	var mask_2_graphics_238 = new cjs.Graphics().p("EAEtBHLMBd8houMBouBd7Mhd7Boug");
	var mask_2_graphics_239 = new cjs.Graphics().p("EACtBJ7MBcvhpxMBpyBcuMhcvBpyg");
	var mask_2_graphics_240 = new cjs.Graphics().p("EAAwBMpMBbjhqzMBq0BbiMhbjBq0g");
	var mask_2_graphics_241 = new cjs.Graphics().p("EgBJBPVMBaXhrzMBr0BaWMhaXBr0g");
	var mask_2_graphics_242 = new cjs.Graphics().p("EgC/BR+MBZLhsxMBszBZLMhZNBszg");
	var mask_2_graphics_243 = new cjs.Graphics().p("EgEzBUlMBYBhtuMBtvBYCMhYDBtug");
	var mask_2_graphics_244 = new cjs.Graphics().p("EgGkBXJMBW4huoMBupBW4MhW5Bupg");
	var mask_2_graphics_245 = new cjs.Graphics().p("EgISBZqMBVwhvgMBvhBVvMhVwBvig");
	var mask_2_graphics_246 = new cjs.Graphics().p("EgJ9BcJMBUohwXMBwYBUoMhUpBwYg");
	var mask_2_graphics_247 = new cjs.Graphics().p("EgLlBelMBThhxMMBxNBThMhTiBxNg");
	var mask_2_graphics_248 = new cjs.Graphics().p("EgNKBg/MBSahx/MByBBSaMhScByAg");
	var mask_2_graphics_249 = new cjs.Graphics().p("EgOtBjWMBRWhyxMByxBRVMhRWByyg");
	var mask_2_graphics_250 = new cjs.Graphics().p("EgQMBlqMBQQhzhMBziBQRMhQSBzig");
	var mask_2_graphics_251 = new cjs.Graphics().p("EgRqBn8MBPOh0PMB0QBPNMhPPB0Qg");
	var mask_2_graphics_252 = new cjs.Graphics().p("EgTEBqKMBOLh07MB09BOLMhOMB09g");
	var mask_2_graphics_253 = new cjs.Graphics().p("EgUcBsWMBNKh1nMB1oBNKMhNLB1og");
	var mask_2_graphics_254 = new cjs.Graphics().p("EgVyBugMBMKh2RMB2SBMKMhMLB2Rg");
	var mask_2_graphics_255 = new cjs.Graphics().p("EgXFBwmMBLLh25MB26BLKMhLMB27g");
	var mask_2_graphics_256 = new cjs.Graphics().p("EgYVBypMBKMh3gMB3hBKNMhKOB3hg");
	var mask_2_graphics_257 = new cjs.Graphics().p("EgZkB0qMBJQh4FMB4HBJQMhJRB4Gg");
	var mask_2_graphics_258 = new cjs.Graphics().p("EgawB2oMBIVh4qMB4qBIVMhIVB4qg");
	var mask_2_graphics_278 = new cjs.Graphics().p("Egj7BORMCHjglmMAlnCHjMiHkAlng");
	var mask_2_graphics_279 = new cjs.Graphics().p("EgjPBPLMCHjglnMAlnCHjMiHkAlng");
	var mask_2_graphics_280 = new cjs.Graphics().p("EgijBQEMCHjglnMAlnCHkMiHkAlng");
	var mask_2_graphics_281 = new cjs.Graphics().p("Egh2BQ9MCHjglnMAlnCHkMiHkAlng");
	var mask_2_graphics_282 = new cjs.Graphics().p("EghKBR2MCHjglnMAlnCHkMiHkAlng");
	var mask_2_graphics_283 = new cjs.Graphics().p("EggeBSvMCHjglnMAlnCHkMiHkAlng");
	var mask_2_graphics_284 = new cjs.Graphics().p("EgfxBToMCHjglnMAlmCHkMiHjAlng");
	var mask_2_graphics_285 = new cjs.Graphics().p("EgfFBUhMCHjglnMAlnCHkMiHkAlng");
	var mask_2_graphics_286 = new cjs.Graphics().p("EgeZBVaMCHjglmMAlnCHjMiHkAlng");
	var mask_2_graphics_287 = new cjs.Graphics().p("EgdtBWUMCHjglnMAlnCHkMiHkAlng");
	var mask_2_graphics_288 = new cjs.Graphics().p("EgdABXNMCHjglnMAlnCHkMiHkAlng");
	var mask_2_graphics_289 = new cjs.Graphics().p("EgcUBYGMCHjglnMAlnCHkMiHkAlng");
	var mask_2_graphics_290 = new cjs.Graphics().p("EgboBY/MCHjglnMAlnCHkMiHkAlng");
	var mask_2_graphics_291 = new cjs.Graphics().p("Ega7BZ4MCHjglnMAlmCHkMiHjAlng");
	var mask_2_graphics_292 = new cjs.Graphics().p("EgaPBaxMCHjglmMAlnCHjMiHkAlng");
	var mask_2_graphics_320 = new cjs.Graphics().p("EgeiCKFMBvThWCMBWCBvTMhvUBWCg");
	var mask_2_graphics_321 = new cjs.Graphics().p("Egd5CK4MBvThWCMBWCBvUMhvUBWCg");
	var mask_2_graphics_322 = new cjs.Graphics().p("EgdQCLsMBvShWCMBWCBvTMhvTBWCg");
	var mask_2_graphics_323 = new cjs.Graphics().p("EgcnCMfMBvShWCMBWCBvUMhvTBWCg");
	var mask_2_graphics_324 = new cjs.Graphics().p("Egb/CNTMBvThWCMBWCBvTMhvUBWCg");
	var mask_2_graphics_325 = new cjs.Graphics().p("EgbWCOGMBvThWCMBWCBvUMhvUBWCg");
	var mask_2_graphics_326 = new cjs.Graphics().p("EgatCO6MBvShWCMBWCBvTMhvTBWCg");
	var mask_2_graphics_327 = new cjs.Graphics().p("EgaECPtMBvShWCMBWCBvUMhvTBWCg");
	var mask_2_graphics_328 = new cjs.Graphics().p("EgZcCQhMBvThWCMBWCBvTMhvUBWCg");
	var mask_2_graphics_329 = new cjs.Graphics().p("EgYzCRUMBvThWCMBWCBvUMhvUBWCg");
	var mask_2_graphics_330 = new cjs.Graphics().p("EgYKCSIMBvShWCMBWCBvTMhvTBWCg");
	var mask_2_graphics_331 = new cjs.Graphics().p("EgXhCS7MBvShWCMBWCBvUMhvTBWCg");
	var mask_2_graphics_332 = new cjs.Graphics().p("EgW5CTvMBvThWCMBWCBvTMhvUBWCg");
	var mask_2_graphics_333 = new cjs.Graphics().p("EgWQCUiMBvShWCMBWCBvTMhvTBWDg");
	var mask_2_graphics_334 = new cjs.Graphics().p("EgVnCVWMBvShWCMBWCBvTMhvTBWCg");
	var mask_2_graphics_335 = new cjs.Graphics().p("EgU+CWJMBvShWCMBWCBvTMhvTBWCg");
	var mask_2_graphics_336 = new cjs.Graphics().p("EgUWCW9MBvThWCMBWCBvTMhvUBWCg");
	var mask_2_graphics_337 = new cjs.Graphics().p("EgTtCXwMBvShWCMBWCBvTMhvTBWCg");
	var mask_2_graphics_338 = new cjs.Graphics().p("EgTECYkMBvShWDMBWCBvUMhvTBWCg");
	var mask_2_graphics_339 = new cjs.Graphics().p("EgSbCZXMBvShWCMBWCBvTMhvTBWCg");
	var mask_2_graphics_340 = new cjs.Graphics().p("EgRzCaKMBvThWCMBWCBvUMhvUBWCg");
	var mask_2_graphics_341 = new cjs.Graphics().p("EgRKCa+MBvShWCMBWCBvTMhvTBWCg");
	var mask_2_graphics_342 = new cjs.Graphics().p("EgQhCbxMBvShWCMBWCBvUMhvTBWCg");
	var mask_2_graphics_343 = new cjs.Graphics().p("EgP5CclMBvThWCMBWCBvTMhvUBWCg");
	var mask_2_graphics_344 = new cjs.Graphics().p("EgPQCdYMBvThWCMBWCBvUMhvUBWCg");
	var mask_2_graphics_345 = new cjs.Graphics().p("EgOnCeMMBvShWCMBWCBvTMhvTBWCg");
	var mask_2_graphics_346 = new cjs.Graphics().p("EgN+Ce/MBvShWCMBWCBvUMhvTBWCg");
	var mask_2_graphics_347 = new cjs.Graphics().p("EgNWCfzMBvThWCMBWCBvTMhvUBWCg");
	var mask_2_graphics_348 = new cjs.Graphics().p("EgMtCgmMBvThWCMBWCBvUMhvUBWCg");
	var mask_2_graphics_349 = new cjs.Graphics().p("EgMEChaMBvShWCMBWCBvTMhvTBWCg");
	var mask_2_graphics_350 = new cjs.Graphics().p("EgLbCiNMBvShWCMBWCBvUMhvTBWCg");
	var mask_2_graphics_351 = new cjs.Graphics().p("EgKzCjBMBvThWCMBWCBvTMhvUBWCg");
	var mask_2_graphics_352 = new cjs.Graphics().p("EgKKCj0MBvThWCMBWCBvUMhvUBWCg");
	var mask_2_graphics_353 = new cjs.Graphics().p("EgJhCkoMBvShWCMBWCBvTMhvTBWCg");
	var mask_2_graphics_354 = new cjs.Graphics().p("EgI4ClbMBvShWCMBWCBvUMhvTBWCg");
	var mask_2_graphics_355 = new cjs.Graphics().p("EgIQCmPMBvThWCMBWCBvTMhvUBWCg");
	var mask_2_graphics_356 = new cjs.Graphics().p("EgHnCnCMBvShWCMBWDBvUMhvUBWCg");
	var mask_2_graphics_357 = new cjs.Graphics().p("EgG+Cn2MBvShWCMBWCBvTMhvTBWCg");
	var mask_2_graphics_358 = new cjs.Graphics().p("EgGVCopMBvShWCMBWCBvUMhvTBWCg");
	var mask_2_graphics_359 = new cjs.Graphics().p("EgFtCpdMBvThWCMBWCBvTMhvUBWCg");
	var mask_2_graphics_360 = new cjs.Graphics().p("EgFECqQMBvShWCMBWCBvTMhvTBWDg");
	var mask_2_graphics_361 = new cjs.Graphics().p("EgEbCrEMBvShWCMBWCBvTMhvTBWCg");
	var mask_2_graphics_362 = new cjs.Graphics().p("EgDyCr3MBvShWCMBWCBvTMhvTBWCg");
	var mask_2_graphics_363 = new cjs.Graphics().p("EgDKCsrMBvThWCMBWCBvTMhvUBWCg");
	var mask_2_graphics_364 = new cjs.Graphics().p("EgChCteMBvShWCMBWCBvTMhvTBWCg");
	var mask_2_graphics_365 = new cjs.Graphics().p("EgB4CuSMBvShWCMBWCBvTMhvTBWCg");
	var mask_2_graphics_366 = new cjs.Graphics().p("EgBPCvFMBvShWCMBWCBvTMhvTBWCg");
	var mask_2_graphics_367 = new cjs.Graphics().p("EgAnCv4MBvThWCMBWCBvUMhvUBWCg");
	var mask_2_graphics_368 = new cjs.Graphics().p("EAABCwsMBvThWCMBWCBvTMhvTBWCg");
	var mask_2_graphics_369 = new cjs.Graphics().p("EAAqCxfMBvThWCMBWCBvUMhvTBWCg");
	var mask_2_graphics_370 = new cjs.Graphics().p("EABSCyTMBvUhWCMBWCBvTMhvUBWCg");
	var mask_2_graphics_371 = new cjs.Graphics().p("EAB7CzGMBvUhWCMBWCBvUMhvUBWCg");
	var mask_2_graphics_372 = new cjs.Graphics().p("EACkCz6MBvThWCMBWCBvTMhvTBWCg");
	var mask_2_graphics_373 = new cjs.Graphics().p("EADNC0tMBvThWCMBWCBvUMhvTBWCg");
	var mask_2_graphics_374 = new cjs.Graphics().p("EAD1C1hMBvUhWCMBWCBvTMhvUBWCg");
	var mask_2_graphics_384 = new cjs.Graphics().p("EAosCttMCD/gwrMAwrCD/MiD/Awqg");
	var mask_2_graphics_385 = new cjs.Graphics().p("EAszCqpMCGhgpKMApKCGgMiGgApLg");
	var mask_2_graphics_386 = new cjs.Graphics().p("EAxPCoBMCIoghiMAhiCInMiInAhjg");
	var mask_2_graphics_387 = new cjs.Graphics().p("EA2ACl1MCKTgZzMAZzCKSMiKSAZzg");
	var mask_2_graphics_388 = new cjs.Graphics().p("EA7FCkFMCLhgR/MAR/CLhMiLiAR/g");
	var mask_2_graphics_389 = new cjs.Graphics().p("EBAbCixMCMTgKHMAKHCMUMiMTAKHg");
	var mask_2_graphics_390 = new cjs.Graphics().p("EBGBCh5MCMqgCMMACNCMqMiMqACMg");
	var mask_2_graphics_391 = new cjs.Graphics().p("EBDRErLMAFuiMkMCMkAFuMgFuCMkg");
	var mask_2_graphics_392 = new cjs.Graphics().p("EA9eEmtMANniMBMCMAANoMgNnCMAg");
	var mask_2_graphics_393 = new cjs.Graphics().p("EA35EiQMAVeiLBMCLCAVeMgVeCLBg");
	var mask_2_graphics_394 = new cjs.Graphics().p("EAylEd2MAdQiJmMCJmAdQMgdQCJmg");
	var mask_2_graphics_395 = new cjs.Graphics().p("EAtiEZeMAk9iHvMCHuAk9Mgk8CHvg");
	var mask_2_graphics_396 = new cjs.Graphics().p("EAowEVKMAsiiFcMCFbAsiMgshCFcg");
	var mask_2_graphics_412 = new cjs.Graphics().p("EggfCnHMB8LhCBMBCBB8NMh8MBCBg");
	var mask_2_graphics_413 = new cjs.Graphics().p("EggfCoQMB8LhCBMBCBB8NMh8MBCBg");
	var mask_2_graphics_414 = new cjs.Graphics().p("EggfCpcMB8LhCBMBCBB8NMh8MBCBg");
	var mask_2_graphics_415 = new cjs.Graphics().p("EggfCqrMB8LhCBMBCBB8NMh8MBCBg");
	var mask_2_graphics_416 = new cjs.Graphics().p("EggfCr9MB8LhCBMBCBB8NMh8MBCBg");
	var mask_2_graphics_417 = new cjs.Graphics().p("EggfCtSMB8LhCBMBCBB8NMh8MBCBg");
	var mask_2_graphics_418 = new cjs.Graphics().p("EggfCuqMB8LhCAMBCBB8MMh8MBCBg");
	var mask_2_graphics_419 = new cjs.Graphics().p("EggfCwGMB8LhCBMBCBB8NMh8MBCBg");
	var mask_2_graphics_420 = new cjs.Graphics().p("EggfCxkMB8LhCBMBCBB8NMh8MBCBg");
	var mask_2_graphics_421 = new cjs.Graphics().p("EggfCzGMB8LhCBMBCBB8MMh8MBCBg");
	var mask_2_graphics_422 = new cjs.Graphics().p("EggfC0qMB8LhCBMBCBB8NMh8MBCBg");
	var mask_2_graphics_423 = new cjs.Graphics().p("EggfC2SMB8LhCBMBCBB8MMh8MBCBg");
	var mask_2_graphics_424 = new cjs.Graphics().p("EggfC38MB8LhCBMBCBB8NMh8MBCBg");
	var mask_2_graphics_425 = new cjs.Graphics().p("EggfC5qMB8LhCBMBCBB8NMh8MBCBg");
	var mask_2_graphics_426 = new cjs.Graphics().p("EggfC7aMB8LhCAMBCBB8MMh8MBCBg");
	var mask_2_graphics_427 = new cjs.Graphics().p("EggfC9OMB8LhCBMBCBB8NMh8MBCBg");
	var mask_2_graphics_428 = new cjs.Graphics().p("EggfC/FMB8LhCBMBCBB8NMh8MBCBg");
	var mask_2_graphics_429 = new cjs.Graphics().p("EggfDA/MB8LhCBMBCBB8NMh8MBCBg");
	var mask_2_graphics_430 = new cjs.Graphics().p("EggfDC8MB8LhCBMBCBB8NMh8MBCBg");
	var mask_2_graphics_431 = new cjs.Graphics().p("EggfDE8MB8LhCBMBCBB8NMh8MBCBg");
	var mask_2_graphics_432 = new cjs.Graphics().p("EggfDG/MB8LhCBMBCBB8NMh8MBCBg");
	var mask_2_graphics_440 = new cjs.Graphics().p("EAXiFBZMADkiMnMCMnADkMgDkCMng");
	var mask_2_graphics_441 = new cjs.Graphics().p("EAXiFDTMADkiMnMCMnADkMgDkCMng");
	var mask_2_graphics_442 = new cjs.Graphics().p("EAXiFFLMADkiMnMCMnADkMgDkCMng");
	var mask_2_graphics_443 = new cjs.Graphics().p("EAXiFHBMADkiMnMCMnADjMgDkCMng");
	var mask_2_graphics_444 = new cjs.Graphics().p("EAXiFI0MADkiMnMCMnADkMgDkCMng");
	var mask_2_graphics_445 = new cjs.Graphics().p("EAXiFKlMADkiMnMCMnADkMgDkCMng");
	var mask_2_graphics_446 = new cjs.Graphics().p("EAXiFMUMADkiMnMCMnADkMgDkCMng");
	var mask_2_graphics_447 = new cjs.Graphics().p("EAXiFOBMADkiMnMCMnADkMgDkCMng");
	var mask_2_graphics_448 = new cjs.Graphics().p("EAXiFPsMADkiMoMCMnADkMgDkCMng");
	var mask_2_graphics_449 = new cjs.Graphics().p("EAXiFRUMADkiMnMCMnADkMgDkCMng");
	var mask_2_graphics_450 = new cjs.Graphics().p("EAXiFS6MADkiMnMCMnADkMgDkCMng");
	var mask_2_graphics_451 = new cjs.Graphics().p("EAXiFUeMADkiMnMCMnADkMgDkCMng");
	var mask_2_graphics_452 = new cjs.Graphics().p("EAXiFWAMADkiMnMCMnADkMgDkCMng");
	var mask_2_graphics_453 = new cjs.Graphics().p("EAXiFXgMADkiMnMCMnADkMgDkCMng");
	var mask_2_graphics_454 = new cjs.Graphics().p("EAXiFY9MADkiMnMCMnADkMgDkCMng");
	var mask_2_graphics_455 = new cjs.Graphics().p("EAXiFaZMADkiMnMCMnADjMgDkCMog");
	var mask_2_graphics_456 = new cjs.Graphics().p("EAXiFbyMADkiMnMCMnADkMgDkCMng");
	var mask_2_graphics_457 = new cjs.Graphics().p("EAXiFdJMADkiMnMCMnADkMgDkCMng");
	var mask_2_graphics_458 = new cjs.Graphics().p("EAXiFeeMADkiMnMCMnADjMgDkCMng");
	var mask_2_graphics_459 = new cjs.Graphics().p("EAXiFfwMADkiMnMCMnADkMgDkCMng");
	var mask_2_graphics_460 = new cjs.Graphics().p("EAXiFhBMADkiMnMCMnADjMgDkCMog");
	var mask_2_graphics_461 = new cjs.Graphics().p("EAXiFiPMADkiMnMCMnADkMgDkCMng");
	var mask_2_graphics_462 = new cjs.Graphics().p("EAXiFjbMADkiMnMCMnADkMgDkCMng");
	var mask_2_graphics_463 = new cjs.Graphics().p("EAXiFklMADkiMnMCMnADkMgDkCMng");
	var mask_2_graphics_464 = new cjs.Graphics().p("EAXiFltMADkiMnMCMnADjMgDkCMng");
	var mask_2_graphics_465 = new cjs.Graphics().p("EAXiFmyMADkiMnMCMnADkMgDkCMng");
	var mask_2_graphics_466 = new cjs.Graphics().p("EAXiFn1MADkiMmMCMnADjMgDkCMng");
	var mask_2_graphics_520 = new cjs.Graphics().p("EAhKFw9MAw8iD4MCD3Aw8Mgw7CD3g");
	var mask_2_graphics_521 = new cjs.Graphics().p("EAgMFxMMAyLiDZMCDaAyLMgyLCDag");
	var mask_2_graphics_522 = new cjs.Graphics().p("EAfPFxcMAzaiC7MCC7AzaMgzaCC7g");
	var mask_2_graphics_523 = new cjs.Graphics().p("EAeTFxsMA0oiCcMCCcA0oMg0oCCcg");
	var mask_2_graphics_524 = new cjs.Graphics().p("EAdZFx8MA10iB9MCB9A11Mg11CB9g");
	var mask_2_graphics_525 = new cjs.Graphics().p("EAcfFyMMA3AiBdMCBdA3AMg3BCBdg");
	var mask_2_graphics_526 = new cjs.Graphics().p("EAbmFycMA4LiA9MCA9A4LMg4LCA9g");
	var mask_2_graphics_527 = new cjs.Graphics().p("EAauFyrMA5ViAcMCAcA5VMg5VCAcg");
	var mask_2_graphics_528 = new cjs.Graphics().p("EAZ4Fy7MA6dh/7MB/7A6eMg6dB/7g");
	var mask_2_graphics_529 = new cjs.Graphics().p("EAZCFzLMA7lh/aMB/aA7lMg7lB/ag");
	var mask_2_graphics_530 = new cjs.Graphics().p("EAYNFzbMA8sh+5MB+5A8sMg8sB+5g");
	var mask_2_graphics_531 = new cjs.Graphics().p("EAXaFzqMA9xh+WMB+XA9yMg9yB+Wg");
	var mask_2_graphics_532 = new cjs.Graphics().p("EAWnFz6MA+2h90MB91A+2Mg+3B91g");
	var mask_2_graphics_533 = new cjs.Graphics().p("EAV1F0KMA/6h9SMB9TA/6Mg/7B9Sg");
	var mask_2_graphics_534 = new cjs.Graphics().p("EAVEF0aMBA9h8wMB8wBA9MhA9B8wg");
	var mask_2_graphics_535 = new cjs.Graphics().p("EAUUF0pMBB/h8NMB8NBB/MhB/B8Ng");
	var mask_2_graphics_536 = new cjs.Graphics().p("EATlF05MBDAh7qMB7qBC/MhDAB7rg");
	var mask_2_graphics_537 = new cjs.Graphics().p("EAS3F1JMBD/h7IMB7IBEAMhEAB7Hg");
	var mask_2_graphics_538 = new cjs.Graphics().p("EASJF1YMBE/h6kMB6kBE+MhE+B6lg");
	var mask_2_graphics_539 = new cjs.Graphics().p("EARdF1oMBF8h6BMB6CBF8MhF9B6Bg");
	var mask_2_graphics_540 = new cjs.Graphics().p("EAQxF14MBG6h5eMB5eBG5MhG6B5eg");
	var mask_2_graphics_541 = new cjs.Graphics().p("EAQAF1vMBInh4dMB4dBIoMhIoB4dg");
	var mask_2_graphics_542 = new cjs.Graphics().p("EAPOF1nMBKXh3ZMB3YBKXMhKWB3Yg");
	var mask_2_graphics_543 = new cjs.Graphics().p("EAOdF1fMBMGh2SMB2SBMGMhMGB2Sg");
	var mask_2_graphics_544 = new cjs.Graphics().p("EANtF1XMBN3h1IMB1IBN3MhN3B1Ig");
	var mask_2_graphics_545 = new cjs.Graphics().p("EAM9F1QMBPohz7MBz8BPoMhPoBz8g");
	var mask_2_graphics_546 = new cjs.Graphics().p("EAMOF1KMBRahysMBysBRaMhRaBysg");
	var mask_2_graphics_547 = new cjs.Graphics().p("EALgF1EMBTNhxZMBxZBTMMhTNBxag");
	var mask_2_graphics_548 = new cjs.Graphics().p("EAKyF0+MBVAhwDMBwEBVAMhVABwDg");
	var mask_2_graphics_549 = new cjs.Graphics().p("EAKGF06MBWzhurMBurBWzMhW0Burg");
	var mask_2_graphics_550 = new cjs.Graphics().p("EAJaF01MBYnhtOMBtPBYnMhYnBtOg");
	var mask_2_graphics_551 = new cjs.Graphics().p("EAIwF0yMBabhrvMBruBabMhaaBrvg");
	var mask_2_graphics_552 = new cjs.Graphics().p("EAIHF0vMBcPhqLMBqLBcPMhcPBqLg");
	var mask_2_graphics_553 = new cjs.Graphics().p("EAHfF0uMBeDhomMBolBeDMheDBolg");
	var mask_2_graphics_554 = new cjs.Graphics().p("EAG4F0tMBf3hm8MBm7Bf3Mhf3Bm7g");
	var mask_2_graphics_555 = new cjs.Graphics().p("EAGTF0tMBhqhlOMBlOBhqMhhqBlOg");
	var mask_2_graphics_556 = new cjs.Graphics().p("EAFvF0uMBjehjdMBjcBjdMhjdBjdg");
	var mask_2_graphics_557 = new cjs.Graphics().p("EAFNF0wMBlQhhoMBhoBlQMhlQBhog");
	var mask_2_graphics_558 = new cjs.Graphics().p("EAEtF0zMBnChfvMBfvBnCMhnCBfvg");
	var mask_2_graphics_559 = new cjs.Graphics().p("EAEOF03MBo0hdzMBdzBo0Mho0Bdzg");
	var mask_2_graphics_560 = new cjs.Graphics().p("EADxF08MBqkhbzMBbzBqkMhqkBbzg");
	var mask_2_graphics_576 = new cjs.Graphics().p("EgHsEzcMCKKgaOMAaOCKMMiKLAaNg");
	var mask_2_graphics_577 = new cjs.Graphics().p("EgLYE2LMCI7ggGMAgGCI8MiI8AgGg");
	var mask_2_graphics_578 = new cjs.Graphics().p("EgO0E5GMCHdglzMAl0CHdMiHeAl0g");
	var mask_2_graphics_579 = new cjs.Graphics().p("EgSBE8OMCFxgrYMArYCFyMiFyArYg");
	var mask_2_graphics_580 = new cjs.Graphics().p("EgVAE/hMCD6gwyMAwyCD6MiD6Awyg");
	var mask_2_graphics_581 = new cjs.Graphics().p("EgXvFC+MCB1g2CMA2CCB2MiB2A2Cg");
	var mask_2_graphics_582 = new cjs.Graphics().p("EgaQFGlMB/ng7HMA7HB/nMh/oA7Hg");
	var mask_2_graphics_583 = new cjs.Graphics().p("EgciFKUMB9OhABMBABB9PMh9PBABg");
	var mask_2_graphics_584 = new cjs.Graphics().p("EgelFOLMB6shEvMBEuB6tMh6tBEug");
	var mask_2_graphics_585 = new cjs.Graphics().p("EggbFSIMB4ChJRMBJRB4CMh4CBJSg");
	var mask_2_graphics_586 = new cjs.Graphics().p("EgiDFWMMB1QhNpMBNpB1RMh1RBNpg");
	var mask_2_graphics_587 = new cjs.Graphics().p("EgjdFaVMByXhR1MBR1ByZMhyYBR1g");
	var mask_2_graphics_588 = new cjs.Graphics().p("EgkrFejMBvYhV2MBV2BvaMhvaBV2g");
	var mask_2_graphics_589 = new cjs.Graphics().p("EgjWFe9MBwLhU0MBU0BwMMhwMBU0g");
	var mask_2_graphics_590 = new cjs.Graphics().p("EgiAFfXMBw8hTyMBTyBw9Mhw9BTyg");
	var mask_2_graphics_591 = new cjs.Graphics().p("EggrFfyMBxshSxMBSxBxtMhxtBSxg");
	var mask_2_graphics_592 = new cjs.Graphics().p("EgfXFgNMBybhRwMBRwBybMhybBRxg");
	var mask_2_graphics_593 = new cjs.Graphics().p("EgeCFgoMBzIhQwMBQwBzKMhzJBQvg");
	var mask_2_graphics_594 = new cjs.Graphics().p("EgcuFhDMBz1hPvMBPwBz2Mhz2BPvg");
	var mask_2_graphics_595 = new cjs.Graphics().p("EgbaFhfMB0ghOwMBOwB0hMh0hBOwg");
	var mask_2_graphics_596 = new cjs.Graphics().p("EgaHFh7MB1LhNxMBNxB1MMh1MBNxg");
	var mask_2_graphics_597 = new cjs.Graphics().p("EgY0FiXMB10hMyMBMyB11Mh11BMyg");
	var mask_2_graphics_598 = new cjs.Graphics().p("EgXiFizMB2dhL0MBLzB2eMh2dBLzg");
	var mask_2_graphics_599 = new cjs.Graphics().p("EgWPFjPMB3DhK1MBK2B3EMh3FBK2g");
	var mask_2_graphics_600 = new cjs.Graphics().p("EgU+FjsMB3qhJ4MBJ4B3qMh3rBJ5g");
	var mask_2_graphics_601 = new cjs.Graphics().p("EgTtFkIMB4PhI7MBI8B4QMh4RBI7g");
	var mask_2_graphics_602 = new cjs.Graphics().p("EgSdFklMB40hH/MBH/B41Mh41BH+g");
	var mask_2_graphics_603 = new cjs.Graphics().p("EgRNFlCMB5XhHDMBHDB5YMh5YBHDg");
	var mask_2_graphics_604 = new cjs.Graphics().p("EgP+FleMB56hGHMBGHB56Mh56BGIg");
	var mask_2_graphics_605 = new cjs.Graphics().p("EAGBG++MAfCiJLMCJLAfBMgfCCJLg");
	var mask_2_graphics_636 = new cjs.Graphics().p("EANuHtNMAcoiJtMCJtAcoMgcoCJtg");
	var mask_2_graphics_637 = new cjs.Graphics().p("EALiHr6MAcniJtMCJuAcnMgcoCJtg");
	var mask_2_graphics_638 = new cjs.Graphics().p("EAJMHqhMAcoiJtMCJtAcnMgcoCJtg");
	var mask_2_graphics_639 = new cjs.Graphics().p("EAGsHpCMAcoiJtMCJtAcnMgcoCJtg");
	var mask_2_graphics_640 = new cjs.Graphics().p("EAEDHndMAcoiJtMCJtAcoMgcoCJtg");
	var mask_2_graphics_641 = new cjs.Graphics().p("EABQHlzMAcoiJtMCJtAcnMgcnCJtg");
	var mask_2_graphics_642 = new cjs.Graphics().p("EgBrHkDMAcniJtMCJtAcnMgcoCJtg");
	var mask_2_graphics_643 = new cjs.Graphics().p("EgExHiNMAcmiJtMCJuAcnMgcoCJtg");
	var mask_2_graphics_644 = new cjs.Graphics().p("EgIBHgRMAcniJtMCJtAcoMgcoCJtg");
	var mask_2_graphics_645 = new cjs.Graphics().p("EgLbHeQMAcniJtMCJtAcnMgcoCJtg");
	var mask_2_graphics_646 = new cjs.Graphics().p("EgO+HcJMAcniJtMCJtAcnMgcoCJtg");
	var mask_2_graphics_647 = new cjs.Graphics().p("EgSrHZ8MAcniJtMCJtAcnMgcoCJtg");
	var mask_2_graphics_648 = new cjs.Graphics().p("EgWhHXpMAcniJtMCJtAcoMgcoCJtg");
	var mask_2_graphics_660 = new cjs.Graphics().p("EhBHG6HMAuWiEyMCEyAuWMguXCEzg");
	var mask_2_graphics_661 = new cjs.Graphics().p("Eg6mHBhMAdkiJgMCJfAdlMgdkCJgg");
	var mask_2_graphics_662 = new cjs.Graphics().p("EgyOHJsMALJiMNMCMMALJMgLJCMNg");
	var mask_2_graphics_663 = new cjs.Graphics().p("Eg04FBxMCMWgIsMAItCMXMiMXAItg");
	var mask_2_graphics_664 = new cjs.Graphics().p("EhHtFDcMCJfgdhMAdiCJgMiJgAdig");
	var mask_2_graphics_665 = new cjs.Graphics().p("EhZNFIjMCDIgy0MAy0CDJMiDIAy0g");
	var mask_2_graphics_666 = new cjs.Graphics().p("EhgXFReMB43hH4MBH4B45Mh43BH4g");
	var mask_2_graphics_667 = new cjs.Graphics().p("EhjJFeGMBqshboMBbnBqtMhqsBbng");
	var mask_2_graphics_668 = new cjs.Graphics().p("Ehi+Ft5MBZWhsoMBsmBZVMhZVBsog");
	var mask_2_graphics_669 = new cjs.Graphics().p("Ehf1GAXMBFTh6ZMB6YBFTMhFTB6Zg");
	var mask_2_graphics_670 = new cjs.Graphics().p("EhZ0GU6MAvLiEgMCEeAvMMgvLCEfg");
	var mask_2_graphics_671 = new cjs.Graphics().p("EhRJGq3MAXriKpMCKoAXqMgXrCKpg");
	var mask_2_graphics_672 = new cjs.Graphics().p("EhGmE0mMCMogAlMAAlCMpMiMoAAlg");
	var mask_2_graphics_684 = new cjs.Graphics().p("Ehh2GuQMBQOhzhMBzfBQPMhQOBzgg");
	var mask_2_graphics_685 = new cjs.Graphics().p("Ehh2Gu4MBQOhzhMBzfBQPMhQOBzgg");
	var mask_2_graphics_686 = new cjs.Graphics().p("Ehh2GvgMBQOhzhMBzfBQPMhQOBzgg");
	var mask_2_graphics_687 = new cjs.Graphics().p("Ehh2GwIMBQOhzhMBzfBQPMhQOBzgg");
	var mask_2_graphics_688 = new cjs.Graphics().p("Ehh2GwwMBQOhzhMBzfBQPMhQOBzgg");
	var mask_2_graphics_689 = new cjs.Graphics().p("Ehh2GxYMBQOhzhMBzfBQPMhQOBzgg");
	var mask_2_graphics_690 = new cjs.Graphics().p("Ehh2GyAMBQOhzhMBzfBQPMhQOBzgg");
	var mask_2_graphics_691 = new cjs.Graphics().p("Ehh2GyoMBQOhzhMBzfBQPMhQOBzgg");
	var mask_2_graphics_692 = new cjs.Graphics().p("Ehh2GzQMBQOhzhMBzfBQPMhQOBzgg");
	var mask_2_graphics_693 = new cjs.Graphics().p("Ehh2Gz4MBQOhzhMBzfBQPMhQOBzgg");
	var mask_2_graphics_694 = new cjs.Graphics().p("Ehh2G0gMBQOhzhMBzfBQPMhQOBzgg");
	var mask_2_graphics_708 = new cjs.Graphics().p("EhXcGUFMB6EhF1MBF0B6FMh6DBF0g");
	var mask_2_graphics_709 = new cjs.Graphics().p("EhWgGZSMB47hHxMBHxB49Mh47BHwg");
	var mask_2_graphics_710 = new cjs.Graphics().p("EhVhGehMB3xhJrMBJsB3yMh3xBJrg");
	var mask_2_graphics_711 = new cjs.Graphics().p("EhUfGjyMB2lhLlMBLlB2mMh2lBLlg");
	var mask_2_graphics_712 = new cjs.Graphics().p("EhTbGpEMB1XhNdMBNdB1XMh1WBNeg");
	var mask_2_graphics_713 = new cjs.Graphics().p("EhSUGuYMB0HhPUMBPUB0HMh0GBPVg");
	var mask_2_graphics_714 = new cjs.Graphics().p("EhRKGztMBy0hRKMBRLBy1Mhy1BRLg");
	var mask_2_graphics_715 = new cjs.Graphics().p("EhPSG3dMBy0hRKMBRLBy1Mhy2BRLg");
	var mask_2_graphics_716 = new cjs.Graphics().p("EhSwHixMBQChzoMBzmBQDMhQCBzng");
	var mask_2_graphics_717 = new cjs.Graphics().p("EhPoHl5MBQBhzoMBznBQDMhQCBzng");
	var mask_2_graphics_718 = new cjs.Graphics().p("Eg/rIYBMAjiiIDMCIDAjiMgjiCIDg");
	var mask_2_graphics_719 = new cjs.Graphics().p("Eg7TIYBMAjiiIDMCIDAjiMgjiCIDg");
	var mask_2_graphics_728 = new cjs.Graphics().p("EhChHGyMB9yg+1MA+2B9zMh9zA+1g");
	var mask_2_graphics_729 = new cjs.Graphics().p("EgxZGu6MCIGgjUMAjUCIHMiIHAjUg");
	var mask_2_graphics_730 = new cjs.Graphics().p("EgZdGckMCMcgG7MAG7CMdMiMcAG7g");
	var mask_2_graphics_731 = new cjs.Graphics().p("EgbyIQqMAVFiLCMCLBAVFMgVFCLCg");
	var mask_2_graphics_732 = new cjs.Graphics().p("EgieH2UMAvmiEUMCEUAvnMgvnCEUg");
	var mask_2_graphics_733 = new cjs.Graphics().p("EgjUHeWMBHvh48MB48BHwMhHwB48g");
	var mask_2_graphics_734 = new cjs.Graphics().p("EgevHJcMBczhppMBpqBczMhc0Bpqg");
	var mask_2_graphics_735 = new cjs.Graphics().p("EgVRG4IMBuQhXSMBXSBuRMhuRBXSg");
	var mask_2_graphics_736 = new cjs.Graphics().p("EgHnGqsMB7zhCqMBCqB70Mh70BCrg");
	var mask_2_graphics_737 = new cjs.Graphics().p("EgAsGntMCA1g4WMA4VCA2MiA2A4Wg");
	var mask_2_graphics_738 = new cjs.Graphics().p("EAGYGltMCEtguhMAuhCEuMiEtAuhg");
	var mask_2_graphics_739 = new cjs.Graphics().p("EANZGkjMCHlglUMAlUCHlMiHlAlUg");
	var mask_2_graphics_740 = new cjs.Graphics().p("EAULGkCMCJogc1MAc2CJoMiJpAc1g");
	var mask_2_graphics_741 = new cjs.Graphics().p("EAajGkBMCLCgVKMAVJCLCMiLBAVKg");
	var mask_2_graphics_742 = new cjs.Graphics().p("EAgaGkXMCL5gOUMAOUCL5MiL4AOUg");
	var mask_2_graphics_760 = new cjs.Graphics().p("EgJXIKpMBZThsoMBsoBZUMhZUBsog");
	var mask_2_graphics_761 = new cjs.Graphics().p("EgKFIM2MBXkhuBMBuBBXlMhXlBuCg");
	var mask_2_graphics_762 = new cjs.Graphics().p("EgKwIPCMBV2hvXMBvXBV2MhV3BvYg");
	var mask_2_graphics_763 = new cjs.Graphics().p("EgLZIRMMBUJhwqMBwrBUJMhUKBwrg");
	var mask_2_graphics_764 = new cjs.Graphics().p("EgL+ITUMBSchx6MBx6BSeMhSdBx6g");
	var mask_2_graphics_765 = new cjs.Graphics().p("EgMiIVaMBQyhzGMBzGBQyMhQyBzHg");
	var mask_2_graphics_766 = new cjs.Graphics().p("EgNCIXfMBPHh0QMB0QBPIMhPJB0Qg");
	var mask_2_graphics_767 = new cjs.Graphics().p("EgNhIZhMBNeh1WMB1WBNfMhNfB1Wg");
	var mask_2_graphics_768 = new cjs.Graphics().p("EgN9IbhMBL3h2aMB2ZBL4MhL4B2ag");
	var mask_2_graphics_769 = new cjs.Graphics().p("EgOYIdfMBKRh3aMB3aBKSMhKSB3ag");
	var mask_2_graphics_770 = new cjs.Graphics().p("EgOwIfaMBIth4XMB4XBItMhItB4Yg");
	var mask_2_graphics_771 = new cjs.Graphics().p("EgPGIhUMBHJh5TMB5TBHLMhHLB5Sg");
	var mask_2_graphics_772 = new cjs.Graphics().p("EgPbIjLMBFoh6LMB6LBFpMhFpB6Lg");
	var mask_2_graphics_773 = new cjs.Graphics().p("EgPtIk/MBEIh7AMB7ABEJMhEJB7Ag");
	var mask_2_graphics_774 = new cjs.Graphics().p("EgP/ImxMBCrh7zMB70BCrMhCsB70g");
	var mask_2_graphics_775 = new cjs.Graphics().p("EgQOIohMBBOh8lMB8lBBQMhBQB8kg");
	var mask_2_graphics_776 = new cjs.Graphics().p("EgQcIqOMA/0h9TMB9TA/1Mg/1B9Tg");
	var mask_2_graphics_777 = new cjs.Graphics().p("EgQpIr4MA+ch9/MB9/A+dMg+dB9/g");
	var mask_2_graphics_778 = new cjs.Graphics().p("EgQ1ItgMA9Gh+pMB+qA9HMg9HB+pg");
	var mask_2_graphics_779 = new cjs.Graphics().p("EgQ/IvGMA7yh/SMB/RA7zMg7yB/Rg");
	var mask_2_graphics_780 = new cjs.Graphics().p("EgRIIwoMA6fh/4MB/4A6hMg6gB/4g");
	var mask_2_graphics_781 = new cjs.Graphics().p("EgRQIyIMA5PiAcMCAcA5RMg5QCAbg");
	var mask_2_graphics_782 = new cjs.Graphics().p("EgRXIzlMA4BiA+MCA+A4DMg4CCA+g");
	var mask_2_graphics_783 = new cjs.Graphics().p("EgReI0/MA22iBeMCBfA23Mg23CBeg");
	var mask_2_graphics_784 = new cjs.Graphics().p("EgRjI2XMA1tiB9MCB9A1tMg1uCB+g");
	var mask_2_graphics_785 = new cjs.Graphics().p("EgRoI3sMA0miCbMCCaA0nMg0mCCag");
	var mask_2_graphics_786 = new cjs.Graphics().p("EgRsI49MAzhiC1MCC2AziMgziCC1g");
	var mask_2_graphics_787 = new cjs.Graphics().p("EgRvI6MMAyfiDPMCDPAyfMgyfCDQg");
	var mask_2_graphics_788 = new cjs.Graphics().p("EgRxI7ZMAxeiDoMCDoAxfMgxgCDog");
	var mask_2_graphics_789 = new cjs.Graphics().p("EgR0I8iMAwhiD/MCD/AwiMgwiCD+g");
	var mask_2_graphics_790 = new cjs.Graphics().p("EgR2I9oMAvmiEVMCEUAvmMgvmCEVg");
	var mask_2_graphics_876 = new cjs.Graphics().p("EgRdJThMBgchmUMBmUBgdMhgeBmUg");
	var mask_2_graphics_877 = new cjs.Graphics().p("EgRzJRjMBi0hkBMBkCBi1Mhi2BkBg");
	var mask_2_graphics_878 = new cjs.Graphics().p("EgSFJPJMBlthhFMBhFBluMhluBhFg");
	var mask_2_graphics_879 = new cjs.Graphics().p("EgSOJMXMBpDhddMBdcBpEMhpEBddg");
	var mask_2_graphics_880 = new cjs.Graphics().p("EgSIJJPMBsyhZFMBZFBs0Mhs0BZEg");
	var mask_2_graphics_881 = new cjs.Graphics().p("EgRrJF2MBw2hT5MBT4Bw3Mhw2BT4g");
	var mask_2_graphics_882 = new cjs.Graphics().p("EgQtJCRMB1FhN1MBN2B1HMh1HBN1g");
	var mask_2_graphics_883 = new cjs.Graphics().p("EgPHI+nMB5chG3MBG3B5dMh5dBG3g");
	var mask_2_graphics_884 = new cjs.Graphics().p("EgMtI7BMB9vg+7MA+6B9wMh9wA+7g");
	var mask_2_graphics_885 = new cjs.Graphics().p("EgJWI3mMCB1g18MA19CB2MiB3A19g");
	var mask_2_graphics_886 = new cjs.Graphics().p("EgE4I0jMCFkgr8MAr8CFkMiFlAr8g");
	var mask_2_graphics_887 = new cjs.Graphics().p("EAA3IyDMCIugg4MAg4CItMiItAg5g");
	var mask_2_graphics_888 = new cjs.Graphics().p("EAIBIwVMCLEgUzMAUzCLEMiLEAUzg");
	var mask_2_graphics_900 = new cjs.Graphics().p("Eg5tJQ6MB2KhMLMBMLB2LMh2LBMKg");
	var mask_2_graphics_901 = new cjs.Graphics().p("EhIzJmRMBgyhl9MBl9Bg0Mhg0Bl8g");
	var mask_2_graphics_902 = new cjs.Graphics().p("EhPQJ/rMBHZh5HMB5GBHZMhHZB5Hg");
	var mask_2_graphics_903 = new cjs.Graphics().p("EhN9KbKMAr6iFkMCFiAr7Mgr6CFjg");
	var mask_2_graphics_904 = new cjs.Graphics().p("EhGJK3AMAQDiLqMCLqAQDMgQECLrg");
	var mask_2_graphics_905 = new cjs.Graphics().p("EhL8JBmMCL2gOMMAOLCL4MiL2AOLg");
	var mask_2_graphics_906 = new cjs.Graphics().p("EhXrJTqMCGggo3MAo3CGhMiGgAo3g");
	var mask_2_graphics_907 = new cjs.Graphics().p("EheaJnWMB9gg/VMA/VB9hMh9gA/Vg");
	var mask_2_graphics_908 = new cjs.Graphics().p("EhiAJ64MByjhReMBReBykMhyjBRfg");
	var mask_2_graphics_909 = new cjs.Graphics().p("EhjVKM+MBnGhfkMBflBnHMhnGBfkg");
	var mask_2_graphics_910 = new cjs.Graphics().p("EhjKKczMBcPhqHMBqGBcPMhcPBqHg");
	var mask_2_graphics_919 = new cjs.Graphics().p("EhJtJZeMCE/gtlMAtlCFAMiE/Atlg");
	var mask_2_graphics_920 = new cjs.Graphics().p("EhHNJZeMCE/gtlMAtlCFAMiE/Atlg");
	var mask_2_graphics_921 = new cjs.Graphics().p("EhFBJZeMCE/gtlMAtlCFAMiE/Atlg");
	var mask_2_graphics_922 = new cjs.Graphics().p("EgpMKx/MAXRiKqMCKpAXRMgXRCKqg");
	var mask_2_graphics_923 = new cjs.Graphics().p("Eg0OJ+yMBRhhyiMByiBRiMhRiByig");
	var mask_2_graphics_924 = new cjs.Graphics().p("EAELI6mMCMjgDmMADmCMkMiMjADmg");
	var mask_2_graphics_925 = new cjs.Graphics().p("EAGnLIZMAAriMmMCMnAArMgAsCMng");
	var mask_2_graphics_926 = new cjs.Graphics().p("EADiLHyMAFTiMgMCMgAFTMgFTCMgg");
	var mask_2_graphics_927 = new cjs.Graphics().p("EAAXLHJMAKPiMOMCMPAKPMgKQCMPg");
	var mask_2_graphics_928 = new cjs.Graphics().p("EgC4LGeMAPhiLwMCLvAPiMgPhCLwg");
	var mask_2_graphics_929 = new cjs.Graphics().p("EgGKLFxMAVHiLBMCLBAVIMgVICLBg");
	var mask_2_graphics_930 = new cjs.Graphics().p("EgJbLFDMAbBiJ/MCJ+AbCMgbBCJ/g");
	var mask_2_graphics_934 = new cjs.Graphics().p("Eg7EKEHMB0phOeMBOeB0rMh0qBOeg");
	var mask_2_graphics_935 = new cjs.Graphics().p("EhBzK5eMBLQh2wMB2wBLRMhLRB2xg");
	var mask_2_graphics_936 = new cjs.Graphics().p("Eg/nK7CMBLQh2wMB2wBLRMhLRB2xg");
	var mask_2_graphics_937 = new cjs.Graphics().p("Eg8zK8SMBLQh2wMB2wBLRMhLRB2xg");
	var mask_2_graphics_946 = new cjs.Graphics().p("EgsxK1fMBY+hs2MBs2BY/MhY/Bs2g");
	var mask_2_graphics_947 = new cjs.Graphics().p("EgrpKzyMBblhqqMBqqBbmMhblBqqg");
	var mask_2_graphics_948 = new cjs.Graphics().p("EgqdKyMMBeBhohMBohBeDMheDBohg");
	var mask_2_graphics_949 = new cjs.Graphics().p("EgpPKwtMBgVhmaMBmZBgXMhgWBmZg");
	var mask_2_graphics_950 = new cjs.Graphics().p("Egn+KvTMBifhkUMBkUBihMhihBkUg");
	var mask_2_graphics_951 = new cjs.Graphics().p("EgmsKt/MBkihiRMBiRBkjMhkjBiRg");
	var mask_2_graphics_952 = new cjs.Graphics().p("EglaKswMBmdhgRMBgRBmdMhmdBgSg");
	var mask_2_graphics_953 = new cjs.Graphics().p("EgkGKrmMBoPheUMBeVBoQMhoQBeVg");
	var mask_2_graphics_954 = new cjs.Graphics().p("EgizKqiMBp8hccMBcbBp8Mhp8Bccg");
	var mask_2_graphics_955 = new cjs.Graphics().p("EghfKpiMBrfhamMBanBrhMhrhBamg");
	var mask_2_graphics_956 = new cjs.Graphics().p("EggNKomMBs+hY0MBY0Bs/Mhs/BY0g");
	var mask_2_graphics_957 = new cjs.Graphics().p("Ege8KnvMBuXhXHMBXGBuYMhuXBXGg");
	var mask_2_graphics_958 = new cjs.Graphics().p("EgdsKm7MBvphVcMBVcBvqMhvpBVcg");
	var mask_2_graphics_959 = new cjs.Graphics().p("EgceKmMMBw2hT3MBT2Bw3Mhw2BT2g");
	var mask_2_graphics_960 = new cjs.Graphics().p("EgbRKlfMBx9hSUMBSUBx+Mhx+BSUg");
	var mask_2_graphics_961 = new cjs.Graphics().p("EgaHKk2MBzAhQ3MBQ3BzBMhzBBQ3g");
	var mask_2_graphics_962 = new cjs.Graphics().p("EgY/KkQMBz+hPdMBPdBz/Mhz/BPdg");
	var mask_2_graphics_963 = new cjs.Graphics().p("EgX6KjtMB04hOIMBOIB05Mh05BOIg");
	var mask_2_graphics_964 = new cjs.Graphics().p("EgW3KjNMB1uhM3MBM2B1vMh1uBM3g");
	var mask_2_graphics_965 = new cjs.Graphics().p("EgV3KivMB2ghLpMBLpB2gMh2gBLpg");
	var mask_2_graphics_966 = new cjs.Graphics().p("EgU5KiUMB3NhKhMBKhB3PMh3PBKgg");
	var mask_2_graphics_967 = new cjs.Graphics().p("EgT/Kh6MB35hJbMBJbB35Mh35BJcg");
	var mask_2_graphics_968 = new cjs.Graphics().p("EgTHKhjMB4ghIaMBIaB4hMh4gBIag");
	var mask_2_graphics_969 = new cjs.Graphics().p("EgSSKhOMB5EhHdMBHdB5FMh5FBHdg");
	var mask_2_graphics_970 = new cjs.Graphics().p("EgRgKg6MB5mhGjMBGjB5mMh5mBGkg");
	var mask_2_graphics_971 = new cjs.Graphics().p("EgQxKgpMB6EhFvMBFvB6GMh6GBFug");
	var mask_2_graphics_972 = new cjs.Graphics().p("EgQFKgYMB6hhE8MBE8B6iMh6iBE8g");
	var mask_2_graphics_973 = new cjs.Graphics().p("EgPcKgJMB67hENMBEOB68Mh68BENg");
	var mask_2_graphics_974 = new cjs.Graphics().p("EgO2Kf8MB7ThDjMBDjB7UMh7UBDjg");
	var mask_2_graphics_975 = new cjs.Graphics().p("EgOSKfwMB7ohC7MBC7B7pMh7pBC7g");
	var mask_2_graphics_976 = new cjs.Graphics().p("EgNxKflMB77hCXMBCXB79Mh79BCXg");
	var mask_2_graphics_977 = new cjs.Graphics().p("EgNTKfbMB8NhB1MBB1B8OMh8OBB1g");
	var mask_2_graphics_978 = new cjs.Graphics().p("EgM4KfSMB8ehBXMBBXB8fMh8fBBWg");
	var mask_2_graphics_979 = new cjs.Graphics().p("EgMfKfKMB8shA7MBA7B8tMh8tBA7g");
	var mask_2_graphics_980 = new cjs.Graphics().p("EgMIKfDMB85hAjMBAiB86Mh86BAjg");
	var mask_2_graphics_981 = new cjs.Graphics().p("EgL0Ke8MB9FhAMMBAMB9GMh9GBAMg");
	var mask_2_graphics_982 = new cjs.Graphics().p("EgLiKe3MB9Pg/4MA/4B9PMh9PA/4g");
	var mask_2_graphics_983 = new cjs.Graphics().p("EgLSKeyMB9Yg/nMA/nB9ZMh9ZA/mg");
	var mask_2_graphics_984 = new cjs.Graphics().p("EgLDKeuMB9fg/YMA/XB9hMh9gA/Xg");
	var mask_2_graphics_985 = new cjs.Graphics().p("EgK3KeqMB9mg/KMA/KB9nMh9nA/Kg");
	var mask_2_graphics_986 = new cjs.Graphics().p("EgKtKenMB9sg+/MA+/B9tMh9tA+/g");
	var mask_2_graphics_987 = new cjs.Graphics().p("EgKkKekMB9xg+1MA+1B9yMh9yA+1g");
	var mask_2_graphics_988 = new cjs.Graphics().p("EgKcKeiMB90g+tMA+tB92Mh92A+tg");
	var mask_2_graphics_989 = new cjs.Graphics().p("EgKWKegMB94g+mMA+mB95Mh95A+mg");
	var mask_2_graphics_990 = new cjs.Graphics().p("EgKRKefMB96g+hMA+hB97Mh97A+hg");
	var mask_2_graphics_991 = new cjs.Graphics().p("EgKNKeeMB98g+dMA+dB9+Mh9+A+cg");
	var mask_2_graphics_992 = new cjs.Graphics().p("EgKLKedMB9/g+aMA+ZB9/Mh9/A+ag");
	var mask_2_graphics_993 = new cjs.Graphics().p("EgKJKecMB+Ag+XMA+XB+AMh+AA+Xg");
	var mask_2_graphics_994 = new cjs.Graphics().p("EgKHKecMB+Ag+WMA+WB+BMh+BA+Wg");
	var mask_2_graphics_995 = new cjs.Graphics().p("EgKGKecMB+Ag+VMA+VB+BMh+BA+Vg");
	var mask_2_graphics_996 = new cjs.Graphics().p("EgKGKecMB+Bg+VMA+UB+CMh+BA+Ug");
	var mask_2_graphics_997 = new cjs.Graphics().p("EgKGKebMB+Bg+UMA+UB+CMh+BA+Ug");
	var mask_2_graphics_998 = new cjs.Graphics().p("EgKGKebMB+Bg+UMA+UB+CMh+BA+Ug");

	this.timeline.addTween(cjs.Tween.get(mask_2).to({graphics:null,x:0,y:0}).wait(42).to({graphics:mask_2_graphics_42,x:1245.024,y:-204.3126}).wait(1).to({graphics:mask_2_graphics_43,x:1254.4439,y:-185.6442}).wait(1).to({graphics:mask_2_graphics_44,x:1261.9562,y:-165.9527}).wait(1).to({graphics:mask_2_graphics_45,x:1267.5424,y:-145.3254}).wait(1).to({graphics:mask_2_graphics_46,x:1271.1933,y:-123.8546}).wait(1).to({graphics:mask_2_graphics_47,x:1272.9074,y:-101.6358}).wait(1).to({graphics:mask_2_graphics_48,x:1272.6904,y:-78.7671}).wait(1).to({graphics:mask_2_graphics_49,x:1270.5556,y:-55.3496}).wait(1).to({graphics:mask_2_graphics_50,x:1266.5235,y:-31.4861}).wait(1).to({graphics:mask_2_graphics_51,x:1260.6222,y:-7.2812}).wait(1).to({graphics:mask_2_graphics_52,x:1252.8866,y:17.159}).wait(1).to({graphics:mask_2_graphics_53,x:1243.3589,y:41.7277}).wait(1).to({graphics:mask_2_graphics_54,x:1254.5575,y:66.3176}).wait(1).to({graphics:mask_2_graphics_55,x:1269.444,y:90.8212}).wait(1).to({graphics:mask_2_graphics_56,x:1282.5119,y:115.1314}).wait(1).to({graphics:mask_2_graphics_57,x:1293.7232,y:139.1418}).wait(1).to({graphics:mask_2_graphics_58,x:1303.0474,y:162.7472}).wait(1).to({graphics:mask_2_graphics_59,x:1310.4608,y:185.8439}).wait(1).to({graphics:mask_2_graphics_60,x:1315.8841,y:208.2213}).wait(1).to({graphics:null,x:0,y:0}).wait(11).to({graphics:mask_2_graphics_72,x:951.5666,y:-43.8086}).wait(1).to({graphics:mask_2_graphics_73,x:930.8288,y:-82.2759}).wait(1).to({graphics:mask_2_graphics_74,x:902.9691,y:-110.6715}).wait(1).to({graphics:mask_2_graphics_75,x:868.4348,y:-128.5387}).wait(1).to({graphics:mask_2_graphics_76,x:827.7863,y:-135.5992}).wait(1).to({graphics:mask_2_graphics_77,x:781.6881,y:-131.7595}).wait(1).to({graphics:mask_2_graphics_78,x:730.8982,y:-117.1127}).wait(1).to({graphics:mask_2_graphics_79,x:676.255,y:-91.9367}).wait(1).to({graphics:mask_2_graphics_80,x:618.6625,y:-56.6896}).wait(1).to({graphics:mask_2_graphics_81,x:559.0755,y:-12.002}).wait(1).to({graphics:mask_2_graphics_82,x:498.4578,y:41.3844}).wait(1).to({graphics:null,x:0,y:0}).wait(19).to({graphics:mask_2_graphics_102,x:803.8638,y:286.6725}).wait(1).to({graphics:mask_2_graphics_103,x:815.7759,y:287.6396}).wait(1).to({graphics:mask_2_graphics_104,x:827.2722,y:289.4396}).wait(1).to({graphics:mask_2_graphics_105,x:838.3373,y:292.1026}).wait(1).to({graphics:mask_2_graphics_106,x:848.9572,y:295.6572}).wait(1).to({graphics:mask_2_graphics_107,x:859.1186,y:300.1298}).wait(1).to({graphics:mask_2_graphics_108,x:868.8094,y:305.5445}).wait(1).to({graphics:mask_2_graphics_109,x:878.0187,y:311.9235}).wait(1).to({graphics:mask_2_graphics_110,x:886.7365,y:319.2864}).wait(1).to({graphics:mask_2_graphics_111,x:894.9542,y:327.6507}).wait(1).to({graphics:mask_2_graphics_112,x:902.6564,y:336.9871}).wait(1).to({graphics:null,x:0,y:0}).wait(15).to({graphics:mask_2_graphics_128,x:887.9249,y:661.4702}).wait(1).to({graphics:mask_2_graphics_129,x:895.9249,y:669.4702}).wait(1).to({graphics:mask_2_graphics_130,x:870.4507,y:705.425}).wait(1).to({graphics:mask_2_graphics_131,x:882.4507,y:705.425}).wait(1).to({graphics:mask_2_graphics_132,x:858.2173,y:701.2222}).wait(1).to({graphics:mask_2_graphics_133,x:870.2173,y:701.2222}).wait(1).to({graphics:mask_2_graphics_134,x:825.7504,y:701.5878}).wait(1).to({graphics:mask_2_graphics_135,x:835.7504,y:701.5878}).wait(1).to({graphics:null,x:0,y:0}).wait(20).to({graphics:mask_2_graphics_156,x:1260.458,y:514.2744}).wait(1).to({graphics:mask_2_graphics_157,x:1275.394,y:531.016}).wait(1).to({graphics:mask_2_graphics_158,x:1287.7747,y:551.3839}).wait(1).to({graphics:mask_2_graphics_159,x:1297.5183,y:559.773}).wait(1).to({graphics:mask_2_graphics_160,x:1304.5647,y:559.583}).wait(1).to({graphics:mask_2_graphics_161,x:1308.8705,y:559.3937}).wait(1).to({graphics:mask_2_graphics_162,x:1310.4092,y:559.2051}).wait(1).to({graphics:mask_2_graphics_163,x:1342.706,y:592.5519}).wait(1).to({graphics:mask_2_graphics_164,x:1373.8262,y:627.4917}).wait(1).to({graphics:mask_2_graphics_165,x:1401.779,y:662.0089}).wait(1).to({graphics:mask_2_graphics_166,x:1426.3648,y:695.8553}).wait(1).to({graphics:null,x:0,y:0}).wait(25).to({graphics:mask_2_graphics_192,x:860.5178,y:1197.4846}).wait(1).to({graphics:mask_2_graphics_193,x:870.5178,y:1207.4846}).wait(1).to({graphics:mask_2_graphics_194,x:872.7986,y:1267.3582}).wait(1).to({graphics:mask_2_graphics_195,x:882.7986,y:1277.3582}).wait(1).to({graphics:mask_2_graphics_196,x:899.0922,y:1308.8842}).wait(1).to({graphics:mask_2_graphics_197,x:909.0922,y:1318.8842}).wait(1).to({graphics:mask_2_graphics_198,x:919.3186,y:1341.111}).wait(1).to({graphics:mask_2_graphics_199,x:929.3186,y:1351.111}).wait(1).to({graphics:mask_2_graphics_200,x:937.6476,y:1357.3983}).wait(1).to({graphics:mask_2_graphics_201,x:947.6476,y:1367.3983}).wait(1).to({graphics:mask_2_graphics_202,x:951.9342,y:1355.128}).wait(1).to({graphics:mask_2_graphics_203,x:965.9342,y:1355.128}).wait(1).to({graphics:mask_2_graphics_204,x:989.3672,y:1378.2849}).wait(1).to({graphics:mask_2_graphics_205,x:1037.8784,y:1417.8883}).wait(1).to({graphics:mask_2_graphics_206,x:1085.0088,y:1451.4264}).wait(1).to({graphics:mask_2_graphics_207,x:1130.3104,y:1478.7496}).wait(1).to({graphics:mask_2_graphics_208,x:1173.3846,y:1499.7845}).wait(1).to({graphics:mask_2_graphics_209,x:1213.8841,y:1514.5382}).wait(1).to({graphics:mask_2_graphics_210,x:1251.5139,y:1523.0914}).wait(1).to({graphics:mask_2_graphics_211,x:1286.0303,y:1525.5913}).wait(1).to({graphics:mask_2_graphics_212,x:1317.2406,y:1522.2444}).wait(1).to({graphics:mask_2_graphics_213,x:1345.0012,y:1513.309}).wait(1).to({graphics:mask_2_graphics_214,x:1369.2154,y:1499.0875}).wait(1).to({graphics:mask_2_graphics_215,x:1389.8307,y:1479.919}).wait(1).to({graphics:mask_2_graphics_216,x:1406.8357,y:1456.1721}).wait(1).to({graphics:mask_2_graphics_217,x:1420.2572,y:1428.2377}).wait(1).to({graphics:mask_2_graphics_218,x:1430.0963,y:1396.5013}).wait(1).to({graphics:null,x:0,y:0}).wait(19).to({graphics:mask_2_graphics_238,x:1301.5234,y:1056.6425}).wait(1).to({graphics:mask_2_graphics_239,x:1287.8489,y:1066.6281}).wait(1).to({graphics:mask_2_graphics_240,x:1274.3465,y:1076.4276}).wait(1).to({graphics:mask_2_graphics_241,x:1261.0185,y:1086.0416}).wait(1).to({graphics:mask_2_graphics_242,x:1247.868,y:1095.4715}).wait(1).to({graphics:mask_2_graphics_243,x:1234.8981,y:1104.7182}).wait(1).to({graphics:mask_2_graphics_244,x:1222.1118,y:1113.783}).wait(1).to({graphics:mask_2_graphics_245,x:1209.5121,y:1122.6667}).wait(1).to({graphics:mask_2_graphics_246,x:1197.1019,y:1131.3704}).wait(1).to({graphics:mask_2_graphics_247,x:1184.8841,y:1139.8951}).wait(1).to({graphics:mask_2_graphics_248,x:1172.8613,y:1148.2417}).wait(1).to({graphics:mask_2_graphics_249,x:1161.0362,y:1156.4111}).wait(1).to({graphics:mask_2_graphics_250,x:1149.4115,y:1164.4042}).wait(1).to({graphics:mask_2_graphics_251,x:1137.9898,y:1172.2217}).wait(1).to({graphics:mask_2_graphics_252,x:1126.7734,y:1179.8646}).wait(1).to({graphics:mask_2_graphics_253,x:1115.7648,y:1187.3335}).wait(1).to({graphics:mask_2_graphics_254,x:1104.9663,y:1194.6293}).wait(1).to({graphics:mask_2_graphics_255,x:1094.3802,y:1201.7526}).wait(1).to({graphics:mask_2_graphics_256,x:1084.0087,y:1208.7041}).wait(1).to({graphics:mask_2_graphics_257,x:1073.854,y:1215.4844}).wait(1).to({graphics:mask_2_graphics_258,x:1063.8929,y:1222.0805}).wait(1).to({graphics:null,x:0,y:0}).wait(19).to({graphics:mask_2_graphics_278,x:878.2695,y:1368.5227}).wait(1).to({graphics:mask_2_graphics_279,x:882.7003,y:1374.2376}).wait(1).to({graphics:mask_2_graphics_280,x:887.1289,y:1379.9519}).wait(1).to({graphics:mask_2_graphics_281,x:891.5575,y:1385.6662}).wait(1).to({graphics:mask_2_graphics_282,x:895.9861,y:1391.3805}).wait(1).to({graphics:mask_2_graphics_283,x:900.4146,y:1397.0947}).wait(1).to({graphics:mask_2_graphics_284,x:904.8432,y:1402.809}).wait(1).to({graphics:mask_2_graphics_285,x:909.2718,y:1408.5233}).wait(1).to({graphics:mask_2_graphics_286,x:913.7003,y:1414.2376}).wait(1).to({graphics:mask_2_graphics_287,x:918.1289,y:1419.9519}).wait(1).to({graphics:mask_2_graphics_288,x:922.5575,y:1425.6662}).wait(1).to({graphics:mask_2_graphics_289,x:926.9861,y:1431.3805}).wait(1).to({graphics:mask_2_graphics_290,x:931.4146,y:1437.0947}).wait(1).to({graphics:mask_2_graphics_291,x:935.8432,y:1442.809}).wait(1).to({graphics:mask_2_graphics_292,x:940.2695,y:1448.5227}).wait(1).to({graphics:null,x:0,y:0}).wait(27).to({graphics:mask_2_graphics_320,x:1067.4852,y:1596.0255}).wait(1).to({graphics:mask_2_graphics_321,x:1071.5591,y:1601.1735}).wait(1).to({graphics:mask_2_graphics_322,x:1075.6332,y:1606.3217}).wait(1).to({graphics:mask_2_graphics_323,x:1079.7073,y:1611.4698}).wait(1).to({graphics:mask_2_graphics_324,x:1083.7813,y:1616.618}).wait(1).to({graphics:mask_2_graphics_325,x:1087.8554,y:1621.7661}).wait(1).to({graphics:mask_2_graphics_326,x:1091.9295,y:1626.9143}).wait(1).to({graphics:mask_2_graphics_327,x:1096.0035,y:1632.0624}).wait(1).to({graphics:mask_2_graphics_328,x:1100.0776,y:1637.2105}).wait(1).to({graphics:mask_2_graphics_329,x:1104.1517,y:1642.3587}).wait(1).to({graphics:mask_2_graphics_330,x:1108.2258,y:1647.5068}).wait(1).to({graphics:mask_2_graphics_331,x:1112.2998,y:1652.655}).wait(1).to({graphics:mask_2_graphics_332,x:1116.3739,y:1657.8031}).wait(1).to({graphics:mask_2_graphics_333,x:1120.448,y:1662.9513}).wait(1).to({graphics:mask_2_graphics_334,x:1124.5221,y:1668.0994}).wait(1).to({graphics:mask_2_graphics_335,x:1128.5961,y:1673.2476}).wait(1).to({graphics:mask_2_graphics_336,x:1132.6702,y:1678.3957}).wait(1).to({graphics:mask_2_graphics_337,x:1136.7443,y:1683.5439}).wait(1).to({graphics:mask_2_graphics_338,x:1140.8184,y:1688.692}).wait(1).to({graphics:mask_2_graphics_339,x:1144.8924,y:1693.8402}).wait(1).to({graphics:mask_2_graphics_340,x:1148.9665,y:1698.9883}).wait(1).to({graphics:mask_2_graphics_341,x:1153.0406,y:1704.1365}).wait(1).to({graphics:mask_2_graphics_342,x:1157.1147,y:1709.2846}).wait(1).to({graphics:mask_2_graphics_343,x:1161.1887,y:1714.4328}).wait(1).to({graphics:mask_2_graphics_344,x:1165.2628,y:1719.5809}).wait(1).to({graphics:mask_2_graphics_345,x:1169.3369,y:1724.7291}).wait(1).to({graphics:mask_2_graphics_346,x:1173.411,y:1729.8772}).wait(1).to({graphics:mask_2_graphics_347,x:1177.485,y:1735.0254}).wait(1).to({graphics:mask_2_graphics_348,x:1181.5591,y:1740.1735}).wait(1).to({graphics:mask_2_graphics_349,x:1185.6332,y:1745.3217}).wait(1).to({graphics:mask_2_graphics_350,x:1189.7073,y:1750.4698}).wait(1).to({graphics:mask_2_graphics_351,x:1193.7813,y:1755.618}).wait(1).to({graphics:mask_2_graphics_352,x:1197.8554,y:1760.7661}).wait(1).to({graphics:mask_2_graphics_353,x:1201.9295,y:1765.9143}).wait(1).to({graphics:mask_2_graphics_354,x:1206.0035,y:1771.0624}).wait(1).to({graphics:mask_2_graphics_355,x:1210.0776,y:1776.2105}).wait(1).to({graphics:mask_2_graphics_356,x:1214.1517,y:1781.3587}).wait(1).to({graphics:mask_2_graphics_357,x:1218.2258,y:1786.5068}).wait(1).to({graphics:mask_2_graphics_358,x:1222.2998,y:1791.655}).wait(1).to({graphics:mask_2_graphics_359,x:1226.3739,y:1796.8031}).wait(1).to({graphics:mask_2_graphics_360,x:1230.448,y:1801.9513}).wait(1).to({graphics:mask_2_graphics_361,x:1234.5221,y:1807.0994}).wait(1).to({graphics:mask_2_graphics_362,x:1238.5961,y:1812.2476}).wait(1).to({graphics:mask_2_graphics_363,x:1242.6702,y:1817.3957}).wait(1).to({graphics:mask_2_graphics_364,x:1246.7443,y:1822.5439}).wait(1).to({graphics:mask_2_graphics_365,x:1250.8184,y:1827.692}).wait(1).to({graphics:mask_2_graphics_366,x:1254.8924,y:1832.8402}).wait(1).to({graphics:mask_2_graphics_367,x:1258.9665,y:1837.9883}).wait(1).to({graphics:mask_2_graphics_368,x:1263.0406,y:1843.1365}).wait(1).to({graphics:mask_2_graphics_369,x:1267.1147,y:1848.2846}).wait(1).to({graphics:mask_2_graphics_370,x:1271.1887,y:1853.4328}).wait(1).to({graphics:mask_2_graphics_371,x:1275.2628,y:1858.5809}).wait(1).to({graphics:mask_2_graphics_372,x:1279.3369,y:1863.7291}).wait(1).to({graphics:mask_2_graphics_373,x:1283.411,y:1868.8772}).wait(1).to({graphics:mask_2_graphics_374,x:1287.4852,y:1874.0255}).wait(1).to({graphics:null,x:0,y:0}).wait(9).to({graphics:mask_2_graphics_384,x:1416.5947,y:1956.3445}).wait(1).to({graphics:mask_2_graphics_385,x:1411.0289,y:1953.0067}).wait(1).to({graphics:mask_2_graphics_386,x:1404.0931,y:1949.6689}).wait(1).to({graphics:mask_2_graphics_387,x:1395.7734,y:1946.3312}).wait(1).to({graphics:mask_2_graphics_388,x:1386.0529,y:1942.9937}).wait(1).to({graphics:mask_2_graphics_389,x:1374.919,y:1939.6562}).wait(1).to({graphics:mask_2_graphics_390,x:1362.3638,y:1936.3189}).wait(1).to({graphics:mask_2_graphics_391,x:1366.6796,y:1951.2776}).wait(1).to({graphics:mask_2_graphics_392,x:1376.5451,y:1973.21}).wait(1).to({graphics:mask_2_graphics_393,x:1384.8542,y:1995.0046}).wait(1).to({graphics:mask_2_graphics_394,x:1391.5374,y:2016.5819}).wait(1).to({graphics:mask_2_graphics_395,x:1396.5303,y:2037.8629}).wait(1).to({graphics:mask_2_graphics_396,x:1399.7392,y:2058.7591}).wait(1).to({graphics:null,x:0,y:0}).wait(15).to({graphics:mask_2_graphics_412,x:1009.345,y:1864.3744}).wait(1).to({graphics:mask_2_graphics_413,x:1009.343,y:1871.6674}).wait(1).to({graphics:mask_2_graphics_414,x:1009.343,y:1879.2664}).wait(1).to({graphics:mask_2_graphics_415,x:1009.343,y:1887.1714}).wait(1).to({graphics:mask_2_graphics_416,x:1009.343,y:1895.3824}).wait(1).to({graphics:mask_2_graphics_417,x:1009.343,y:1903.8994}).wait(1).to({graphics:mask_2_graphics_418,x:1009.343,y:1912.7224}).wait(1).to({graphics:mask_2_graphics_419,x:1009.343,y:1921.8514}).wait(1).to({graphics:mask_2_graphics_420,x:1009.343,y:1931.2864}).wait(1).to({graphics:mask_2_graphics_421,x:1009.343,y:1941.0274}).wait(1).to({graphics:mask_2_graphics_422,x:1009.343,y:1951.0744}).wait(1).to({graphics:mask_2_graphics_423,x:1009.343,y:1961.4274}).wait(1).to({graphics:mask_2_graphics_424,x:1009.343,y:1972.0864}).wait(1).to({graphics:mask_2_graphics_425,x:1009.343,y:1983.0514}).wait(1).to({graphics:mask_2_graphics_426,x:1009.343,y:1994.3224}).wait(1).to({graphics:mask_2_graphics_427,x:1009.343,y:2005.8994}).wait(1).to({graphics:mask_2_graphics_428,x:1009.343,y:2017.7824}).wait(1).to({graphics:mask_2_graphics_429,x:1009.343,y:2029.9714}).wait(1).to({graphics:mask_2_graphics_430,x:1009.343,y:2042.4664}).wait(1).to({graphics:mask_2_graphics_431,x:1009.343,y:2055.2674}).wait(1).to({graphics:mask_2_graphics_432,x:1009.345,y:2068.3744}).wait(1).to({graphics:null,x:0,y:0}).wait(7).to({graphics:mask_2_graphics_440,x:1073.261,y:2079.7189}).wait(1).to({graphics:mask_2_graphics_441,x:1073.2682,y:2091.9099}).wait(1).to({graphics:mask_2_graphics_442,x:1073.2682,y:2103.8824}).wait(1).to({graphics:mask_2_graphics_443,x:1073.2682,y:2115.6365}).wait(1).to({graphics:mask_2_graphics_444,x:1073.2682,y:2127.1723}).wait(1).to({graphics:mask_2_graphics_445,x:1073.2682,y:2138.4898}).wait(1).to({graphics:mask_2_graphics_446,x:1073.2682,y:2149.5889}).wait(1).to({graphics:mask_2_graphics_447,x:1073.2682,y:2160.4697}).wait(1).to({graphics:mask_2_graphics_448,x:1073.2682,y:2171.1321}).wait(1).to({graphics:mask_2_graphics_449,x:1073.2682,y:2181.5762}).wait(1).to({graphics:mask_2_graphics_450,x:1073.2682,y:2191.8019}).wait(1).to({graphics:mask_2_graphics_451,x:1073.2682,y:2201.8093}).wait(1).to({graphics:mask_2_graphics_452,x:1073.2682,y:2211.5984}).wait(1).to({graphics:mask_2_graphics_453,x:1073.2682,y:2221.1691}).wait(1).to({graphics:mask_2_graphics_454,x:1073.2682,y:2230.5215}).wait(1).to({graphics:mask_2_graphics_455,x:1073.2682,y:2239.6555}).wait(1).to({graphics:mask_2_graphics_456,x:1073.2682,y:2248.5712}).wait(1).to({graphics:mask_2_graphics_457,x:1073.2682,y:2257.2685}).wait(1).to({graphics:mask_2_graphics_458,x:1073.2682,y:2265.7475}).wait(1).to({graphics:mask_2_graphics_459,x:1073.2682,y:2274.0081}).wait(1).to({graphics:mask_2_graphics_460,x:1073.2682,y:2282.0504}).wait(1).to({graphics:mask_2_graphics_461,x:1073.2682,y:2289.8744}).wait(1).to({graphics:mask_2_graphics_462,x:1073.2682,y:2297.48}).wait(1).to({graphics:mask_2_graphics_463,x:1073.2682,y:2304.8673}).wait(1).to({graphics:mask_2_graphics_464,x:1073.2682,y:2312.0362}).wait(1).to({graphics:mask_2_graphics_465,x:1073.2682,y:2318.9868}).wait(1).to({graphics:mask_2_graphics_466,x:1073.261,y:2325.7189}).wait(1).to({graphics:null,x:0,y:0}).wait(53).to({graphics:mask_2_graphics_520,x:1369.3047,y:2674.4103}).wait(1).to({graphics:mask_2_graphics_521,x:1368.0848,y:2683.9747}).wait(1).to({graphics:mask_2_graphics_522,x:1366.8161,y:2693.4353}).wait(1).to({graphics:mask_2_graphics_523,x:1365.5084,y:2702.7943}).wait(1).to({graphics:mask_2_graphics_524,x:1364.1629,y:2712.052}).wait(1).to({graphics:mask_2_graphics_525,x:1362.7808,y:2721.2083}).wait(1).to({graphics:mask_2_graphics_526,x:1361.3633,y:2730.2633}).wait(1).to({graphics:mask_2_graphics_527,x:1359.9117,y:2739.2172}).wait(1).to({graphics:mask_2_graphics_528,x:1358.4271,y:2748.0701}).wait(1).to({graphics:mask_2_graphics_529,x:1356.9107,y:2756.8221}).wait(1).to({graphics:mask_2_graphics_530,x:1355.3639,y:2765.4734}).wait(1).to({graphics:mask_2_graphics_531,x:1353.7877,y:2774.0242}).wait(1).to({graphics:mask_2_graphics_532,x:1352.1834,y:2782.4745}).wait(1).to({graphics:mask_2_graphics_533,x:1350.5522,y:2790.8247}).wait(1).to({graphics:mask_2_graphics_534,x:1348.8953,y:2799.075}).wait(1).to({graphics:mask_2_graphics_535,x:1347.2139,y:2807.2255}).wait(1).to({graphics:mask_2_graphics_536,x:1345.5093,y:2815.2764}).wait(1).to({graphics:mask_2_graphics_537,x:1343.7825,y:2823.228}).wait(1).to({graphics:mask_2_graphics_538,x:1342.0348,y:2831.0806}).wait(1).to({graphics:mask_2_graphics_539,x:1340.2674,y:2838.8344}).wait(1).to({graphics:mask_2_graphics_540,x:1338.4657,y:2846.51}).wait(1).to({graphics:mask_2_graphics_541,x:1337.9612,y:2856.6503}).wait(1).to({graphics:mask_2_graphics_542,x:1337.3166,y:2866.9186}).wait(1).to({graphics:mask_2_graphics_543,x:1336.5281,y:2877.3125}).wait(1).to({graphics:mask_2_graphics_544,x:1335.5883,y:2887.8281}).wait(1).to({graphics:mask_2_graphics_545,x:1334.49,y:2898.4615}).wait(1).to({graphics:mask_2_graphics_546,x:1333.2255,y:2909.2084}).wait(1).to({graphics:mask_2_graphics_547,x:1331.7873,y:2920.0646}).wait(1).to({graphics:mask_2_graphics_548,x:1330.1676,y:2931.0255}).wait(1).to({graphics:mask_2_graphics_549,x:1328.3585,y:2942.0865}).wait(1).to({graphics:mask_2_graphics_550,x:1326.3522,y:2953.2427}).wait(1).to({graphics:mask_2_graphics_551,x:1324.1404,y:2964.4892}).wait(1).to({graphics:mask_2_graphics_552,x:1321.7151,y:2975.8208}).wait(1).to({graphics:mask_2_graphics_553,x:1319.068,y:2987.2321}).wait(1).to({graphics:mask_2_graphics_554,x:1316.1907,y:2998.7175}).wait(1).to({graphics:mask_2_graphics_555,x:1313.0749,y:3010.2715}).wait(1).to({graphics:mask_2_graphics_556,x:1309.712,y:3021.8881}).wait(1).to({graphics:mask_2_graphics_557,x:1306.0935,y:3033.5612}).wait(1).to({graphics:mask_2_graphics_558,x:1302.2108,y:3045.2846}).wait(1).to({graphics:mask_2_graphics_559,x:1298.0552,y:3057.0519}).wait(1).to({graphics:mask_2_graphics_560,x:1293.5873,y:3068.8194}).wait(1).to({graphics:null,x:0,y:0}).wait(15).to({graphics:mask_2_graphics_576,x:1002.8118,y:2851.9467}).wait(1).to({graphics:mask_2_graphics_577,x:1008.8747,y:2861.4544}).wait(1).to({graphics:mask_2_graphics_578,x:1014.0605,y:2870.8171}).wait(1).to({graphics:mask_2_graphics_579,x:1018.419,y:2880.0349}).wait(1).to({graphics:mask_2_graphics_580,x:1021.9942,y:2889.1077}).wait(1).to({graphics:mask_2_graphics_581,x:1024.8299,y:2898.0355}).wait(1).to({graphics:mask_2_graphics_582,x:1026.9693,y:2906.8182}).wait(1).to({graphics:mask_2_graphics_583,x:1028.4554,y:2915.4558}).wait(1).to({graphics:mask_2_graphics_584,x:1029.3301,y:2923.9484}).wait(1).to({graphics:mask_2_graphics_585,x:1029.6347,y:2932.2958}).wait(1).to({graphics:mask_2_graphics_586,x:1029.4097,y:2940.4981}).wait(1).to({graphics:mask_2_graphics_587,x:1028.6942,y:2948.5553}).wait(1).to({graphics:mask_2_graphics_588,x:1027.4932,y:2956.4628}).wait(1).to({graphics:mask_2_graphics_589,x:1034.4907,y:2964.0925}).wait(1).to({graphics:mask_2_graphics_590,x:1041.38,y:2971.6351}).wait(1).to({graphics:mask_2_graphics_591,x:1048.1604,y:2979.0904}).wait(1).to({graphics:mask_2_graphics_592,x:1054.8326,y:2986.4586}).wait(1).to({graphics:mask_2_graphics_593,x:1061.3975,y:2993.7396}).wait(1).to({graphics:mask_2_graphics_594,x:1067.8559,y:3000.9333}).wait(1).to({graphics:mask_2_graphics_595,x:1074.2087,y:3008.0399}).wait(1).to({graphics:mask_2_graphics_596,x:1080.4568,y:3015.0593}).wait(1).to({graphics:mask_2_graphics_597,x:1086.6009,y:3021.9915}).wait(1).to({graphics:mask_2_graphics_598,x:1092.6419,y:3028.8364}).wait(1).to({graphics:mask_2_graphics_599,x:1098.5807,y:3035.5942}).wait(1).to({graphics:mask_2_graphics_600,x:1104.4181,y:3042.2648}).wait(1).to({graphics:mask_2_graphics_601,x:1110.1549,y:3048.8482}).wait(1).to({graphics:mask_2_graphics_602,x:1115.792,y:3055.3445}).wait(1).to({graphics:mask_2_graphics_603,x:1121.3303,y:3061.7535}).wait(1).to({graphics:mask_2_graphics_604,x:1126.7354,y:3068.0661}).wait(1).to({graphics:mask_2_graphics_605,x:1114.9605,y:3059.1052}).wait(1).to({graphics:null,x:0,y:0}).wait(30).to({graphics:mask_2_graphics_636,x:1152.2647,y:3339.6826}).wait(1).to({graphics:mask_2_graphics_637,x:1138.2516,y:3331.3474}).wait(1).to({graphics:mask_2_graphics_638,x:1123.2682,y:3322.4349}).wait(1).to({graphics:mask_2_graphics_639,x:1107.3182,y:3312.9474}).wait(1).to({graphics:mask_2_graphics_640,x:1090.4016,y:3302.8849}).wait(1).to({graphics:mask_2_graphics_641,x:1072.5182,y:3292.2474}).wait(1).to({graphics:mask_2_graphics_642,x:1053.6682,y:3281.0349}).wait(1).to({graphics:mask_2_graphics_643,x:1033.8516,y:3269.2474}).wait(1).to({graphics:mask_2_graphics_644,x:1013.0682,y:3256.8849}).wait(1).to({graphics:mask_2_graphics_645,x:991.3182,y:3243.9474}).wait(1).to({graphics:mask_2_graphics_646,x:968.6016,y:3230.4349}).wait(1).to({graphics:mask_2_graphics_647,x:944.9182,y:3216.3474}).wait(1).to({graphics:mask_2_graphics_648,x:920.2647,y:3201.6826}).wait(1).to({graphics:null,x:0,y:0}).wait(11).to({graphics:mask_2_graphics_660,x:729.6749,y:3126.15}).wait(1).to({graphics:mask_2_graphics_661,x:694.1319,y:3066.1744}).wait(1).to({graphics:mask_2_graphics_662,x:647.0645,y:3000.4683}).wait(1).to({graphics:mask_2_graphics_663,x:615.4536,y:2957.6603}).wait(1).to({graphics:mask_2_graphics_664,x:609.9726,y:2950.0519}).wait(1).to({graphics:mask_2_graphics_665,x:593.486,y:2942}).wait(1).to({graphics:mask_2_graphics_666,x:512.672,y:2933.461}).wait(1).to({graphics:mask_2_graphics_667,x:413.8022,y:2923.4324}).wait(1).to({graphics:mask_2_graphics_668,x:313.4712,y:2913.3983}).wait(1).to({graphics:mask_2_graphics_669,x:214.9622,y:2903.3604}).wait(1).to({graphics:mask_2_graphics_670,x:121.5056,y:2893.3206}).wait(1).to({graphics:mask_2_graphics_671,x:36.1812,y:2883.2809}).wait(1).to({graphics:mask_2_graphics_672,x:-38.0644,y:2875.0572}).wait(1).to({graphics:null,x:0,y:0}).wait(11).to({graphics:mask_2_graphics_684,x:134.0301,y:3267.01}).wait(1).to({graphics:mask_2_graphics_685,x:166.0273,y:3271.0114}).wait(1).to({graphics:mask_2_graphics_686,x:198.0273,y:3275.0114}).wait(1).to({graphics:mask_2_graphics_687,x:230.0273,y:3279.0114}).wait(1).to({graphics:mask_2_graphics_688,x:262.0273,y:3283.0114}).wait(1).to({graphics:mask_2_graphics_689,x:294.0273,y:3287.0114}).wait(1).to({graphics:mask_2_graphics_690,x:326.0273,y:3291.0114}).wait(1).to({graphics:mask_2_graphics_691,x:358.0273,y:3295.0114}).wait(1).to({graphics:mask_2_graphics_692,x:390.0273,y:3299.0114}).wait(1).to({graphics:mask_2_graphics_693,x:422.0273,y:3303.0114}).wait(1).to({graphics:mask_2_graphics_694,x:454.0301,y:3307.01}).wait(1).to({graphics:null,x:0,y:0}).wait(13).to({graphics:mask_2_graphics_708,x:668.4362,y:3367.3453}).wait(1).to({graphics:mask_2_graphics_709,x:679.6319,y:3393.4504}).wait(1).to({graphics:mask_2_graphics_710,x:690.7761,y:3419.525}).wait(1).to({graphics:mask_2_graphics_711,x:701.8716,y:3445.5696}).wait(1).to({graphics:mask_2_graphics_712,x:712.9189,y:3471.5833}).wait(1).to({graphics:mask_2_graphics_713,x:723.9185,y:3497.5655}).wait(1).to({graphics:mask_2_graphics_714,x:734.8629,y:3523.4604}).wait(1).to({graphics:mask_2_graphics_715,x:746.8629,y:3547.4604}).wait(1).to({graphics:mask_2_graphics_716,x:722.4348,y:3601.9102}).wait(1).to({graphics:mask_2_graphics_717,x:742.4348,y:3621.9102}).wait(1).to({graphics:mask_2_graphics_718,x:690.5714,y:3657.9133}).wait(1).to({graphics:mask_2_graphics_719,x:718.5714,y:3657.9133}).wait(1).to({graphics:null,x:0,y:0}).wait(8).to({graphics:mask_2_graphics_728,x:781.4499,y:3715.7402}).wait(1).to({graphics:mask_2_graphics_729,x:780.8906,y:3628.8982}).wait(1).to({graphics:mask_2_graphics_730,x:780.2045,y:3539.2959}).wait(1).to({graphics:mask_2_graphics_731,x:846.8484,y:3518.3391}).wait(1).to({graphics:mask_2_graphics_732,x:930.8259,y:3519.4951}).wait(1).to({graphics:mask_2_graphics_733,x:1007.079,y:3520.5806}).wait(1).to({graphics:mask_2_graphics_734,x:1073.3591,y:3521.5781}).wait(1).to({graphics:mask_2_graphics_735,x:1128.0758,y:3522.4755}).wait(1).to({graphics:mask_2_graphics_736,x:1170.2334,y:3523.2552}).wait(1).to({graphics:mask_2_graphics_737,x:1180.6437,y:3536.291}).wait(1).to({graphics:mask_2_graphics_738,x:1187.7848,y:3548.2655}).wait(1).to({graphics:mask_2_graphics_739,x:1192.2098,y:3559.183}).wait(1).to({graphics:mask_2_graphics_740,x:1194.4533,y:3569.0474}).wait(1).to({graphics:mask_2_graphics_741,x:1195.0224,y:3577.8626}).wait(1).to({graphics:mask_2_graphics_742,x:1194.3471,y:3585.6097}).wait(1).to({graphics:null,x:0,y:0}).wait(17).to({graphics:mask_2_graphics_760,x:1206.7636,y:3916.4977}).wait(1).to({graphics:mask_2_graphics_761,x:1200.0446,y:3919.5531}).wait(1).to({graphics:mask_2_graphics_762,x:1193.3417,y:3922.5151}).wait(1).to({graphics:mask_2_graphics_763,x:1186.664,y:3925.3868}).wait(1).to({graphics:mask_2_graphics_764,x:1180.0187,y:3928.1707}).wait(1).to({graphics:mask_2_graphics_765,x:1173.4126,y:3930.8693}).wait(1).to({graphics:mask_2_graphics_766,x:1166.8524,y:3933.4849}).wait(1).to({graphics:mask_2_graphics_767,x:1160.3445,y:3936.0197}).wait(1).to({graphics:mask_2_graphics_768,x:1153.8953,y:3938.4759}).wait(1).to({graphics:mask_2_graphics_769,x:1147.5108,y:3940.8554}).wait(1).to({graphics:mask_2_graphics_770,x:1141.1969,y:3943.1601}).wait(1).to({graphics:mask_2_graphics_771,x:1134.9594,y:3945.3921}).wait(1).to({graphics:mask_2_graphics_772,x:1128.8038,y:3947.5528}).wait(1).to({graphics:mask_2_graphics_773,x:1122.7355,y:3949.6441}).wait(1).to({graphics:mask_2_graphics_774,x:1116.7595,y:3951.6676}).wait(1).to({graphics:mask_2_graphics_775,x:1110.8808,y:3953.6247}).wait(1).to({graphics:mask_2_graphics_776,x:1105.1043,y:3955.5168}).wait(1).to({graphics:mask_2_graphics_777,x:1099.4345,y:3957.3453}).wait(1).to({graphics:mask_2_graphics_778,x:1093.8759,y:3959.1115}).wait(1).to({graphics:mask_2_graphics_779,x:1088.4328,y:3960.8166}).wait(1).to({graphics:mask_2_graphics_780,x:1083.1092,y:3962.4618}).wait(1).to({graphics:mask_2_graphics_781,x:1077.9092,y:3964.048}).wait(1).to({graphics:mask_2_graphics_782,x:1072.8363,y:3965.5764}).wait(1).to({graphics:mask_2_graphics_783,x:1067.8944,y:3967.0478}).wait(1).to({graphics:mask_2_graphics_784,x:1063.0868,y:3968.4633}).wait(1).to({graphics:mask_2_graphics_785,x:1058.4168,y:3969.8235}).wait(1).to({graphics:mask_2_graphics_786,x:1053.8877,y:3971.1294}).wait(1).to({graphics:mask_2_graphics_787,x:1049.5023,y:3972.3816}).wait(1).to({graphics:mask_2_graphics_788,x:1045.2636,y:3973.5809}).wait(1).to({graphics:mask_2_graphics_789,x:1041.1743,y:3974.7279}).wait(1).to({graphics:mask_2_graphics_790,x:1037.2007,y:3975.7833}).wait(1).to({graphics:null,x:0,y:0}).wait(85).to({graphics:mask_2_graphics_876,x:1160.2836,y:4428.594}).wait(1).to({graphics:mask_2_graphics_877,x:1158.6579,y:4431.1938}).wait(1).to({graphics:mask_2_graphics_878,x:1156.5059,y:4434.3162}).wait(1).to({graphics:mask_2_graphics_879,x:1153.7411,y:4437.8847}).wait(1).to({graphics:mask_2_graphics_880,x:1150.2551,y:4441.8131}).wait(1).to({graphics:mask_2_graphics_881,x:1145.9152,y:4446.009}).wait(1).to({graphics:mask_2_graphics_882,x:1140.5618,y:4450.3776}).wait(1).to({graphics:mask_2_graphics_883,x:1134.0067,y:4454.8272}).wait(1).to({graphics:mask_2_graphics_884,x:1126.0309,y:4459.2751}).wait(1).to({graphics:mask_2_graphics_885,x:1116.3837,y:4463.654}).wait(1).to({graphics:mask_2_graphics_886,x:1104.7821,y:4467.9207}).wait(1).to({graphics:mask_2_graphics_887,x:1090.9121,y:4472.0644}).wait(1).to({graphics:mask_2_graphics_888,x:1074.3741,y:4476.0985}).wait(1).to({graphics:null,x:0,y:0}).wait(11).to({graphics:mask_2_graphics_900,x:874.388,y:4550.8494}).wait(1).to({graphics:mask_2_graphics_901,x:806.0132,y:4550.8291}).wait(1).to({graphics:mask_2_graphics_902,x:724.6818,y:4550.8099}).wait(1).to({graphics:mask_2_graphics_903,x:636.7232,y:4550.7916}).wait(1).to({graphics:mask_2_graphics_904,x:547.5758,y:4550.7715}).wait(1).to({graphics:mask_2_graphics_905,x:499.7465,y:4591.7086}).wait(1).to({graphics:mask_2_graphics_906,x:444.6911,y:4673.1396}).wait(1).to({graphics:mask_2_graphics_907,x:407.3062,y:4741.5027}).wait(1).to({graphics:mask_2_graphics_908,x:389.2608,y:4796.4606}).wait(1).to({graphics:mask_2_graphics_909,x:385.1228,y:4838.9009}).wait(1).to({graphics:mask_2_graphics_910,x:390.0181,y:4870.5659}).wait(1).to({graphics:null,x:0,y:0}).wait(8).to({graphics:mask_2_graphics_919,x:671.1303,y:4700.635}).wait(1).to({graphics:mask_2_graphics_920,x:687.1303,y:4700.635}).wait(1).to({graphics:mask_2_graphics_921,x:701.1303,y:4700.635}).wait(1).to({graphics:mask_2_graphics_922,x:772.6457,y:4564.8105}).wait(1).to({graphics:mask_2_graphics_923,x:920.5097,y:4610.0015}).wait(1).to({graphics:mask_2_graphics_924,x:949.2339,y:4551.3808}).wait(1).to({graphics:mask_2_graphics_925,x:946.4817,y:4563.6497}).wait(1).to({graphics:mask_2_graphics_926,x:955.7004,y:4589.3316}).wait(1).to({graphics:mask_2_graphics_927,x:965.3112,y:4616.9058}).wait(1).to({graphics:mask_2_graphics_928,x:975.2307,y:4646.359}).wait(1).to({graphics:mask_2_graphics_929,x:985.363,y:4677.6737}).wait(1).to({graphics:mask_2_graphics_930,x:995.6377,y:4710.8603}).wait(1).to({graphics:null,x:0,y:0}).wait(3).to({graphics:mask_2_graphics_934,x:870.7337,y:4868.9763}).wait(1).to({graphics:mask_2_graphics_935,x:820.5347,y:4945.5556}).wait(1).to({graphics:mask_2_graphics_936,x:834.5347,y:4955.5556}).wait(1).to({graphics:mask_2_graphics_937,x:852.5347,y:4963.5556}).wait(1).to({graphics:null,x:0,y:0}).wait(8).to({graphics:mask_2_graphics_946,x:979.5141,y:5007.8256}).wait(1).to({graphics:mask_2_graphics_947,x:989.441,y:5013.6176}).wait(1).to({graphics:mask_2_graphics_948,x:998.9063,y:5019.0936}).wait(1).to({graphics:mask_2_graphics_949,x:1007.9243,y:5024.2702}).wait(1).to({graphics:mask_2_graphics_950,x:1016.5097,y:5029.1632}).wait(1).to({graphics:mask_2_graphics_951,x:1024.6772,y:5033.7875}).wait(1).to({graphics:mask_2_graphics_952,x:1032.4409,y:5038.157}).wait(1).to({graphics:mask_2_graphics_953,x:1039.815,y:5042.2848}).wait(1).to({graphics:mask_2_graphics_954,x:1046.8133,y:5046.1831}).wait(1).to({graphics:mask_2_graphics_955,x:1053.4494,y:5049.8634}).wait(1).to({graphics:mask_2_graphics_956,x:1059.7364,y:5053.3364}).wait(1).to({graphics:mask_2_graphics_957,x:1065.6873,y:5056.6122}).wait(1).to({graphics:mask_2_graphics_958,x:1071.3147,y:5059.7002}).wait(1).to({graphics:mask_2_graphics_959,x:1076.6307,y:5062.6094}).wait(1).to({graphics:mask_2_graphics_960,x:1081.6475,y:5065.3482}).wait(1).to({graphics:mask_2_graphics_961,x:1086.3764,y:5067.9244}).wait(1).to({graphics:mask_2_graphics_962,x:1090.8287,y:5070.3455}).wait(1).to({graphics:mask_2_graphics_963,x:1095.0154,y:5072.6187}).wait(1).to({graphics:mask_2_graphics_964,x:1098.9471,y:5074.7505}).wait(1).to({graphics:mask_2_graphics_965,x:1102.634,y:5076.7473}).wait(1).to({graphics:mask_2_graphics_966,x:1106.086,y:5078.6152}).wait(1).to({graphics:mask_2_graphics_967,x:1109.3129,y:5080.3599}).wait(1).to({graphics:mask_2_graphics_968,x:1112.324,y:5081.9869}).wait(1).to({graphics:mask_2_graphics_969,x:1115.1284,y:5083.5015}).wait(1).to({graphics:mask_2_graphics_970,x:1117.735,y:5084.9087}).wait(1).to({graphics:mask_2_graphics_971,x:1120.1523,y:5086.2133}).wait(1).to({graphics:mask_2_graphics_972,x:1122.3888,y:5087.4201}).wait(1).to({graphics:mask_2_graphics_973,x:1124.4526,y:5088.5335}).wait(1).to({graphics:mask_2_graphics_974,x:1126.3517,y:5089.5579}).wait(1).to({graphics:mask_2_graphics_975,x:1128.0937,y:5090.4976}).wait(1).to({graphics:mask_2_graphics_976,x:1129.6862,y:5091.3567}).wait(1).to({graphics:mask_2_graphics_977,x:1131.1367,y:5092.1391}).wait(1).to({graphics:mask_2_graphics_978,x:1132.4524,y:5092.8488}).wait(1).to({graphics:mask_2_graphics_979,x:1133.6404,y:5093.4897}).wait(1).to({graphics:mask_2_graphics_980,x:1134.7076,y:5094.0655}).wait(1).to({graphics:mask_2_graphics_981,x:1135.661,y:5094.5799}).wait(1).to({graphics:mask_2_graphics_982,x:1136.5072,y:5095.0365}).wait(1).to({graphics:mask_2_graphics_983,x:1137.253,y:5095.4389}).wait(1).to({graphics:mask_2_graphics_984,x:1137.9049,y:5095.7908}).wait(1).to({graphics:mask_2_graphics_985,x:1138.4694,y:5096.0954}).wait(1).to({graphics:mask_2_graphics_986,x:1138.953,y:5096.3565}).wait(1).to({graphics:mask_2_graphics_987,x:1139.3621,y:5096.5772}).wait(1).to({graphics:mask_2_graphics_988,x:1139.7029,y:5096.7612}).wait(1).to({graphics:mask_2_graphics_989,x:1139.9818,y:5096.9118}).wait(1).to({graphics:mask_2_graphics_990,x:1140.205,y:5097.0323}).wait(1).to({graphics:mask_2_graphics_991,x:1140.3788,y:5097.1261}).wait(1).to({graphics:mask_2_graphics_992,x:1140.5093,y:5097.1966}).wait(1).to({graphics:mask_2_graphics_993,x:1140.6029,y:5097.2471}).wait(1).to({graphics:mask_2_graphics_994,x:1140.6655,y:5097.2809}).wait(1).to({graphics:mask_2_graphics_995,x:1140.7036,y:5097.3014}).wait(1).to({graphics:mask_2_graphics_996,x:1140.7231,y:5097.312}).wait(1).to({graphics:mask_2_graphics_997,x:1140.7303,y:5097.3158}).wait(1).to({graphics:mask_2_graphics_998,x:1140.7409,y:5097.2821}).wait(1).to({graphics:null,x:0,y:0}).wait(1));

	// Layer_20
	this.instance_2 = new lib.element_2("synched",0);
	this.instance_2.setTransform(1134.5,4998.7,1,1,0,0,0,1134.5,4998.7);
	this.instance_2._off = true;

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.lf(["#D3BD93","#966A40"],[0,0.529],-3493,3123.6,2647.2,4236.7).s().p("EgZwA8bQn/mgpNtGIg+haQhMhshEiMQjElGhImgQgykaAAmgQAAmTBunUQBpm6DLn2QHSx+NQw4QBChUDekKQDgkUCYjaQCxj+BujcIFXAAQkAFvmGHvQ2wc2nsUCQhjEBhBD1QlQTgH+PLQBhC0AxA6IBPBZQNaO9JLEqQEcCQFCA2QDuAqGhAIQEqAGFqhYQHSh0F0jwQQCqWAC1iQAAouoMjuQn0jkt0BoQrVBau2ImQmWDrq/HrIiXjuICWhuQG8kuQ+pyQGOjmIjioQJOi4GqAAQLiAAHiFGQJ0GqAAOCQAAIQkOIMQjQGWmgHeQjyEWq0EIQrYEWpGAAQvLgCssqUg");
	this.shape_1.setTransform(1941.7,1585.275);

	var maskedShapeInstanceList = [this.instance_2,this.shape_1];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_2;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_2}]},42).to({state:[]},18).to({state:[{t:this.instance_2}]},12).to({state:[]},10).to({state:[{t:this.instance_2}]},20).to({state:[]},10).to({state:[{t:this.instance_2}]},16).to({state:[]},8).to({state:[{t:this.instance_2}]},20).to({state:[]},10).to({state:[{t:this.shape_1}]},26).to({state:[]},26).to({state:[{t:this.instance_2}]},20).to({state:[]},20).to({state:[{t:this.instance_2}]},20).to({state:[]},14).to({state:[{t:this.instance_2}]},28).to({state:[]},54).to({state:[{t:this.instance_2}]},10).to({state:[]},12).to({state:[{t:this.instance_2}]},16).to({state:[]},20).to({state:[{t:this.instance_2}]},8).to({state:[]},26).to({state:[{t:this.instance_2}]},54).to({state:[]},40).to({state:[{t:this.instance_2}]},16).to({state:[]},30).to({state:[{t:this.instance_2}]},30).to({state:[]},12).to({state:[{t:this.instance_2}]},12).to({state:[]},12).to({state:[{t:this.instance_2}]},12).to({state:[]},10).to({state:[{t:this.instance_2}]},14).to({state:[]},12).to({state:[{t:this.instance_2}]},8).to({state:[]},14).to({state:[{t:this.instance_2}]},18).to({state:[]},30).to({state:[{t:this.instance_2}]},86).to({state:[]},12).to({state:[{t:this.instance_2}]},12).to({state:[]},10).to({state:[{t:this.instance_2}]},9).to({state:[]},11).to({state:[{t:this.instance_2}]},4).to({state:[]},4).to({state:[{t:this.instance_2}]},8).to({state:[]},53).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(42).to({_off:false},0).to({_off:true},18).wait(12).to({_off:false},0).to({_off:true},10).wait(20).to({_off:false},0).to({_off:true},10).wait(16).to({_off:false},0).to({_off:true},8).wait(20).to({_off:false},0).to({_off:true},10).wait(72).to({_off:false},0).to({_off:true},20).wait(20).to({_off:false},0).to({_off:true},14).wait(28).to({_off:false},0).to({_off:true},54).wait(10).to({_off:false},0).to({_off:true},12).wait(16).to({_off:false},0).to({_off:true},20).wait(8).to({_off:false},0).to({_off:true},26).wait(54).to({_off:false},0).to({_off:true},40).wait(16).to({_off:false},0).to({_off:true},30).wait(30).to({_off:false},0).to({_off:true},12).wait(12).to({_off:false},0).to({_off:true},12).wait(12).to({_off:false},0).to({_off:true},10).wait(14).to({_off:false},0).to({_off:true},12).wait(8).to({_off:false},0).to({_off:true},14).wait(18).to({_off:false},0).to({_off:true},30).wait(86).to({_off:false},0).to({_off:true},12).wait(12).to({_off:false},0).to({_off:true},10).wait(9).to({_off:false},0).to({_off:true},11).wait(4).to({_off:false},0).to({_off:true},4).wait(8).to({_off:false},0).to({_off:true},53).wait(1));

	// Layer_8 (mask)
	var mask_3 = new cjs.Shape();
	mask_3._off = true;
	var mask_3_graphics_42 = new cjs.Graphics().p("EgSQAhmMgoUhDLMB1JAAAMAAABDLg");
	var mask_3_graphics_60 = new cjs.Graphics().p("EhFlAq+MAmwhV7MBkbAAAMAAABV7g");
	var mask_3_graphics_72 = new cjs.Graphics().p("EhEQAq+Mgu4hV7MDmRAAAMAAABV7g");
	var mask_3_graphics_82 = new cjs.Graphics().p("Eiy5AmXMAAAhMtMFlzAAAMAAABMtg");
	var mask_3_graphics_102 = new cjs.Graphics().p("Eiy5AzpMAAAhnRMFlzAAAMAAABafMiYpAAAIpXMyg");
	var mask_3_graphics_112 = new cjs.Graphics().p("Eiy5hV7MFlzAAAMAAABX9MllzBT6g");
	var mask_3_graphics_128 = new cjs.Graphics().p("Eiy5i2yMFlzAAAMAAAAomMllzFE/g");
	var mask_3_graphics_136 = new cjs.Graphics().p("Eiy5BUXMAAAiotMFlzAAAMAAACMlMhqVAAAIJYcIg");
	var mask_3_graphics_156 = new cjs.Graphics().p("Eiy5iVgMFlzAAAMAAACFuMllzClTg");
	var mask_3_graphics_166 = new cjs.Graphics().p("Eiy5gGFMAAAhesMFlzAAAMAAADJjg");
	var mask_3_graphics_192 = new cjs.Graphics().p("EBSICO3QhMhshEiMQjElGhImgQgykaAAmgQAAmTBunVMj/hAAQMAAAj3XMFlzAAAMAAAD3XMhj0gAQQlQThH+PLQBhC0AxA6IBPBZIiPBnIg+hag");
	var mask_3_graphics_204 = new cjs.Graphics().p("EBkSCU6Qn/mgpNtHIg+hZQhMhthEiLQjElGhImgQgykaAAmgQAAmUBunUMj/hAAQMAAAj3XMFlzAAAMAAAD3XMhj0gAQQlQThH+PLQBhCzAxA7IBPBZQNaO8JLErQEcCQFCA2QDhAnGDAKIAAIDQufgbsLp5g");
	var mask_3_graphics_218 = new cjs.Graphics().p("EBkSCU5Qoam2pwuKQhMhshEiMQjElGhImgQgykaAAmgQAAmTBunVMj/hAAQMAAAj3XMFlzAAAMAAAD3XMhj0gAQQlQThH+PLQBhC0AxA6QOOQIJmE4QEcCQFCA2QDuAqGiAIQEqAGFqhYQHSh0F0jwQM8oWChvoIKNBdQg8FaiyFZQjQGWmgHeQjyEWq0EIQrYEWpGAAQvMgCssqUg");
	var mask_3_graphics_238 = new cjs.Graphics().p("EBTaChEMAAAghLIhSh1QhMhshEiMQjElGhImgQgykaAAmgQAAmTBunVMj/hAAQMAAAj3XMFlzAAAMAAAFCHg");
	var mask_3_graphics_258 = new cjs.Graphics().p("Eiy5BJPMAAAkR+MFlzAAAMAAAGRfg");
	var mask_3_graphics_278 = new cjs.Graphics().p("Eiy5kCbMFlzAAAMAAAFL3MllzC5Ag");
	var mask_3_graphics_292 = new cjs.Graphics().p("Eiy5kLrMFlzAAAMAAAF3XMllzCgAg");
	var mask_3_graphics_320 = new cjs.Graphics().p("Eiy5k2HMFlzAAAMAAAGpPMllzDDAg");
	var mask_3_graphics_374 = new cjs.Graphics().p("EBm5EEJMkZyAABMAAAoqAMFlzAAAMAAAIi3MhMBAo2g");
	var mask_3_graphics_384 = new cjs.Graphics().p("EBm6DriIgBrOMkZyAAAMAAAop/MFlzAAAMAAAKTXg");
	var mask_3_graphics_396 = new cjs.Graphics().p("EBQKEMCMAWvgruMkZyAAAMAAAop/MFlzAAAMAAAKTXg");
	var mask_3_graphics_412 = new cjs.Graphics().p("Eiy5Bq1MAAAm0gMFlzAAAMAAAKTXg");
	var mask_3_graphics_432 = new cjs.Graphics().p("Eiy5E2oMAAAptPMFlzAAAMAAAJh/MhkvAAAItgLQg");
	var mask_3_graphics_440 = new cjs.Graphics().p("Eiy5FEIMAAAqIPMFlzAAAMAAAKIPg");
	var mask_3_graphics_466 = new cjs.Graphics().p("Eiy5FqQMAAArUfMFlzAAAMAAALUfg");
	var mask_3_graphics_520 = new cjs.Graphics().p("Eiy5EyMMAAArNXMFlzAAAMAAAM2Xg");
	var mask_3_graphics_560 = new cjs.Graphics().p("Eiy5CYkMAAAqMnMFlzAAAMAAAPoHg");
	var mask_3_graphics_576 = new cjs.Graphics().p("Eiy5GTIMAAAtQvMFlzAAAMAAANePMi8qAdAg");
	var mask_3_graphics_606 = new cjs.Graphics().p("EA8qHXAMjvjikQMAAAsNPMFlzAAAMAAAO0/g");
	var mask_3_graphics_636 = new cjs.Graphics().p("EAdbHlAMAAAgzQMjQUhSgMAAAtEPMFlzAAAMAAAPJ/g");
	var mask_3_graphics_648 = new cjs.Graphics().p("EhJ0G+wMAoAgzQMiRFg7QMAAAtEPMFlzAAAMAAAPn/g");
	var mask_3_graphics_660 = new cjs.Graphics().p("Eh0EGnAMAHggzQMhGVgjgMAAAtEPMFlzAAAMAAAPn/g");
	var mask_3_graphics_672 = new cjs.Graphics().p("Eh0EGnAIlA7gMg51ACAMAAAuBfMFlzAAAMAAAPn/g");
	var mask_3_graphics_684 = new cjs.Graphics().p("EhgkHCgMgPAAowMhDVABQMAAAvgfMFlzAAAMAAAPn/g");
	var mask_3_graphics_694 = new cjs.Graphics().p("EgvEHKAMgPAAowMh01gGQMAAAvgfMFlzAAAMAAAPn/g");
	var mask_3_graphics_708 = new cjs.Graphics().p("Eiy5HjgMAAAvgfMFlzAAAMAAAPn/Mi7vAGgI3ALgg");
	var mask_3_graphics_720 = new cjs.Graphics().p("Eiy5oq/MFlzAAAMAAAPn/MiswAGgMAnABlQMjgDACQg");
	var mask_3_graphics_728 = new cjs.Graphics().p("Eiy5oq/MFlzAAAMAAAPn/MiswAGgMBJABlQMkCDACQg");
	var mask_3_graphics_742 = new cjs.Graphics().p("Eiy5IYAMAAAxI/MFlzAAAMAAARh/g");
	var mask_3_graphics_760 = new cjs.Graphics().p("Eiy5FCYMAAAvHPMFlzAAAMAAAUJvg");
	var mask_3_graphics_790 = new cjs.Graphics().p("Eiy5I9oMAAAyoPMFlzAAAMAAATVPg");
	var mask_3_graphics_876 = new cjs.Graphics().p("EBB5KdxMj0ygKPMAAA1DPMFlzAAAMAAAVZvMhnAAFsg");
	var mask_3_graphics_888 = new cjs.Graphics().p("EAP5KmhMjCygMvMAAA1SPMFlzAAAMAAAVtvMighADMg");
	var mask_3_graphics_900 = new cjs.Graphics().p("EgrGKvRMiHzgMvMAAA1jvMFlzAAAMAAAV/PMjPAADMg");
	var mask_3_graphics_910 = new cjs.Graphics().p("Eiy5re3MFlzAAAMAAAV/PMjDgADLMgqgA5EMh3zACRg");
	var mask_3_graphics_918 = new cjs.Graphics().p("Eiy5re3MFlzAAAMAAAV/PMichAAsMANgAlDMjWyAYxg");
	var mask_3_graphics_930 = new cjs.Graphics().p("Eiy5LBOMAAA2gbMFlzAAAMAAAW+bg");
	var mask_3_graphics_934 = new cjs.Graphics().p("Eiy5sNNMFlzAAAMAAAWTrMllzCGwg");
	var mask_3_graphics_938 = new cjs.Graphics().p("Eiy5MNOMAAA4abMFlzAAAMAAAW49MhmvAACMg+fBhcg");
	var mask_3_graphics_946 = new cjs.Graphics().p("Eiy5MNOMAAA4abMFlzAAAMAAAXQ/MhOPAAAMg5gBJcg");

	this.timeline.addTween(cjs.Tween.get(mask_3).to({graphics:null,x:0,y:0}).wait(42).to({graphics:mask_3_graphics_42,x:1905.15,y:215}).wait(18).to({graphics:mask_3_graphics_60,x:1834.65,y:275}).wait(12).to({graphics:mask_3_graphics_72,x:1543.15,y:275}).wait(10).to({graphics:mask_3_graphics_82,x:1135,y:245.5}).wait(20).to({graphics:mask_3_graphics_102,x:1135,y:330.5}).wait(10).to({graphics:mask_3_graphics_112,x:1135,y:550}).wait(16).to({graphics:mask_3_graphics_128,x:1135,y:1169.925}).wait(8).to({graphics:mask_3_graphics_136,x:1135,y:539.925}).wait(20).to({graphics:mask_3_graphics_156,x:1135,y:956.925}).wait(10).to({graphics:mask_3_graphics_166,x:1135,y:645}).wait(26).to({graphics:mask_3_graphics_192,x:1135,y:923.275}).wait(12).to({graphics:mask_3_graphics_204,x:1135,y:1019.0375}).wait(14).to({graphics:mask_3_graphics_218,x:1135,y:1019.1}).wait(20).to({graphics:mask_3_graphics_238,x:1135,y:1030.8}).wait(20).to({graphics:mask_3_graphics_258,x:1135,y:1284.75}).wait(20).to({graphics:mask_3_graphics_278,x:1135,y:1654.025}).wait(14).to({graphics:mask_3_graphics_292,x:1135,y:1713.225}).wait(28).to({graphics:mask_3_graphics_320,x:1135,y:1984.825}).wait(54).to({graphics:mask_3_graphics_374,x:1135,y:1880.7125}).wait(10).to({graphics:mask_3_graphics_384,x:1135,y:2110.025}).wait(12).to({graphics:mask_3_graphics_396,x:1135,y:2110.025}).wait(16).to({graphics:mask_3_graphics_412,x:1135,y:2110.025}).wait(20).to({graphics:mask_3_graphics_432,x:1135,y:1988.025}).wait(8).to({graphics:mask_3_graphics_440,x:1135,y:2074.425}).wait(26).to({graphics:mask_3_graphics_466,x:1135,y:2318.375}).wait(54).to({graphics:mask_3_graphics_520,x:1135,y:2631.575}).wait(40).to({graphics:mask_3_graphics_560,x:1135,y:3200.375}).wait(16).to({graphics:mask_3_graphics_576,x:1135,y:2851.975}).wait(30).to({graphics:mask_3_graphics_606,x:1135,y:3036.775}).wait(30).to({graphics:mask_3_graphics_636,x:1135,y:3103.975}).wait(12).to({graphics:mask_3_graphics_648,x:1135,y:3199.975}).wait(12).to({graphics:mask_3_graphics_660,x:1135,y:3199.975}).wait(12).to({graphics:mask_3_graphics_672,x:1135,y:3199.975}).wait(12).to({graphics:mask_3_graphics_684,x:1135,y:3199.975}).wait(10).to({graphics:mask_3_graphics_694,x:1135,y:3199.975}).wait(14).to({graphics:mask_3_graphics_708,x:1135,y:3257.6}).wait(12).to({graphics:mask_3_graphics_720,x:1135,y:3551.975}).wait(8).to({graphics:mask_3_graphics_728,x:1135,y:3551.975}).wait(14).to({graphics:mask_3_graphics_742,x:1135,y:3590.375}).wait(18).to({graphics:mask_3_graphics_760,x:1135,y:4127.175}).wait(30).to({graphics:mask_3_graphics_790,x:1135,y:3959.2}).wait(86).to({graphics:mask_3_graphics_876,x:1135,y:4401.375}).wait(12).to({graphics:mask_3_graphics_888,x:1135,y:4457.375}).wait(12).to({graphics:mask_3_graphics_900,x:1135,y:4513.375}).wait(10).to({graphics:mask_3_graphics_910,x:1135,y:4703.2}).wait(8).to({graphics:mask_3_graphics_918,x:1135,y:4703.2}).wait(12).to({graphics:mask_3_graphics_930,x:1135,y:4705.375}).wait(4).to({graphics:mask_3_graphics_934,x:1135,y:4999.775}).wait(4).to({graphics:mask_3_graphics_938,x:1135,y:4999.775}).wait(8).to({graphics:mask_3_graphics_946,x:1135,y:4999.775}).wait(53).to({graphics:null,x:0,y:0}).wait(1));

	// Layer_1
	this.instance_3 = new lib.element_2("synched",0);
	this.instance_3.setTransform(1134.5,4998.7,1,1,0,0,0,1134.5,4998.7);
	this.instance_3._off = true;

	var maskedShapeInstanceList = [this.instance_3];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_3;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(42).to({_off:false},0).wait(958));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,2269,9997.4);


// stage content:
(lib.laima_web_line_full = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = false; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	for (var index = 0; index < 6; index++) {
		// line_drawing
		var instance = new lib.line_drawing("synched",0,false);
		instance.setTransform(104.1,(2499*index)+314.4,0.25,0.25,0,0,0,410.8,1257.6);
		if (index != 0) {
			instance._off = true;

			this.timeline.addTween(cjs.Tween.get(instance).wait(1000*index).to({_off:false},0).wait(1000*index));
		} else {
			this.timeline.addTween(cjs.Tween.get(instance).wait(2000));
		}

	}

	this._renderFirstFrame();

}).prototype = p = new lib.AnMovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,568.7,15000);
// library properties:
lib.properties = {
	id: '219A793EC0ED764DB54DA7D8C44F51C0',
	width: 570,
	height: 15000,
	fps: 50,
	color: "#666666",
	opacity: 0.00,
	manifest: [],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['219A793EC0ED764DB54DA7D8C44F51C0'] = {
	getStage: function() { return exportRoot.stage; },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}


an.makeResponsive = function(isResp, respDim, isScale, scaleType, domContainers) {
	var lastW, lastH, lastS=1;
	window.addEventListener('resize', resizeCanvas);
	resizeCanvas();
	function resizeCanvas() {
		var w = lib.properties.width, h = lib.properties.height;
		var iw = window.innerWidth, ih=window.innerHeight;
		var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;
		if(isResp) {
			if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {
				sRatio = lastS;
			}
			else if(!isScale) {
				if(iw<w || ih<h)
					sRatio = Math.min(xRatio, yRatio);
			}
			else if(scaleType==1) {
				sRatio = Math.min(xRatio, yRatio);
			}
			else if(scaleType==2) {
				sRatio = Math.max(xRatio, yRatio);
			}
		}
		domContainers[0].width = w * pRatio * sRatio;
		domContainers[0].height = h * pRatio * sRatio;
		domContainers.forEach(function(container) {
			container.style.width = w * sRatio + 'px';
			container.style.height = h * sRatio + 'px';
		});
		stage.scaleX = pRatio*sRatio;
		stage.scaleY = pRatio*sRatio;
		lastW = iw; lastH = ih; lastS = sRatio;
		stage.tickOnUpdate = false;
		stage.update();
		stage.tickOnUpdate = true;
	}
}
an.handleSoundStreamOnTick = function(event) {
	if(!event.paused){
		var stageChild = stage.getChildAt(0);
		if(!stageChild.paused){
			stageChild.syncStreamSounds();
		}
	}
}


})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;
console.log(AdobeAn);
