<?php namespace Bboxdigi\Content\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateSustainabilityItemsTable extends Migration
{
    public function up()
    {
        Schema::create('bboxdigi_content_sustainability_items', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->timestamps();
            $table->boolean('is_published')->default(false);
            $table->string('title');
            $table->text('subitems');
        });
    }

    public function down()
    {
        Schema::dropIfExists('bboxdigi_content_sustainability_items');
    }
}