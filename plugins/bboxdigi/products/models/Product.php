<?php namespace Bboxdigi\Products\Models;

use Model;

use Cms\Classes\Page as CmsPage;
use Cms\Classes\Theme;

/**
 * Model
 */
class Product extends Model
{
    use \October\Rain\Database\Traits\Validation;

    use \October\Rain\Database\Traits\SoftDelete;
    use \October\Rain\Database\Traits\Sluggable;
    use \October\Rain\Database\Traits\Sortable;

    /**
     * @var array Generate slugs for these attributes.
     */
    protected $slugs = ['slug' => 'title'];

    public $implement = ['@RainLab.Translate.Behaviors.TranslatableModel'];

    public $translatable = [
        'title',
        'excerpt',
        'weight',
        'desc',
        'nutrition',
        'nutr1',
        'nutr2',
        'nutr3',
        'nutr4',
        'nutr5',
        'nutr6',
        'nutr7',
        'nutr8',
    ];

    protected $dates = ['deleted_at', 'new_at'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'bboxdigi_products_products';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $appends = [
        'title_image_thumb'
    ];


    public $belongsToMany = [
        'categories' => [
            'Bboxdigi\Products\Models\Category',
            'table' => 'bboxdigi_products_cat_prod'
        ],
        'allergens' => [
            'Bboxdigi\Products\Models\Allergen',
            'table' => 'bboxdigi_products_aller_prod'
        ],
        'collections' => [
            'Bboxdigi\Products\Models\Collection',
            'table' => 'bboxdigi_products_coll_prod'
        ],
    ];

    public $attachOne = [
        'title_image' => 'System\Models\File',
    ];

    public function  getTitleImageThumbAttribute()
    {
        if ($this->title_image) {
            return $this->title_image->getThumb(300,300);
        }
    }

    public function scopeIsPublished($query)
    {
        return $query->where('is_published', 1);
    }

    public static function getMenuTypeInfo($type)
    {
        $result = [
            'dynamicItems' => true
        ];
        $theme = Theme::getActiveTheme();
        $pages = CmsPage::listInTheme($theme, true);
        $result['cmsPages'] = $pages;

        return $result;
    }

    public static function resolveMenuItem($item, $url, $theme)
    {
        $result = [
            'items' => []
        ];

        $page = CmsPage::loadCached($theme, $item->cmsPage);

        $defaultLocale = \RainLab\Translate\Models\Locale::getDefault()->code;

        $alternateLocales = array_keys(\RainLab\Translate\Models\Locale::listEnabled());

        $products = self::isPublished()->get();
        foreach ($products as $product) {
            $pageUrl = \Utopigs\Seo\Models\Sitemap::getPageLocaleUrl($page, $product, $defaultLocale, [
                'slug' => 'slug'
                ]);

            $productItem = [
                'title' => $product->title,
                'url'   => $pageUrl,
                'mtime' => $product->updated_at,
            ];
            if (count($alternateLocales) > 1) {
                foreach ($alternateLocales as $locale) {
                    $productItem['alternate_locale_urls'][$locale] = \Utopigs\Seo\Models\Sitemap::getPageLocaleUrl($page, $product, $locale, ['slug' => 'slug']);
                }
            }

            $result['items'][] = $productItem;
        }
        return $result;
    }
}
