<?php namespace Bboxdigi\Base;

use System\Classes\PluginBase;

use Cms\Classes\Page;
use Response;
use Redirect;
use Request;
use Session;
use Event;

class Plugin extends PluginBase
{
    /**
     * @var array Plugin dependencies
     */
    public $require = [
        'RainLab.Builder',
        'RainLab.Translate',
        'Indikator.Backend',
    ];

    /**
     * Register components
     *
     */
    public function registerComponents()
    {
    	return [
            'Bboxdigi\Base\Components\Menu' => 'Menu',
    	];
    }

    /**
     * Register plugin backend settings
     *
     */
    public function registerSettings()
    {
    }

    /**
     * Add Twig tags
     * 
     */
    public function registerMarkupTags()
    {
        return [
            'functions' => [
                'backendLoggedIn' => function() {
                    return \BackendAuth::getUser() != null;
                },
            ],
        ];
    }

    /**
     * Register artisan comands
     *
     */
    public function register()
    {
        $this->registerConsoleCommand('bboxdigi:fresh-install', 'Bboxdigi\Base\Console\FreshInstall');
    }

    public function boot()
    {
        // Extend all backend form usage
        Event::listen('backend.form.extendFields', function($widget) {


            // Only for the CMS Index controller
            if (!$widget->getController() instanceof \Cms\Controllers\Index) {
                return;
            }


            // Only for the Page model
            if (!$widget->model instanceof \Cms\Classes\Page) {
                return;
            }


            // Add custom fields...
            $widget->addTabFields([
                'viewBag[meta_keywords]' => [
                    'label'   => 'Meta Keywords',
                    'type'    => 'text',
                    'tab'     => 'cms::lang.editor.meta'
                ],
            ]);
        });
    }
    
}
