<?php namespace Bboxdigi\Base\Traits;

use DbDongle;
use October\Rain\Database\Collection;
use Bboxdigi\Base\Scopes\IsPublishedScope;
use Exception;

trait IsPublished
{
    /**
     * @var int Indicates if the model should be aligned to new parent.
     */
    // protected $moveToNewParentId = null;

    public function scopeIsPublished ($query) {
        $is_published_scope = new IsPublishedScope;
        return $is_published_scope->apply($query, $this);
    }

    public function scopeOrderByPublished ($query) {
        return $query->orderBy('published_at', 'desc');
    }


    /*
     * Constructor Example
     */
    public static function bootIsPublished()
    {
        // static::addGlobalScope(new SoftDeletingScope);

        // static::extend(function($model){
            /*
             * Define relationships
             */
            // $model->hasMany['children'] = [
            //     get_class($model),
            //     'key' => $model->getParentColumnName()
            // ];

            // $model->belongsTo['parent'] = [
            //     get_class($model),
            //     'key' => $model->getParentColumnName()
            // ];

            /*
             * Bind events
             */
            // $model->bindEvent('model.beforeCreate', function() use ($model) {
            //     $model->setDefaultLeftAndRight();
            // });

            // $model->bindEvent('model.beforeSave', function() use ($model) {
            //     $model->storeNewParent();
            // });

            // $model->bindEvent('model.afterSave', function() use ($model) {
            //     $model->moveToNewParent();
            //     $model->setDepth();
            // });

            // $model->bindEvent('model.beforeDelete', function() use ($model) {
            //     $model->deleteDescendants();
            // });

            // if (static::hasGlobalScope(SoftDeletingScope::class)) {
            //     $model->bindEvent('model.beforeRestore', function() use ($model) {
            //         $model->shiftSiblingsForRestore();
            //     });

            //     $model->bindEvent('model.afterRestore', function() use ($model) {
            //         $model->restoreDescendants();
            //     });
            // }
        // });
    }
}
