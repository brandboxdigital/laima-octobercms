<?php namespace Bboxdigi\Products;

use System\Classes\PluginBase;

use Event;
use bboxdigi\products\models\Product;
use bboxdigi\products\models\Category;
use bboxdigi\products\models\Collection;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
      return [
          'Bboxdigi\Products\Components\ProductComponent' => 'Products',
      ];
    }

    public function registerSettings()
    {
    }

    public function boot()
    {
        /*
         * Register menu items for the RainLab.Pages plugin
         */
        Event::listen('pages.menuitem.listTypes', function() {
            
            return [
                'all-products'       => 'All Products',
                'prod-cat' => 'Product Categories',
                'prod-col' => 'Product Collections'
            ];
        });

        Event::listen('pages.menuitem.getTypeInfo', function($type) {
            
            if ($type == 'all-products') {
                return Product::getMenuTypeInfo($type);
            }
            elseif ($type == 'prod-cat') {
                return Category::getMenuTypeInfo($type);
            }
            elseif ($type == 'prod-col') {
                return Collection::getMenuTypeInfo($type);
            }
        });

        Event::listen('pages.menuitem.resolveItem', function($type, $item, $url, $theme) {
            
            if ($type == 'all-products') {
                return Product::resolveMenuItem($item, $url, $theme);
            }
            elseif ($type == 'prod-cat') {
                return Category::resolveMenuItem($item, $url, $theme);
            }
            elseif ($type == 'prod-col') {
                return Collection::resolveMenuItem($item, $url, $theme);
            }
        });
    }
}
