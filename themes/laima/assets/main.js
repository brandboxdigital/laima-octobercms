// Reference our components so they get included
import { initializeComponents } from 'utils/initializeComponents'
// import $ from 'jquery'
import 'popper.js'
import 'bootstrap'
import Rellax from 'rellax'
import AOS from 'aos'
import autocomplete from 'autocompleter'

const ready = () => {
  if (
    document.readyState === 'interactive' ||
    document.readyState === 'complete'
  ) {
    return Promise.resolve()
  }

  return new Promise(resolve =>
    document.addEventListener('DOMContentLoaded', resolve)
  )
}

let leafMap
const shopsMap = $('#shops-map')
let shops = []
let hasMarkers = false;
let markersReady = false;
let tempMarkers = [];
const shopsCitiesEl = $('#shops-cities a');
const featuredSlider = $('#featured-products .featured-slider')
const shopsContacts = $('#shops-contacts .shop-contact');
let shopJSON;
let userMarker;
var leafIcon = {
  url: shopsMap.data('leaf-icon'),
  size: [40, 40],
}

var userIcon = {
  url: shopsMap.data('user-icon'),
  size: [40, 40],
}

let shopsJSONurl = '/mock/shops.json';

ready().then(() => {
  initializeComponents(document)

  if (shopsMap !== undefined) {
    leafMap = L.map('shops-map').setView([56.962838, 24.131839], 15)
    L.tileLayer(
      'https://api.mapbox.com/styles/v1/richardev/ckclhvqta0nk31iqs26xd2ngc/tiles/256/{z}/{x}/{y}@2x?access_token=pk.eyJ1IjoicmljaGFyZGV2IiwiYSI6ImNrY2xnY3VoMTBnYmoyc3J6NmUxdDFkdjYifQ.FgrOAAAY6Xr4Ct3h5-Zkmg',
      {
        maxZoom: 18,
        minZoom: 1,
        id: 'mapbox/streets-v11',
        scrollWheelZoom: false,
        accessToken:
          'pk.eyJ1IjoicmljaGFyZGV2IiwiYSI6ImNrY2xnY3VoMTBnYmoyc3J6NmUxdDFkdjYifQ.FgrOAAAY6Xr4Ct3h5-Zkmg'
      }
    ).addTo(leafMap)

    leafIcon = L.icon({
      // iconUrl: '/static/img/icons/circle.svg',
      iconUrl: leafIcon.url,
      iconSize: leafIcon.size,
      // shadowSize: [40, 98],
    })

    userIcon = L.icon({
      iconUrl: userIcon.url,
      iconSize: userIcon.size,
    })

    $.request('onGetShops', {
        success: function(el) {
          if (el.length) {
            shopJSON = el;
            $.each(el, function (e, target) {
              if (target.title) target.label = target.title
              if (target.lat && target.long) {
                hasMarkers = true;
                let city = String(target.city);

                if (shops[city] == undefined) shops[city] = new L.featureGroup().addTo(leafMap)

                shops[city].addLayer(
                  new L.marker([parseFloat(target.lat), parseFloat(target.long)], {
                    icon: leafIcon,
                    city: city,
                    slug: String(target.slug)
                  }).on('click', function (cel) {
                    // console.log(el.target.options.slug);
                    let slug = cel.target.options.slug;
                    let city = cel.target.options.city;

                    // console.log(slug);

                    if (shopsContacts !== undefined) {
                      shopsContacts.hide();
                      shopsContacts.removeClass('active');
                      $('.clear-shop-filter').fadeIn(150);
                      $.each(shopsContacts, function (i, contactEl) {
                        if ($(contactEl).data('city') == city) {
                          $(contactEl).show();
                          if ($(contactEl).data('slug') == slug) {
                            $(contactEl).addClass("active");
                            $('html, body').animate({
                              scrollTop: $(contactEl).offset().top - 90
                            }, 300);
                          }
                        }
                      });
                    }

                    shopsCitiesEl.removeClass('active');
                    $('#shops-cities a[data-city="' + city + '"]').addClass('active');
                  })
                );

                tempMarkers.push([parseFloat(target.lat), parseFloat(target.long)]);
              }
            })
            markersReady = true;
          }
        }
    })

    // group.addTo(leafMap)
    leafMap.scrollWheelZoom.disable()
  }

  if (featuredSlider !== undefined) {
    featuredSlider.slick({
      arrows: true,
      swipeToSlide: true,
      speed: 300,
      infinite: true,
      mobileFirst: true,
      draggable: true,
      nextArrow:
        '<div class="next-arrow" data-aos="zoom-out" data-aos-delay="700"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 130 130"><circle cx="65" cy="65" r="65" style="fill:none"/><circle cx="65" cy="65" r="64" style="fill:none;stroke:#2c0302;stroke-width:2px"/><line x1="44.4" y1="65.5" x2="33.6" y2="54.71" style="fill:none;stroke:#2c0302;stroke-linecap:round;stroke-width:3px"/><line x1="44.4" y1="65.5" x2="33.6" y2="76.29" style="fill:none;stroke:#2c0302;stroke-linecap:round;stroke-width:3px"/></svg></div>',
      prevArrow:
        '<div class="prev-arrow" data-aos="zoom-out" data-aos-delay="900"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 130 130"><circle cx="65" cy="65" r="65" style="fill:none"/><circle cx="65" cy="65" r="64" style="fill:none;stroke:#2c0302;stroke-width:2px"/><line x1="96.4" y1="54.71" x2="85.6" y2="65.5" style="fill:none;stroke:#2c0302;stroke-linecap:round;stroke-width:3px"/><line x1="96.4" y1="76.29" x2="85.6" y2="65.5" style="fill:none;stroke:#2c0302;stroke-linecap:round;stroke-width:3px"/></svg></div>',
      slidesToShow: 3,
      touchThreshold: 100,
      centerMode: true,
      centerPadding: 0,
      responsive: [
        { breakpoint: 1499, settings: { slidesToShow: 3 } },
        { breakpoint: 1199, settings: { slidesToShow: 3 } },
        { breakpoint: 991, settings: { slidesToShow: 2 } },
        { breakpoint: 768, settings: { slidesToShow: 2 } },
        { breakpoint: 576, settings: { slidesToShow: 1 } },
        { breakpoint: 385, settings: { slidesToShow: 1 } },
        { breakpoint: 0, settings: { slidesToShow: 1 } }
      ]
    })
  }
})

$(function(){
  if (shopsMap !== undefined) {
    var checkMarkersReady = window.setInterval(function () {
      if (markersReady) {
        clearInterval(checkMarkersReady);

        var input = document.getElementById("search-shops");

        autocomplete({
          input: input,
          fetch: function (text, update) {
            text = text.toLowerCase();
            // you can also use AJAX requests instead of preloaded data
            var suggestions = $.grep(shopJSON, function (row) {
              let prepText = text.replace(/[^\w\s]/g, ".*");
              let pattern = new RegExp(prepText, 'gmiu');
              return pattern.test(row.title.toLowerCase()) || pattern.test(row.text.toLowerCase()) || pattern.test(row.phone.toLowerCase()) || pattern.test(row.email.toLowerCase()) || pattern.test(row.city.toLowerCase())
            })
            update(suggestions);
          },
          onSelect: function (item) {
            input.value = item.title;
            if (shopsContacts !== undefined) {
              shopsContacts.hide();
              shopsContacts.removeClass('active');
              $('.clear-shop-filter').fadeIn(150);
              $.each(shopsContacts, function (i, el) {
                if ($(el).data('city') == item.city) {
                  $(el).show();
                  if ($(el).data('slug') == item.slug) {
                    $(el).addClass("active");
                    leafMap.fitBounds([[parseFloat(item.lat), parseFloat(item.long)]]);
                  }
                }
              });
            }

            shopsCitiesEl.removeClass('active');
            $('#shops-cities a[data-city="' + item.city + '"]').addClass('active');
          }
        });

        shopsCitiesEl.click(function (el) {
          let city = $(this).data('city');
          let cityShops = shops[city];

          shopsCitiesEl.removeClass('active');
          $(this).addClass('active');

          if (cityShops) {
            leafMap.fitBounds(cityShops.getBounds());
            // leafMap.panTo(cityShops.getBounds());
          }

          if (shopsContacts !== undefined) {
            shopsContacts.hide();
            shopsContacts.removeClass('active');
            $('.clear-shop-filter').fadeIn(150);
            $.each(shopsContacts, function (i, el) {
              if ($(el).data('city') == city) $(el).show();
            });
          }
        });

        $('.clear-shop-filter').click(function (el) {
          $(this).hide();
          shopsContacts.removeClass('active');
          shopsCitiesEl.removeClass('active');
          shopsContacts.show();
          leafMap.fitBounds(tempMarkers);
        });

        leafMap.fitBounds(tempMarkers);

        $("#show-nearest-shops").click(function () {
          $('html, body').animate({
            scrollTop: shopsCitiesEl.offset().top - 60
          }, 300);

          if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
              if (userMarker == undefined) {
                userMarker = new L.featureGroup().addTo(leafMap)
                userMarker.addLayer(
                  new L.marker([parseFloat(position.coords.latitude), parseFloat(position.coords.longitude)], {
                    icon: userIcon
                  }));
              }
              var closestLocation;
              var tempClosest;

              $.each(tempMarkers, function (i, el) {
                var temp = Math.abs(parseFloat((parseFloat(position.coords.latitude) - el[0]) - (parseFloat(position.coords.longitude) - el[1])));
                if (tempClosest == undefined || temp < tempClosest) {
                  tempClosest = temp;
                  closestLocation = [el[0], el[1]];
                }
              });

              leafMap.panTo(new L.LatLng(parseFloat(position.coords.latitude), parseFloat(position.coords.longitude)));
              leafMap.fitBounds([
                closestLocation,
                [parseFloat(position.coords.latitude), parseFloat(position.coords.longitude)],
              ]);

              var checkCurrentLocation = window.setInterval(function () {
                leafMap.removeLayer(userMarker);
                userMarker = new L.featureGroup().addTo(leafMap)
                userMarker.addLayer(
                  new L.marker([parseFloat(position.coords.latitude), parseFloat(position.coords.longitude)], {
                    icon: userIcon
                  }));
              }, 5000);

              $('.clear-shop-filter').fadeIn(150);
            },
              function (error) {
                alert(error.message);
              }, {
              enableHighAccuracy: true
              , timeout: 5000
            });
          }
        });


      }
    }, 250);
  }

  $('#contact-form').submit(function(e){
    // e.preventDefault();
    $('#contact-form').addClass('contact-progress');
    $('#contact-form .form-progress .sending').fadeIn(150);
  });

  $('#contact-form .form-group div').on('event',function(){
    $(this).addClass('updated').trigger('classChange');
  });

  $('#contact-form .form-group div').on('classChange', function() {
    $('#contact-form .form-progress .sending').fadeOut(150);
    $('#contact-form .form-progress .sent').fadeOut(150);
    $('#contact-form .form-progress .close').fadeOut(150);
    $('#contact-form').removeClass('progress');
  });

  $('#contact-form .form-progress .close').click(function(){
    $('#contact-form .form-progress .sending').fadeOut(150);
    $('#contact-form .form-progress .sent').fadeOut(150);
    $('#contact-form .form-progress .close').fadeOut(150);
    $('#contact-form').removeClass('contact-progress');
  });

  AOS.init({ once: true })
  $('#hero .slick-track')
    .addClass('rellax')
    .data('rellax-speed', '-3')
    .attr('data-rellax-speed', '-3')
  $('#collection .collection-slider')
    .addClass('rellax')
    .data('rellax-speed', '1.25')
    .attr('data-rellax-speed', '1')
  // var rellax = new Rellax('.rellax')
});

// // If you want to use conditioner instead:
//
// // 1.  Remove the 'manual init' block
// // 2.  Uncomment the lines below
// // 3.  Run $ npm install conditioner-js --save
//
// import conditioner from 'conditioner-js';
//
// // When you want to use monitors, import them here
// import 'conditioner-js/dist/min/monitors/media';
//
// // Setup conditioner loader to load modules from bundle
// conditioner.setOptions({
//     loader: {
//         require: (path, callback) => {
//             const module = path in components ? components[path] : require(path);
//             return callback(module.default ? module.default : module);
//         },
//         toUrl: path => path
//     },
//     paths: {
//         monitors: 'conditioner-js/dist/min/monitors/'
//     }
// });
//
// // Initialize Conditioner
// ready().then(conditioner.init);
