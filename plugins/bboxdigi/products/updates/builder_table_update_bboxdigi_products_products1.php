<?php namespace Bboxdigi\Products\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBboxdigiProductsProducts1 extends Migration
{
    public function up()
    {
        Schema::table('bboxdigi_products_products', function($table)
        {
            $table->string('nutr8')->nullable();
        });
    }

    public function down()
    {
        Schema::table('bboxdigi_products_products', function($table)
        {

            $table->dropColumn('nutr8');
        });
    }
}