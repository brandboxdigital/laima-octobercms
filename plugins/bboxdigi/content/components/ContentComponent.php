<?php namespace Bboxdigi\Content\Components;

use Cms\Classes\ComponentBase;
use Cms\Classes\Page;
use Bboxdigi\Content\Models\Text;
use Bboxdigi\Content\Models\Slide;
use Bboxdigi\Content\Models\Shop;
use Bboxdigi\Content\Models\History;
use Bboxdigi\Content\Models\Question;
use Bboxdigi\Content\Models\Contact;
use Bboxdigi\Content\Models\PackageHistory;
use Bboxdigi\Content\Models\SustainabilityItem;
use Bboxdigi\Content\Models\SustainabilityGoal;
use Bboxdigi\Content\Models\ExtraContent;
use Bboxdigi\Content\Models\SpringArticle;
use RainLab\Translate\Classes\Translator;
use Cache;

class ContentComponent extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Article Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun() {
        $activeLocale = Translator::instance()->getLocale();
        $this->page['texts'] = Cache::rememberForever('texts_'.$activeLocale, function() {
            return Text::all()->keyBy('id')->toArray();
        });
        // dd($this->page['texts']);
    }

    public function getContacts() {
        return Contact::all();
    }

    public function getQuestions() {
        return Question::orderBy('sort_order', 'asc')->get();
    }

    public function getSlides() {
        return Slide::where('is_published', 1)->orderBy('sort_order', 'asc')->get();
    }

    public function getShops() {
        return Shop::where('is_published', 1)->orderBy('sort_order', 'asc')->get();
    }

    public function getPackageHistory() {
        return PackageHistory::orderBy('year', 'asc')->get();
    }

    public function getHistory() {
        return History::orderBy('year', 'asc')->get();
    }

    public function getShopCities() {
        $shops = $this->getShops();
        return $shops->groupBy('city');
    }

    public function onGetShops(){
        return $this->getShops();
    }

    public function getSustainabilityItems() {
        return SustainabilityItem::orderBy('sort_order', 'asc')->get();
    }

    public function getSustainabilityGoals() {
        return SustainabilityGoal::orderBy('sort_order', 'asc')->get();
    }

    public function getExtraContent() {
        // dd(ExtraContent::first());
        return ExtraContent::first();
    }

    public function getSpringArticles() {
        return SpringArticle::where('is_published', 1)->orderBy('sort_order', 'asc')->get();
    }
}
