<?php namespace Bboxdigi\Products\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateBboxdigiProductsCollProd extends Migration
{
    public function up()
    {
        Schema::create('bboxdigi_products_coll_prod', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('collection_id')->unsigned();
            $table->integer('product_id')->unsigned();
            $table->primary(['collection_id','product_id']);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('bboxdigi_products_coll_prod');
    }
}
