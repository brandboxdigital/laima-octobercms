<?php namespace Bboxdigi\Articles\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateBboxdigiArticlesRecipes extends Migration
{
    public function up()
        {
            Schema::create('bboxdigi_articles_recipes', function($table)
            {
                $table->engine = 'InnoDB';
                $table->increments('id')->unsigned();
                $table->timestamp('created_at')->nullable();
                $table->timestamp('updated_at')->nullable();
                $table->timestamp('deleted_at')->nullable();
                $table->string('title');
                $table->string('slug');
                $table->text('excerpt')->nullable();
                $table->text('body')->nullable();
                $table->boolean('is_published')->default(0);
                $table->timestamp('published_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
                $table->dateTime('published_till')->nullable();
            });
        }
        
        public function down()
        {
            Schema::dropIfExists('bboxdigi_articles_recipes');
        }
}
